<?php
class Giving extends MY_Controller{
    
    const STATUS_UPDATE = 'update';
    const STATUS_ERROR  = 'error';
    const STATUS_SAVED  = 'saved';
    const PDF_GIVING_FOLDER = 'application/giving_pdfs/';
    /**
    * @var visitors_model
    */
    public $orm;
    
    public function __construct(){
        parent::__construct();  
        $this->authorize_or_redirect();
        $this->load->model('visitors_model','orm', TRUE );
        $this->load->model('giving_model');
        $this->orm->set_user($this->user);    
    }

    public function index($first_letter = null, $selected = array(),  $status = '') {
        if( $first_letter==null && (!($first_letter = $this->uri->segment(3)) || !preg_match('/^[a-zA-Z]{1}$/',$first_letter))){
            $first_letter = 'ALL';
        }  
       
        $type = new Type();        
        $type->set_type( 6 );    
        $type->set_company_id( $this->user->company_id );
        $type->set_active( 1 );
     
        $givings = $this->model->get_object( $type );
     
        $date_giving = date('Y-m-d');
        
        $date_giving = (isset($_GET['giving_date']) and $_GET['giving_date'] !='') ? $this->input->get('giving_date') : $date_giving;        
        $start_date = (isset($_POST['start_date']) and $_POST['start_date'] !='') ? $this->input->post('start_date') : date('Y-m-d',  strtotime('-7 days'));
        $end_date = (isset($_POST['end_date']) and $_POST['end_date'] !='') ? $this->input->post('end_date') : date('Y-m-d');
        $view_type = (isset($_GET['view']) and $_GET['view'] != '')? $this->input->get('view') : 1;
        
        if(!empty($_POST) and isset($_POST['action']) and $_POST['action'] == 'Save') {
            // $giving_input = $this->input->post('giving');
            $giving_input = $this->input->post('fund');
            $date_giving  = $this->input->post('giving_date');
            foreach ($giving_input as $key => $value) {
                if($value['giving'] == null || $value['giving']=='null'){
                    continue;
                }
                if($value['edit'] == "false") {
                    $input = array('member_id' => $value['visitor'],
                            'giving_id' => $value['giving'],
                            'value' => $value['amount'],
                            'giving_date' => $date_giving
                            );
                    $this->db->insert('dr_member_giving',$input);

                } else {
                    $input = array(
                        'value' => $value['amount'],
                        'giving_id' => $value['giving']

                        );
                    $this->db->where('id',$value['edit']);
                    $this->db->update('dr_member_giving',$input);    
               }

            }
          
            // if(is_array($giving_input) and count($giving_input) > 0) {
            //     foreach($giving_input as $member_id => $giving) {
            //         if(is_array($giving) and count($giving) > 0) {
            //             $i = 0;
            //             foreach($giving as $giv_val) {
            //                 $fund_id = $givings[$i]->id;
            //                 $givingDate = $this->giving_model->getGivingMember($member_id,$fund_id,$date_giving) ;
                          
            //                 if(!$givingDate) {
            //                     $input = array('member_id' => $member_id,
            //                             'giving_id' => $fund_id,
            //                             'value' => $giv_val,
            //                             'giving_date' => $date_giving
            //                             );
            //                     $this->db->insert('dr_member_giving',$input);
            //                 } else {
            //                     foreach ($givingDate as $key => $gd) {
            //                         $input = array(
            //                             'value' => $giv_val,
            //                             );
            //                         $this->db->where('id',$gd->id);
            //                         $this->db->update('dr_member_giving',$input);    
            //                     }
            //                 }
                            
            //                 $i++;
            //             }
            //         }
            //     }
            // }
            exit;
          
        } else if(!empty($_POST) and isset($_POST['action']) and $_POST['action'] == 'Email Report'){
            
            $this->load->library('funding_report');
            $company = $this->user->_company[0];
            $members = $this->input->post('visitors');
            $membersReport = array();
            $limit = (isset($_POST['DataTables_Table_0_length'])) ? $this->input->post('DataTables_Table_0_length') : 10;
            
            $type = new Type();        
            $type->set_type( 6 );        
            $type->set_company_id( $this->user->company_id );
            $type->set_active( 1 );
            $givings = $this->model->get_object( $type );
            
            if(!$members) {
                
                $filter = new Visitor();
                $filter->set_company_id( $this->user->company_id );
                $filter->set_person_type( Person::TYPE_MEMBER );
                if($view_type == 1)
                    $filter->set_head_household( 1);
                $filter->set_order_by('first_name ASC');
                $visitors = $this->orm->get_object( $filter, false, array('first_name' => "REGEXP '^<first_name>'"),null,'Visits');
                if(count($visitors) > 0) {
                    foreach( $visitors as $visitor ){
                        $memberInfo['person'] =  $visitor;
                        $givingInfo = array ();
                        if(isset($givings) and is_array($givings) and count($givings) > 0) {
                            foreach($givings as $give) {
                                $givingDate = $this->giving_model->getGivingMemberTotalDate($visitor->id ,$give->id,$start_date,$end_date) ;
                                $givingInfo[$give->name]  = $givingDate;
                            }
                        }
                        $memberInfo['giving'] =  $givingInfo;
                        $membersReport[] = $memberInfo;
                    }
                }
                
            } else {
                foreach($members as $member_id) {
                    $filter = new Visitor();
                    $filter->set_company_id( $this->user->company_id );
                    $filter->set_person_type( Person::TYPE_MEMBER );
                    $filter->set_id($member_id);
                    $member = $this->orm->get_object( $filter , true);
                    
                    $memberInfo['person'] =  $member;
                    $givingInfo = array ();
                    if(isset($givings) and is_array($givings) and count($givings) > 0) {
                        foreach($givings as $give) {
                            $givingDate = $this->giving_model->getGivingMemberTotalDate($member_id ,$give->id,$start_date,$end_date) ;
                            $givingInfo[$give->name]  = $givingDate;
                        }
                    }
                    $memberInfo['giving'] =  $givingInfo;
                    $membersReport[] = $memberInfo;
                    
                }
            }
            $filename = $this->get_local_path().self::PDF_GIVING_FOLDER.$this->funding_report->get_company_name($company);
            
            $primary_contact = $company->primary_contact;
                
            $filter = new Visitor();
            $filter->set_id($primary_contact);
            $filter->set_person_type( Person::TYPE_USER );
            $primaryContact = $this->model->get_object( $filter,true );
            $start_date = date('m/d/Y',  strtotime($start_date));
            $end_date = date('m/d/Y',  strtotime($end_date));
            $this->funding_report->create_pdf( $company, $membersReport ,$start_date , $end_date , $filename);
            $this->load->library('email');
            $this->email->from( 'no-reply@visitorplus.net', 'No-Reply'); 
            $this->email->to( $primaryContact->email ); 
            $this->email->subject('Thank you for your giving contributions.  Here is your report.');
            $this->email->message( 'Thank you for your loyalty to your giving.  Below you will find a PDF report with all of your details for the date range of:'.$start_date.' - '.$end_date);
            $this->email->attach( $filename );       
            // $this->email->send();
        
        }

        
        $this->load->view('include/sidebar', array('user'=> $this->user, 'visitor_menu'=>true, 'no_new_user'=>true ));
        $this->load->view('giving/index', array('user'  => $this->user,                                                     
                                                     'selected_letter' => $first_letter, 
                                                     'selected_user'   => $selected,
                                                     'givings' => $givings,
                                                     'date_giving' => $date_giving,
                                                     'start_date' => $start_date,
                                                     'end_date' => $end_date,
                                                     'view_type' => $view_type,
                                                     'status' => $status ));
        $this->load->view('include/footer');
    }

    public function get_visitor_notes_popup(){
        if( $visitor_id = $this->input->post('id')){
            $visitorF = new Visitor();
            $visitorF->set_id( $visitor_id);
            $visitorF->set_company_id( $this->user->company_id );
            $visitorF->set_person_type( Person::TYPE_MEMBER );
            $visitor = $this->orm->get_object( $visitorF, true );

            if($visitor){
                $data = '<div id="div_status_'.$visitor->id.'" class="hidden">
                            <a class="close" data-dismiss="alert">�</a>
                         </div>
                         <textarea id="text_notes_'.$visitor->id.'">'.$visitor->notes.'</textarea>
                         <a class="btn" href="javascript:void(0)" onclick="save_notes('.$visitor->id.')">Save</a>
                         <a id="btn_cancel_'.$visitor->id.'"class="btn" href="javascript:void(0)" onclick="hide_notes('.$visitor->id.',this)">Cancel</a>';             
                $title = 'Notes';
                $field_data = $visitor->notes ? 'Edit Notes' : 'Add Notes';
                $trigger = 'data-trigger=\'click\'';
                $tag = 'a';
                $id = 'notes_';
                $class = 'btn span2'; 
                echo '<'.$tag.' '.$trigger.' class="'.$class.'"  data-html="" data-placement="left" id="'.$id.$visitor->id.'" data-content=\''.$data.'\'   data-original-title="'.$title.'">'.$field_data.'</'.$tag.'>';  
            }    
        }
    }

    public function ajax_save_notes(){
        $status = "";
        if( ($notes = $this->input->post('notes')) && ($id = $this->input->post('id'))){
            $visitorF = new Visitor();
            $visitorF->set_id( $id);
            $visitorF->set_company_id( $this->user->company_id );
            $visitorF->set_person_type( Person::TYPE_MEMBER );
            $visitor = $this->orm->get_object( $visitorF, true );
        
            if( $visitor ){
                $visitor->set_notes( $notes );
                $this->orm->save_object($visitor);
                $status = 'success';
            } else {
                $status = 'fail';
            }
        }
        echo json_encode( $status );
    }


    function ajax_loading($first_letter = null) {
        if( $first_letter==null && (!($first_letter = $this->uri->segment(3)) || !preg_match('/^[a-zA-Z]{1}$/',$first_letter))){
            $first_letter = 'ALL';
        }  
           
        $date_giving = (isset($_GET['giving_date']) and $_GET['giving_date'] !='') ? $this->input->get('giving_date') : date('Y-m-d');
        $view_type = (isset($_GET['view_type']) and $_GET['view_type'] != '')? $this->input->get('view_type') : 1;
        
        $visitor_filter = new Visitor();
        $visitor_filter->set_company_id( $this->user->company_id );
        $visitor_filter->set_person_type( Person::TYPE_MEMBER );

        if($view_type == 1)
            $visitor_filter->set_head_household( 1);
    
        $search = $this->input->get('sSearch');
        
        if($search !='') {
            $visitor_filterSecond = new Visitor();
            $visitor_filterSecond->set_company_id( $this->user->company_id );
            $visitor_filterSecond->set_person_type( Person::TYPE_MEMBER );
            $visitor_filter->set_first_name($search);    
            $visitor_filterSecond->set_last_name($search);    
            if($view_type == 1)
                $visitor_filterSecond->set_head_household( 1);
        } elseif( $first_letter != 'ALL'){
            $visitor_filter->set_first_name($first_letter);    
        }
        
        $columnOrder = (isset($_GET['iSortCol_0']) and $_GET['iSortCol_0'] !='') ? $this->input->get('iSortCol_0') : 1;
        $orderBy     = (isset($_GET['sSortDir_0']) and $_GET['sSortDir_0'] !='') ? $this->input->get('sSortDir_0') : 'desc';
        
        if($columnOrder == 2) {
            $visitor_filter->set_order_by(array('first_name '.$orderBy));
            if(isset($visitor_filterSecond)) {
                $visitor_filterSecond->set_order_by(array('first_name '.$orderBy));
            }
        }elseif($columnOrder == 3) {
            $visitor_filter->set_order_by(array('last_name '.$orderBy));
            if(isset($visitor_filterSecond)) {
                $visitor_filterSecond->set_order_by(array('last_name '.$orderBy));
            }
        } else {
            $visitor_filter->set_order_by(array('first_name ASC'));
            if(isset($visitor_filterSecond)) {
                $visitor_filterSecond->set_order_by(array('first_name ASC'));
            }
        }
        
        if($search !='') {            
            $visitors = $this->orm->get_object( $visitor_filter, false, array('first_name' => "REGEXP '<first_name>'"),null,'Visits');
            $visitorsSecond = $this->orm->get_object( $visitor_filterSecond, false, array('last_name' => "REGEXP '<last_name>'"),null,'Visits');
            
            $visitors = $visitorsSecond + $visitors;
        } else {
            $visitors = $this->orm->get_object( $visitor_filter, false, array('first_name' => "REGEXP '^<first_name>'"),null,'Visits');
        }

        $sLimit = 0;
        $offset = 0;
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $offset = $this->input->get('iDisplayStart');
            $sLimit = $this->input->get('iDisplayLength');
        }
        
        $sLimit      = ($sLimit == 0 ) ? 10 : $sLimit;
        
        $rowTotal       = count($visitors);
        
        if($rowTotal > $offset) {
            $visitors = array_slice($visitors, $offset,$sLimit);
        }
        $this->load->helper('custom_helper');
        
        $output = array(
    		"sEcho" => intval($_GET['sEcho']),
    		"iTotalRecords" => $rowTotal,
    		"iTotalDisplayRecords" =>$rowTotal,
    		"aaData" => array()
    	);

        $type = new Type();        
        $type->set_type( 6 );        
        $type->set_company_id( $this->user->company_id );
        $type->set_active( 1 );
        $givings = $this->model->get_object( $type );
        
        if(count($visitors) > 0) {           
            foreach( $visitors as $index=>$visitor ){
                $row = array();
                $attr = '';
                
                $row['DT_RowClass'] = $attr;
                $row[] = form_checkbox( 'visitors[]', $visitor->id, isset($selected_user[$visitor->id]) ? true: false );
                // $row[] = get_popover_data_member($visitor, 'visit_date');
                $row[] = get_href_members($visitor->first_name, $visitor->id);
                $row[] = get_href_members($visitor->last_name, $visitor->id);

                $htmlElement='';
                $hasoption = false;
                if(isset($givings) and is_array($givings) and count($givings) > 0) {
                    foreach($givings as $give) {
                        $givingDate = $this->giving_model->getGivingMember($visitor->id ,$give->id,$date_giving) ;
                        if($givingDate){
                            $hasoption= true;
                            $index = 1;
                            foreach ($givingDate as $key => $gitem) {
                                $htmlElement .= '<span class="object_fund" data-id="'.$visitor->id.'" data-is="'.$gitem->id.'"><div><input type="text" name="giving['.$visitor->id.'][]" class="input" value="'.$gitem->value.'" data-n="'.$give->id.'_'.$index.'"></div>';
                                $htmlElement .= '<div><select class="selection" id="selected_user_'.$visitor->id.'" name="option_'.$give->id.'_'.$index.'">';
                                foreach($givings as $g) {
                                    if($g->id == $gitem->giving_id){
                                        $htmlElement .= '<option value="'.$g->id.'" selected>'.$g->name.'</option>';    
                                    }else{
                                        $htmlElement .= '<option value="'.$g->id.'">'.$g->name.'</option>';    
                                    }
                                }
                                $htmlElement .='</select></span></div>';
                                $index = $index +1;   
                            }
                        }
                    }
                }

                if($hasoption == false){
                    $htmlElement .= '<span class="object_fund" data-id="'.$visitor->id.'" data-is="false"><div><input type="text" name="giving['.$visitor->id.'][]" class="input" value="" data-n="'.$givings[0]->id.'_1"></div>';
                    $htmlElement .= '<div><select class="selection" id="selected_user_'.$visitor->id.'" name="option_'.$givings[0]->id.'_1">';
                    foreach($givings as $g) {
                        $htmlElement .= '<option value="'.$g->id.'">'.$g->name.'</option>';
                    }
                    $htmlElement .='</select></span></div>';
                }

                $htmlElement .='<span id="add_more_container_'.$visitor->id.'"></span>';

                $htmlElement .='<a href="javascript:void(0)" onclick="add_more_fund('.$visitor->id.')">add another</a>';

                $row[] = $htmlElement;

                $row[] = get_popover_data_member($visitor,'notes');
                $output['aaData'][] = $row;

            }
        }
        
        echo json_encode( $output );
    }

    function giving_print() {
        
        $type = new Type();        
        $type->set_type( 6 );    
        $type->set_company_id( $this->user->company_id );
        $type->set_active( 1 );
     
        $givings = $this->model->get_object( $type );

        $date_giving = date('Y-m-d');
        
        $date_giving = (isset($_GET['giving_date']) and $_GET['giving_date'] !='') ? $this->input->get('giving_date') : $date_giving;        
        $start_date = (isset($_POST['start_date']) and $_POST['start_date'] !='') ? $this->input->post('start_date') : date('Y-m-d',  strtotime('-7 days'));
        $end_date = (isset($_POST['end_date']) and $_POST['end_date'] !='') ? $this->input->post('end_date') : date('Y-m-d');
        $view_type = (isset($_GET['view']) and $_GET['view'] != '')? $this->input->get('view') : 1;
        
            
        $this->load->library('funding_report');
        $company = $this->user->_company[0];
        $members = $this->input->post('visitors');
        $membersReport = array();
        $limit = (isset($_POST['DataTables_Table_0_length'])) ? $this->input->post('DataTables_Table_0_length') : 10;
        
        $type = new Type();        
        $type->set_type( 6 );        
        $type->set_company_id( $this->user->company_id );
        $type->set_active( 1 );
        $givings = $this->model->get_object( $type );
        
        if(!$members) {
            
            $filter = new Visitor();
            $filter->set_company_id( $this->user->company_id );
            $filter->set_person_type( Person::TYPE_MEMBER );
            if($view_type == 1)
                $filter->set_head_household( 1);
            $filter->set_order_by('first_name ASC');
            $visitors = $this->orm->get_object( $filter, false, array('first_name' => "REGEXP '^<first_name>'"),null,'Visits');
            if(count($visitors) > 0) {
                foreach( $visitors as $visitor ){
                    $memberInfo['person'] =  $visitor;
                    $givingInfo = array ();
                    if(isset($givings) and is_array($givings) and count($givings) > 0) {
                        foreach($givings as $give) {
                            $givingDate = $this->giving_model->getGivingMemberTotalDate($visitor->id ,$give->id,$start_date,$end_date) ;
                            $givingInfo[$give->name]  = $givingDate;
                        }
                    }
                    $memberInfo['giving'] =  $givingInfo;
                    $membersReport[] = $memberInfo;
                }
            }
            
        } else {
            foreach($members as $member_id) {
                $filter = new Visitor();
                $filter->set_company_id( $this->user->company_id );
                $filter->set_person_type( Person::TYPE_MEMBER );
                $filter->set_id($member_id);
                $member = $this->orm->get_object( $filter , true);
                
                $memberInfo['person'] =  $member;
                $givingInfo = array ();
                if(isset($givings) and is_array($givings) and count($givings) > 0) {
                    foreach($givings as $give) {
                        $givingDate = $this->giving_model->getGivingMemberTotalDate($member_id ,$give->id,$start_date,$end_date) ;
                        $givingInfo[$give->name]  = $givingDate;
                    }
                }
                $memberInfo['giving'] =  $givingInfo;
                $membersReport[] = $memberInfo;
                
            }
        }

        $filename = $this->get_local_path().self::PDF_GIVING_FOLDER.$this->funding_report->get_company_name($company);
        
        $primary_contact = $company->primary_contact;
            
        // $filter = new Visitor();
        // $filter->set_id($primary_contact);
        // $filter->set_person_type( Person::TYPE_USER );
        // $primaryContact = $this->model->get_object( $filter,true );
        $start_date = date('m/d/Y',  strtotime($start_date));
        $end_date = date('m/d/Y',  strtotime($end_date));
        $test = $this->funding_report->create_pdf( $company, $membersReport ,$start_date , $end_date , $filename);
        // $this->load->library('email');
        // $this->email->from( 'no-reply@visitorplus.net', 'No-Reply'); 
        // $this->email->to( $primaryContact->email ); 
        // $this->email->subject('Thank you for your giving contributions.  Here is your report.');
        // $this->email->message( 'Thank you for your loyalty to your giving.  Below you will find a PDF report with all of your details for the date range of:'.$start_date.' - '.$end_date);
        // $this->email->attach( $filename );       
        // $this->email->send();
        echo base_url().self::PDF_GIVING_FOLDER.$this->funding_report->get_company_name($company);die;
        //echo $this->funding_report->get_company_name($company);die(' lfse');
       // echo $pdf;

    }

}
