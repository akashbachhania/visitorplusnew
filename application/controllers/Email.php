<?php
class Email extends MY_Controller{
    
    const BATCH_EMAIL  = 'action_send';
    const BATCH_SAVE   = 'action_save';
    const BATCH_CANCEL = 'action_cancel';
    const BATCH_LETTER = 'action_letter';
    const BATCH_DELETE = 'action_delete';
    
    const STATUS_SENT   = 'sent';
    const STATUS_NOTSENT   = 'notsent';
    const STATUS_SAVED  = 'saved';
    const STATUS_ERROR  = 'error';
    const STATUS_FAILED = 'failed';
    const STATUS_UPDATE = 'update';
    
    const SCHEDULED_FIRST = 'first';
    const SCHEDULED_BIRTHDAY = 'bday';
    
    private $scheduled_email = false;
    
    public function __construct(){ 
        parent::__construct();  
        $this->authorize_or_redirect();
        $this->load->model('email_model','email_model', TRUE );
        $this->load->library('html_table');                                 
        $this->load->library('icontact_api');                                 
    }
    public function index() {
        
        $template = new Template(); 
        $param = $this->uri->segment(3);
         
        $show_event = true;
        if( preg_match('/\d+/',$param)){
            $template = $this->email_model->get_object( new Template( $param, $this->user->company_id ), true );    
        } else if( $param == 'batch_email'){  
            $visitor_ids = ($visitor_ids = $this->uri->segment(4)) ? (array)$visitor_ids : $this->input->post('visitors');
            if( is_array($visitor_ids) ){
                $checked_visitors = array();
                foreach( $visitor_ids as $id ){
                    if( $visitor = $this->email_model->get_object(new Visitor($id, $this->user->company_id),true)){
                        $checked_visitors[] = $id;
                    }
                }
                $template->set_mail_to( $checked_visitors );
            }
            $template->set_mail_from(
                $this->user->_company[0]->email ? 
                $this->user->_company[0]->email : 
                $this->user->email
            );
            $show_event = false;
        } else if( $param = 'batch_forward'){
            $visitor_ids = ($visitor_ids = $this->uri->segment(4)) ? (array)$visitor_ids : $this->input->post('visitors');
            
            $template->set_mail_from(
                $this->user->_company[0]->email ? 
                $this->user->_company[0]->email : 
                $this->user->email
            );
            $template->set_mail_subject('Visitor plus: Contact forward');
            $template->set_mail_body( $this->create_forward_body($visitor_ids));
            $show_event = false;
        }
        
        if($template->event){
            $this->scheduled_email = true;
        }
        if($this->scheduled_email) {
            $show_event = true;
        }
        
        $templates = $this->email_model->get_object( new Template(null, $this->user->company_id));
        $visitors  = $this->email_model->get_object( new Visitor(null,$this->user->company_id));
        
        foreach( $visitors as $visitor ){
            $select_visitors[ $visitor->id ] = $visitor->first_name .' '. $visitor->last_name;
        }        
       
        $list = array();
    
        if($this->user->vr_username !='' and $this->user->vr_password !='') {
            $instance = $this->initialize_vr();
            $list = $instance->getLists();
        }
       
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true));
        $this->load->view('email/email', array( 
            'template'   => $template, 
            'templates'  => $templates, 
            'status'     => null, 
            'template_id'=> $param,
            'visitors'      => $visitors,
            'select_visitors' => $select_visitors,
            'vertical_list' => $list,
            'scheduled'     => $this->scheduled_email, 
            'show_event'    => $show_event,
            'user_info'     => $this->user
            )
        );
        $this->load->view('include/footer');  
    }
    function icontact() {
        $selectedTemplate = $this->uri->segment(3,0);
        
        if($this->user->vr_username !='' and $this->user->vr_password !='') {
           
        } else {
            redirect('email/?message=no-icontact-credentials');
        }
        
        $list       = array();
        $messages   = array();
        $campigns   = array();
        $selectedMessage = false;
    
        $instance = $this->initialize_vr();
        $list = $instance->getLists();            
        $messages = $instance->getMessages('normal');
        $campigns = $instance->getCampaigns();
       
        if($selectedTemplate > 0 and is_array($messages) and count($messages) > 0) {            
            foreach ($messages as $object) {
                if($object->messageId == $selectedTemplate) {
                    $selectedMessage = $object;
                    break;
                }
            }
            
        }
        $visitors  = $this->email_model->get_object( new Visitor(null,$this->user->company_id));
        
        foreach( $visitors as $visitor ){
            $select_visitors[ $visitor->id ] = $visitor->first_name .' '. $visitor->last_name;
        } 
        
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true));
        $this->load->view('email/icontact', array(             
            'status'     => null, 
            'message_id'=> $selectedTemplate,            
            'vertical_list' => $list,
            'messages' => $messages,
            'campigns' => $campigns,
            'user_info'     => $this->user,
            'message_id'    => $selectedTemplate,
            'message' => $selectedMessage,
            'select_visitors' => $select_visitors,
            )
        );
        $this->load->view('include/footer');  
    }
    function get_icontact_messages() {
        $selectedTemplate = $this->input->post('id');
        
        if($this->user->vr_username !='' and $this->user->vr_password !='' and $selectedTemplate !='') {
           
        } else {
            redirect('email/?message=no-icontact-credentials');
        }
        
        $instance = $this->initialize_vr();   
        $messages = $instance->getMessages('normal');
        $arrayMessages = array('' => '~*~ Select ~*~');
        if(is_array($messages) and count($messages) > 0) {
            foreach ($messages as $object) {
                if($object->campaignId == $selectedTemplate)
                    $arrayMessages[$object->messageId] = $object->subject;
            }
        }
        
        echo form_dropdown('message_id', $arrayMessages, null, ' id="message_id" onchange="getMessage(this.value);" ');
    }
    function process_icontact() {
        if($this->user->vr_username !='' and $this->user->vr_password !='') {
           
        } else {
            redirect('email/?message=no-icontact-credentials');
        }
        
        $status = null;
        $errors = '';
        $instance = $this->initialize_vr();
        
        
     
        if( $this->input->post( self::BATCH_EMAIL )){
            
            
            $status = self::STATUS_ERROR;
            //STATUS_SENT
           // if( $template->validate()){          
                //$status = $this->send_email( $template );
           // }
            $camp_id        = $this->input->post('camp_id');
            $message_id     = $this->input->post('message_id');
            $list_name      = $this->input->post('list_name');
            $subject        = $this->input->post('mail_subject');
            $body           = $_POST['mail_body'];
            
            if($list_name !='') {
                 
                if($message_id !='' ) {
                    $instance->sendMessage($list_name, $message_id);
                    
                    $response = $instance->getLastResponse();
                    $response = json_decode($response);
                   
                    if(isset($response->warnings) and is_array($response->warnings)) {
                        $errors = implode('<br>', $response->warnings);
                        
                    } else {
                        $status = self::STATUS_SENT;
                       
                    }
                   
                } elseif($message_id == '' and $subject !='' and $body !='' and $camp_id !='') {
                   
                    $result_message = $instance->addMessage($subject, $camp_id, $body,$body);
                    
                    if($result_message and isset($result_message->messageId)) {
                        $instance->sendMessage($list_name, $result_message->messageId);
                        $response = $instance->getLastResponse();
                        $response = json_decode($response);
                        if(isset($response->warnings) and is_array($response->warnings)) {
                            $errors = implode('<br>', $response->warnings);

                        } else {
                            $status = self::STATUS_SENT;

                        }
                    }
                    
                }
            }
           
        } 
        
       
        $list       = array();
        $messages   = array();
        $campigns   = array();
        $selectedMessage = false;
    
       
        $list = $instance->getLists();            
        $messages = $instance->getMessages('normal');
        $campigns = $instance->getCampaigns();
       
        if($selectedTemplate > 0 and is_array($messages) and count($messages) > 0) {            
            foreach ($messages as $object) {
                if($object->messageId == $selectedTemplate) {
                    $selectedMessage = $object;
                    break;
                }
            }
            
        }
        
        $visitors  = $this->email_model->get_object( new Visitor(null,$this->user->company_id));
        
        foreach( $visitors as $visitor ){
            $select_visitors[ $visitor->id ] = $visitor->first_name .' '. $visitor->last_name;
        } 
        
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true));
        $this->load->view('email/icontact', array(             
            'status'     => null, 
            'message_id'=> $selectedTemplate,            
            'vertical_list' => $list,
            'messages' => $messages,
            'campigns' => $campigns,
            'user_info'     => $this->user,
            'status'     => $status, 
            'message_id'    => $selectedTemplate,
            'message' => $selectedMessage,
            'errors' => $errors ,
            'select_visitors' => $select_visitors,
            )
        );
        $this->load->view('include/footer');     
    }
    public function scheduled(){
        $this->scheduled_email = true;
        $this->index();
    }
    public function process(){
           
        if(!$template = $this->input_mapper->create_object_from_input('Template')){ 
            $template = new Template();
            $template->set_mail_from(
                $this->user->_company[0]->email ? 
                $this->user->_company[0]->email : 
                $this->user->email
            );
        }
        $template->set_company_id( $this->user->company_id ); 
        $status = null;
         
        if( $this->input->post( self::BATCH_EMAIL )){

            $status = self::STATUS_FAILED;
            if( $template->validate()){          
                $status = $this->send_email( $template );
            }
             
        } else if( $this->input->post( self::BATCH_SAVE )){
            if( $template->mail_from && $template->mail_subject ){
                $old_id = $template->id;
                $id = $this->email_model->save_object( $template );
                $template->set_id( $id );
                $status = $old_id == $id ? self::STATUS_UPDATE: self::STATUS_SAVED; 
            } else {
                $status = self::STATUS_ERROR;
            }           
        } else if( $this->input->post( self::BATCH_CANCEL )){
            header('Location: '.site_url('dashboard'));
        } else if( $this->input->post( self::BATCH_DELETE )){
            
        }
        
        if($template->event){
           $this->scheduled_email = true;
        }
        $templates = $this->email_model->get_object( new Template(null, $this->user->company_id));
        $visitors  = $this->email_model->get_object( new Visitor(null,$this->user->company_id)); 
 
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true ));
        $this->load->view('email/email', array( 
            'template'   => $template, 
            'templates'  => $templates, 
            'status'     => $status, 
            'template_id'=> $template->id, 
            'visitors'   => $visitors,  
            'scheduled'  => $this->scheduled_email
            )
        );
        $this->load->view('include/footer');        
    }
    
    public function send_email(Template $template ){ 
        $this->load->library('email'); 
        $this->load->library('mailer');        
        $this->mailer->initialize($this->email, $this->get_mail_config());
        
        $visitor_recipients = $template->get_visitor_recipients();
        $anyonymous_recipients = $template->get_anonymous_recipients();
        if($anyonymous_recipients){
           $template->add_recipients($anyonymous_recipients);            
           if( $visitor_recipients ){
               $template->add_recipients( $this->email_model->get_recipients($visitor_recipients));
           }
        } else {
           $template->add_recipients( $this->email_model->get_recipients($visitor_recipients));    
        }
                 
        $sent = $this->mailer->send_template($template); 
        $this->email_model->system_log(
            'Email status',
            $this->mailer->get_logs()
        );
        return $sent ? self::STATUS_SENT : self::STATUS_NOTSENT;    
    }
    
    public function ajax_get_emails(){
        $filter = new Visitor();
        $filter->set_company_id( $this->user->company_id );
        
        $users = $this->email_model->get_object($filter);
        $array = array();
        foreach( $users as $user ){
            $array[] ="$user->first_name $user->last_name <$user->email>";            
        }
        echo json_encode($array);
    }
    
    public function ajax_template(){
        $status = 'fail'; 
        if( ($type = $this->input->post('type')) && ($id = $this->input->post('id'))){
            switch($type){
                case 2:
                    if( $template = $this->email_model->get_object( new Template($id, $this->user->company_id), true)){
                        if( $this->send_email( $template )){
                            $status = 'success';
                        } 
                    }
                break;
                case 1:
                    if( $template = $this->email_model->get_object( new Template($id, $this->user->company_id), true)){
                        $this->email_model->delete_object( $template );
                        $status = 'success';
                    }                
                break;
            }
        }
        echo json_encode(array('status'=>$status));
    }
    
    public function ajax_get_church_emails (){ 
         
        $this->user = $this->email_model->get_object( $this->user, true );
        $mails[] = "{$this->user->_company->name} <{$this->user->_company->email}>";
        $mails[] = "{$this->user->name}<{$this->user->email}>";
         
        echo json_encode($mails);
    }
    
    public function ajax_add_new_list(){
        if( $list_name = $this->input->post('list_name')){
            
            if($this->user->vr_username !='' and $this->user->vr_password !='') {
                $instance = $this->initialize_vr();
                if($objectNewList = $instance->addList($list_name)){
                    
                    echo json_encode(
                        array(
                            'id'     => $objectNewList->listId,
                            'status' => 'success'
                        )
                    );
                } 
            }
            
            
        }
    }
    
    public function ajax_add_list_members(){
        if( ($ids = $this->input->post('ids')) && ($lid = $this->input->post('list_id')) ){
            $ids = explode('-',$ids);
            $visitors = array();
            foreach( $ids as $id){
                if( $id && ($visitor = $this->email_model->get_object(new Visitor($id,$this->user->company_id), true))){
                    $visitors[] = $visitor;
                }
            }
            $status = 'fail';
           
            if($this->user->vr_username !='' and $this->user->vr_password !='') {
                    $instance = $this->initialize_vr();
                    
                    foreach($visitors as $visitor) {
                        $first_name = $visitor->first_name;
                        $last_name =  $visitor->last_name;
                        $email =  $visitor->email;
                        
                        if($email !='') {
                            
                            $response = $instance->getContactWithEmail($email);
                            
                            if(!$response) {
                                $addContactResponse = $instance->addContact($email, 'normal', null, $first_name, $last_name);
                                 $instance->subscribeContactToList($addContactResponse->contactId,$lid);
                            } else {
                                 $contactId = $response->contactId;
                                 $instance->subscribeContactToList($contactId,$lid);
                            }
                            
                           $status = 'success';
                            
                        }
                    }
                    
                    echo json_encode(
                            array(
                                'status' => $status
                            )
                        );
            }
        }
    }    
    
    private function initialize_vr(){
       
        $instance = $this->icontact_api->initialize(
            $this->config->item('iconact_app_id'),
            $this->user->vr_username,
            $this->user->vr_password
        );    
        try{
                $instance->getLists();
                return $instance;
            } catch (Exception $ex) {
               return false;
            }
    }
    
    private function create_forward_body( $visitor_ids ){
        $body = '';
        if( is_array( $visitor_ids)){
            foreach($visitor_ids as $id){
                $visitor = $this->email_model->get_objects_with_related(
                    new Visitor($id,$this->user->company_id),
                    array( 
                        'Visits',
                        'Relations', 
                        'Assignment_log',
                        'Note'
                    )            
                );                
                if( $visitor ){
                    
                    $fwd_array = array();
                    $fwd_array[] = '<strong>'.$visitor->first_name.' '. $visitor->last_name.'</strong>';
                    
                    $address = array();
                    if($visitor->address) $address[] = $visitor->address;
                    if($visitor->city)    $address[] = $visitor->city;
                    if($visitor->zip)     $address[] = $visitor->zip;
                     
                    $fwd_array[] = implode(', ', $address);
                    
                    if( $visitor->phone_1 ){
                        $fwd_array[] = $visitor->phone_1;
                        
                    }
                    if( $visitor->email ){
                        $fwd_array[] = $visitor->email;
                        $fwd_array[] = '<br>';
                    }
                    if( $visitor->birth ){
                        $fwd_array[] = '<strong>Birth Date:</strong>'.substr($visitor->birth,5,2).'-'.substr($visitor->birth,8,2).'-'.substr($visitor->birth,0,4).'';
                    }                    
                    if( $visitor->gender ){
                        $fwd_array[] = '<strong>Gender:</strong>'.ucfirst( $visitor->gender ).'';
                    }

                    if( $visitor->spouse ){
                        $fwd_array[] = '<strong>Spouse:</strong>'.$visitor->spouse.'';
                    }
                    
                    if( $visitor->fax ){
                        $fwd_array[] = '<strong>Fax:</strong>'.$visitor->fax.'';
                    }
                                         
                    if( $visitor->_note && is_array($visitor->_note) ){
                        $fwd_array[] = '<strong>Notes:</strong>';
                        foreach( $visitor->_note as $note){
                            $fwd_array[] = nl2br($note->note);    
                        }
                    }

                    /**
                    * put your comment there...
                    * 
                    * @var Visitor
                    */
                    $visitor;
                    $body .= implode( '<br>', $fwd_array);
                    $body .= '<br><br>';
                }
            }        
        }
        return $body;
    }
    
    
}