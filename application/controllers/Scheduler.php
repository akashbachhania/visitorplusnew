<?php
class Scheduler extends MY_Controller{
    public function __construct(){
        parent::__construct();  
        $this->load->model('email_model','scheduler_model', TRUE );    
    }
    
    public function send_birthday_templates(){
        $today = gmdate('Y-m-d');
        $templates = $this->scheduler_model->get_templates(Template::EVENT_BIRTHDAY);
        foreach( $templates as $template ){
            $log = $this->scheduler_model->get_object(
                new TemplateLog(null,$template_id),
                true
            );
            
            if( !$log ){
                $log = new TemplateLog(null,$template_id);
            }
            
            if( $log->datetime != $today ){
                $visitors = $this->scheduler_model->get_recipients(
                    $template->get_visitor_recipients()
                );

                foreach($visitors as $visitor){                    
                    if( $visitor->is_bday_today() ){
                        $recipients[] = $visitor;
                    }
                }
                $this->send_template($template, $recipients);
                $log->set_datetime($today);
                $this->scheduler_model->save_object($log);
            }
        }
    }
    
    public function send_new_member_template(Visitor $visitor){
    
    }
    
    public function send_template(Template $template, $recipients){
        $this->load->library('email');
        $config = $this->get_mail_config();
        $this->email->initialize( $config );
        $this->email->from($template->mail_from); 
        
        foreach( $recipients as $recipient){
            $message = $this->compose($recipient, $template);
            $this->email->to( $recipient->email ); 
            $this->email->subject( $template->mail_subject );
            $this->email->message( $message );               
            $this->email->send();                    
        }        
    }

}
