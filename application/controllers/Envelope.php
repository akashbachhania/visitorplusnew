<?php 
require_once(APPPATH.'controllers/paper.php');
class Envelope extends Paper{
    
    public function __construct(){
        parent::__construct();  
        $this->order_type = Order::ORDER_LETTER;        
    }
}
