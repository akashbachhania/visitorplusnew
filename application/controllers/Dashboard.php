<?php
class Dashboard extends MY_Controller{
    public function __construct(){
             
        parent::__construct();  
        $this->authorize_or_redirect();
        $this->load->model('dashboard_model','dashboard_model', TRUE );       
        $this->load->library('icontact_api');  
        $this->load->library('order_helper');          
    }

    public function index(){
        $start_date = ($start_date = $this->input->post('start_date')) ? $start_date : gmdate('m/d/Y', time()-7*24*60*60);
        $end_date   = ($end_date = $this->input->post('end_date'))     ? $end_date   : gmdate('m/d/Y');
       
        $filter_start = new Visits();
        $filter_start->set_datetime($start_date);
        $filter_start->set_company_id( $this->user->company_id );
        $filter_start->set_is_member(0);
        
        $filter_end = new Visits();
        $filter_end->set_datetime($end_date);
        $filter_end->set_company_id( $this->user->company_id );       
        $filter_end->set_is_member(0);
        
        /**
        * Fetch all visits for Currenct company
        * that are between start date and and date
        */
       
        $stats =  $this->dashboard_model->get_object( array( $filter_start, $filter_end), false, 
                                                      array( 
                                                            '0'=> array('datetime'=> Orm::REL_GREAT_E),
                                                            '1'=> array('datetime'=> Orm::REL_LESS_E)
                                                           ), 
                                                      Orm::REL_BINDING_AND );
       
        $total  = count( $stats);
        $male   = 0;
        $female = 0;
        foreach( $stats as $stat ){
            if( $stat->_visitor[0]->gender == Visitor::GENDER_MALE){
                $male++;
            } else {
                $female++;
            }
        }
        
        $male_perc   = round($male ? $male/$total*100 : 0,0);
        $female_perc = round($female ? $female/$total*100:0,0);
        
        /* Member statistics */
        $filter_start = new Visits();
        $filter_start->set_datetime($start_date);
        $filter_start->set_company_id( $this->user->company_id );
        $filter_start->set_is_member(true);
        
        $filter_end = new Visits();
        $filter_end->set_datetime($end_date);
        $filter_end->set_company_id( $this->user->company_id );     
        $filter_end->set_is_member(true);
       
        /**
        * Fetch all visits for Currenct company
        * that are between start date and and date
        */
        
        $person_object = new Person (null , $this->user->company_id , Person::TYPE_MEMBER);
        
        $statsM =  $this->dashboard_model->get_object( array( $filter_start, $filter_end), false, 
                                                      array( 
                                                            '0'=> array('datetime'=> Orm::REL_GREAT_E),
                                                            '1'=> array('datetime'=> Orm::REL_LESS_E)
                                                           ), 
                                                      Orm::REL_BINDING_AND );
        
        
        $totalM  = count( $statsM);
        $maleM   = 0;
        $femaleM = 0;
        unset($stat);
        $unique_member = array();
        foreach( $statsM as $stat ){
            $member = new Member ($stat->visitor_id , $this->user->company_id);
            $member = $this->model->get_object( $member , true);
            
            $unique_member[] = $stat->visitor_id;
            if( $member->gender == Visitor::GENDER_MALE){
                $maleM++;
            } else {
                $femaleM++;
            }
        }
        $unique_member = array_unique($unique_member);
        $maleM_perc   = round($maleM ? $maleM/$totalM*100 : 0,0);
        $femaleM_perc = round($femaleM ? $femaleM/$totalM*100:0,0);
        //echo '<pre>';print_r($this->user);die;
        /* Member Statistics End */
        
        
        $this->load->view('include/sidebar', array('user'=>$this->user, 'load_charts' => true));
        $this->load->view('dashboard/dashboard', 
            array(
                'user'  => $this->user,
                'visits'=> $stats, 
                'male'  => $male_perc,
                'female'=> $female_perc,
                'total' => $total,
                'male_M'  => $maleM_perc,
                'female_M'=> $femaleM_perc,
                'total_M' => $totalM,
                'total_member' => count($unique_member),
                'start_date' => $start_date,
                'end_date'   => $end_date,
                )
            );
        $this->load->view('include/footer');
    }

    public function settings(){
        $saved = null;
        $error = '';
        $path = parse_url( site_url());
         
        $path =  str_replace('index.php','',$path['path']) ; 
        $path = $_SERVER['DOCUMENT_ROOT'] .  $path  . 'application/views/assets/img/';
        
        $filename = $this->user->company_id.'_logo';
        $file     = null;
        $primary_contact = (isset($this->user->_company[0]->primary_contact) and $this->user->_company[0]->primary_contact !='') ? $this->user->_company[0]->primary_contact : 0;
        if( $this->input->post('save') ){
            if( $this->input->post('password') == $this->input->post('password_2')){
                $filter = new Visitor();
                $filter->set_email($this->input->post('username'));
                $filter->set_person_type( Person::TYPE_USER );
                $taken = false;
                if( $visitors = $this->model->get_object( $filter )){
                    foreach( $visitors as $visitor)    {
                        if( $visitor->id != $this->user->id ){
                            $error = 'User with this email already exists';
                            $taken = true;
                        }
                    }
                }
                
                if( !$taken ){
                    $this->user->email       = $this->input->post('username');
                    $this->user->password    = $this->input->post('password');
                    $this->user->vr_username = $this->input->post('vr_username');
                    $this->user->vr_password = $this->input->post('vr_password');
                    $this->user->default_list = $this->input->post('default_list');
                    
                    if($primary_contact == $this->user->id) {
                        $this->user->text_contacts = $this->input->post('text_contacts');
                        $this->user->send_emails = $this->input->post('send_emails');
                        
                        $grant_success = $this->input->post('grant_access');
                        $this->db->where('company_id',$this->user->company_id);
                        $this->db->delete('dr_funds_members');
                        if(is_array($grant_success) and count($grant_success) > 0) {
                            foreach($grant_success as $mem) {
                                $this->db->insert('dr_funds_members', array('company_id' => $this->user->company_id , 'user_id' => $mem));
                            }
                        }
                    }
                    $this->model->save_object( $this->user );
                    $saved = true;                    
                }
               
            }  else {
                $error = 'Please make sure that passwords match';
                $saved = false;
            }
        } 

        /* Image Logo upload
        $file = null;
        if( file_exists( $path. $filename)){
            $file = str_replace('index.php','',site_url() .'application/views/assets/img/' . $filename);
        }*/

        $users = array();
        $funds = array();
        if($this->user->id == $primary_contact){
            $this->load->library('order_helper');          
            $person_filter = new Person();
            $person_filter->set_company_id( $this->user->company_id );         
            $person_filter->set_person_type( Person::TYPE_USER );
            $users = $this->model->get_object( $person_filter );            
        }
        
        $fundsUsers = $this->dashboard_model->getGrantMembers($this->user->company_id) ;
        if($fundsUsers) {
            foreach($fundsUsers as $fund) {
                $funds[] = $fund->user_id;
            }
        }
       
        $list = array();
    
        if($this->user->vr_username !='' and $this->user->vr_password !='') {
            $instance = $this->initialize_vr();
            if($instance)
                $list = $instance->getLists();
            
        }
        
        $type = new Type();        
        $type->set_type( 6 );        
        $type->set_company_id( $this->user->company_id );
        $givings = $this->model->get_object( $type );

        $this->load->view('common/header', array('fluid_layout'=>false,'dont_display_header' =>true, 'companies'=>$this->companies,'saved'=>$saved));
        $this->load->view('dashboard/settings', array('userLive' => $this->user,'funds' => $funds, 'giving' => $givings , 'file'=>$file, 'error_msg' => $error,'primary_contact' => $primary_contact,'users'=>$users,'vertical_list' => $list,));
        $this->load->view('common/footer');
       
    } 
    private function initialize_vr(){
       
        $instance = $this->icontact_api->initialize(
            $this->config->item('iconact_app_id'),
            $this->user->vr_username,
            $this->user->vr_password
        );    
        try{
                $instance->getLists();
                return $instance;
            } catch (Exception $ex) {
               return false;
            }
    }
    public function add_new(){
         
        /**
        * Type of new object
        */
        $class_name = $this->uri->segment(3,0);
        /**
        * Company id (Only for new USER)
        */
        $company_id = $this->uri->segment(4,0);
        /**
        * Show user -> click on Coordinators table entry
        * 
        * 
        */
        
        $primary_contact = (isset($this->user->_company[0]->primary_contact) and $this->user->_company[0]->primary_contact !='') ? $this->user->_company[0]->primary_contact : 0;
        if($this->user->id != $primary_contact){
            redirect('login');
        }
        $this->load->library('order_mapper', array('input'=>$this->input));          
        
        $show_user = $this->uri->segment(5,0);
        
        $date = array();
        if( $this->input->post('save') && in_array( $class_name, array('Person') ) ){
            
            $object = $this->order_mapper->create_object_from_input($class_name);            
            
            if( $object->validate()){
                $old_id = $object->id;
                $object->set_id($this->model->save_object( $object ));
                if( $old_id != $object->id ){
                    $saved = true;    
                } else {
                    $saved = 'update';
                }
            } else {
                $saved = false;
            } 
            $data  = array('object' => $object, 'saved' => $saved, 'action' => site_url('dashboard/add_new/'.$class_name));                 
            
        }  else {
            /**
            * User clicked ADD NEW 
            */
            if( $show_user ){
              $filter = new Person();
              //company_id is user_id in this instance
              $filter->set_id( $company_id );
              $users = $this->model->get_object( $filter );
              $user = new Person();
              if( $users ){
                  $user = reset($users);
              }
              $data = array( 'object' => $user, 'action' => site_url('dashboard/add_new/'. $class_name) );
            } else if( in_array( $class_name, array('Person'))){
                $object = new $class_name();
                if( $class_name == 'Person'){
                    $object->set_company_id( $company_id );
                    $object->set_person_type( Person::TYPE_USER );
                }
                $data = array( 'object' => $object, 'action' => site_url('dashboard/add_new/'. $class_name) );                
            } 
        }
        
        $data['states'] = $this->model->get_us_states();
        $this->load->view('common/header', array('fluid_layout'=>true, 'user'=>$this->user,'dont_display_header'=>true));             
        $this->load->view('common/add_new', $data );             
        $this->load->view('common/footer');           
    }
    public function new_users(){
        $emails = split(',', $this->input->post('emails'));
        if( $emails ){
            foreach( $emails as $email ){
            
                if( !$this->is_valid_email($email)) continue;
                
                $visitor = new Visitor();
                $visitor->set_email($email);
                $visitor->set_company_id($this->user->company_id);
                $visitor->set_person_type(Person::TYPE_USER);
                
                if( !$existing_visitor = $this->model->get_object($visitor,true)){
                    $visitor->set_password(md5(rand(0,100000).time().$email));
                    $visitor->set_person_type(Person::TYPE_USER);
                    $this->model->save_object($visitor);
                    $this->send_new_visitor_email($visitor);
                } else {
                    $this->send_new_visitor_email($existing_visitor);                
                }
            }
        }
        
        $this->load->view('common/header',      array('fluid_layout'=>false,'dont_display_header' =>true, 'companies'=>$this->companies,'saved'=>$saved));
        $this->load->view('dashboard/settings', array('user' => $this->user, 'file'=>$file, 'error_msg' => $error));
        $this->load->view('common/footer');        
    }
    
    protected function is_valid_email($email){
        return preg_match('![^@]+@[^@]+!',$email) ? true : false;
    }
    
    protected function send_new_visitor_email($visitor){
        $this->load->library('email'); 
        $this->load->library('mailer');        
        $this->mailer->initialize($this->email, $this->get_mail_config());
        
        $body = 
        'User account has been create for you at VisitorPlus.net<br>'.
        'Plese follow this link to log in: ' .site_url('login').'<br>'.
        'Username: '.$visitor->email.'<br>'.
        'Password: '.$visitor->password.'<br>';
        
        $template = new Template();
        $template->add_recipients(array($visitor)); 
        $template->set_mail_from( $this->user->email );
        $template->set_mail_body($body);
        $template->set_mail_subject('VisitorPlus User account details');
                 
        $sent = $this->mailer->send_template($template); 
    }
    
    
    public function get_feed(){
        $use_timestamp = $this->input->post('use_timestamp');
         
        $start_date = $this->get_eu_date($this->input->post('start_date'));
        $end_date   = $this->get_eu_date($this->input->post('end_date'));
        
        if( $use_timestamp == 'false' ){
            if( $end_date == gmdate('Y-m-d')){
                $end_date .=' '.gmdate('H:i:s'); 
            }            
            $this->session->set_userdata('start_date', $end_date);
            $this->session->set_userdata('used_notes', array());
            $this->session->set_userdata('used_visitors', array());
        } else {
            $start_date = $this->session->userdata('start_date');
            $end_date   = gmdate('Y-m-d H:i:s');
            
            $this->session->set_userdata('start_date', $end_date);
        }

        $used_notes       = $this->session->userdata('used_notes');
        $used_assignments = $this->session->userdata('used_visitors');
                
        $note_start = new Note(null,$this->user->company_id);
        $note_start->set_change_time($start_date);
        $note_end   = new Note(null,$this->user->company_id);
        $note_end->set_change_time($end_date);
        
        $notes = $this->get_data($note_start, $note_end);

        $assignments_filter = new Assignment_log();
        $assignments_filter->set_change_time($end_date);
        $assignments_filter->set_company_id($this->user->company_id);
        
        $assignments_filter_start = new Assignment_log();
        $assignments_filter_start->set_change_time($start_date);
        $assignments_filter_start->set_company_id($this->user->company_id);
                
        $assignments = $this->get_data($assignments_filter_start, $assignments_filter);
        $data = array();
        foreach($assignments as $assignment ){
            if(in_array($assignment->id, $used_assignments)) continue;
            
            if(!isset($fetched_visitors[$assignment->visitor_id])){
                $fetched_visitors[$assignment->visitor_id] = $this->model->get_object(new Visitor($assignment->visitor_id, $this->user->company_id), true);    
                if(!$fetched_visitors[$assignment->visitor_id]) continue;
            }
            
            $data[strtotime($assignment->change_time)][] = array(
                'name' => $fetched_visitors[$assignment->visitor_id]->first_name.' '.$fetched_visitors[$assignment->visitor_id]->last_name,
                'type' => 'New Assignment',
                'text' => $assignment->_type[0]->name,
                'timestamp' => $this->get_us_date_format($assignment->change_time)
            );
            
            $used_assignments[] = $assignment->id;
        }
        
        foreach($notes as $note){
            if(in_array($note->id, $used_notes)) continue;
            
            if(!isset($fetched_visitors[$note->visitor_id])){
                $fetched_visitors[$note->visitor_id] = $this->model->get_object(new Visitor($note->visitor_id,  $this->user->company_id),true);    
                if(!$fetched_visitors[$note->visitor_id]) continue;
            }
             
            $data[strtotime($note->change_time)][] = array(
                'name' => $fetched_visitors[$note->visitor_id]->first_name.' '.$fetched_visitors[$note->visitor_id]->last_name,
                'type' => 'New Note',
                'text' => $note->note,
                'timestamp' => $this->get_us_date_format($note->change_time)
            );                
            $used_notes[] = $note->id;            
        }
        
        $this->session->set_userdata('used_notes',    $used_notes);
        $this->session->set_userdata('used_visitors', $used_assignments);
                
        if( $data ){
            ksort($data);
            $ret = array();
            foreach($data as $timestamp=>$arrays){
                foreach($arrays as $array){
                    $ret[] = $array;    
                }
            }
            echo json_encode(
                array(
                    'status' => 'success',
                    'data'   => $ret
                )
            );
            
        } else {
            echo json_encode(
                array(
                    'status' => 'false'
                )
            );            
        }
    }
    
    protected function get_data( $start, $end){
        return $this->model->get_object( 
            array( $start, $end ), false, 
            array( 
                '0' => array('change_time' => Orm::REL_GREAT_E),
                '1' => array('change_time' => Orm::REL_LESS_E )
            
            ),
            Orm::REL_BINDING_AND
        );    
    }
    
    protected function parse_notes($visitor, $timestamp){
        $notes = array();
        if(preg_match_all('!\[(?P<month>\d+)-(?P<day>\d+)-(?P<year>\d+) (?P<time>\d+:\d+:\d+).+?\]\s+(?P<notes>[^\]\[]+)!s',$visitor->notes, $matches)){
            foreach( $matches['notes'] as $index=>$note ){
                $note_timestamp = $matches['year'][$index].'-'.$matches['month'][$index].'-'.$matches['day'][$index].' '.$matches['time'][$index];
                if( strtotime($note_timestamp)> strtotime($timestamp)){
                    $notes[$note_timestamp] = $matches['notes'][$index];
                }
            }
        }      
        return $notes;
    }
    
    protected function get_timestamp( $timestamp ){
        return $timestamp;
    }
    
    protected function get_eu_date( $date ){
        $date = str_replace('/','-',$date);
        if(preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches )){
            $date = $matches['3'].'-'.$matches['1'].'-'.$matches['2'];
        }
        return $date;  
    }    
}
