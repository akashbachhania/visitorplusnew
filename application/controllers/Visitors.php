<?php
class Visitors extends MY_Controller{
    
    const BATCH_EMAIL = 'Email';
    const BATCH_TEXT  = 'Text';
    const BATCH_POSTCARD = 'Postcard';
    const BATCH_LETTER = 'Letter';
    
    const POSTCARD = '2';
    const LETTER   = '3';
    const FORWARD  = '4';
    const MAIL     = '1';
    const DELETE     = '5';
    
    const STATUS_UPDATE = 'update';
    const STATUS_ERROR  = 'error';
    const STATUS_SAVED  = 'saved';
    /**
    * @var visitors_model
    */
    public $orm;
    
    public function __construct(){
             
        parent::__construct();  
        $this->authorize_or_redirect();
        $this->load->model('visitors_model','orm', TRUE );
        $this->orm->set_user($this->user);
        $this->load->library('icontact_api');     
    }

    public function index( $first_letter = null, $selected = array(),  $status = '', $sort_descending = true ){
        if( $first_letter==null && (!($first_letter = $this->uri->segment(3)) || !preg_match('/^[a-zA-Z]{1}$/',$first_letter))){
            $first_letter = 'ALL';
        }

        $states = $this->orm->get_us_states(); 
        $languages = array(''=>'Select Item','add_new'=>'Add Item')+$this->orm->get_languages( $this->user->company_id ); 
        $months = $this->get_months();
        $days   = $this->get_days();
        $years  = $this->get_years();

        $this->load->view('include/sidebar', array('user'=>$this->user, 'visitor_menu'=>true, 'no_new_user'=>true , 'jquery_ui' => true));
        $this->load->view('visitors/visitors', array('user'     => $this->user,
                                                     
                                                     'selected_letter' => $first_letter, 
                                                     'selected_user'   => $selected,
                                                     'sort_descending' => $sort_descending,
                                                     'states'   => $states, 
                                                     'languages'=> $languages, 
                                                     'years'=>$years, 
                                                     'months'=>$months, 
                                                     'days'=>$days,
                                                     'status' => $status ));
        $this->load->view('include/footer');
    }

    function ajax_loading() {
        if( $first_letter==null && (!($first_letter = $this->uri->segment(3)) || !preg_match('/^[a-zA-Z]{1}$/',$first_letter))){
            $first_letter = 'ALL';
        }  
        
        $columnOrder = (isset($_GET['iSortCol_0']) and $_GET['iSortCol_0'] !='') ? $this->input->get('iSortCol_0') : 1;
        $order     = (isset($_GET['sSortDir_0']) and $_GET['sSortDir_0'] !='') ? $this->input->get('sSortDir_0') : 'ASC';
        
        $search = $this->input->get('sSearch');
        
        $orderBy = ' order by visit.datetime DESC';
        
        if($columnOrder == 2) {
            $orderBy = ' order by per.first_name '.$order;
        }elseif($columnOrder == 3) {
            $orderBy = ' order by per.last_name '.$order;
        } 
        
        $searchQuery = '';
        if($search != '') {
            $searchQuery = ' AND (per.first_name like "%'.$search.'%" or per.last_name like "%'.$search.'%")';
        }
        if($first_letter != 'ALL') {
            $searchQuery = ' AND (per.first_name like "'.$first_letter.'%")';
        }
        
        $sLimit = 0;
        $offset = 0;        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $offset = $this->input->get('iDisplayStart');
            $sLimit = $this->input->get('iDisplayLength');
        }       
       $sLimit      = ($sLimit == 0 ) ? 10 : $sLimit;
        
        $queryOne = 'SELECT distinct per.id,visit.datetime
                    FROM dr_persons as per
                    LEFT JOIN dr_visits as visit
                    ON per.id = visit.visitor_id
                    WHERE per.company_id = '.$this->user->company_id.'
                    AND per.person_type = '.Person::TYPE_VISITOR.'
                    '.$searchQuery.'
                    
                    '.$orderBy.'
                    limit '.$offset.', '.$sLimit.';';
       
       $queryTwo = 'SELECT per.id
                    FROM dr_persons as per
                    LEFT JOIN dr_visits as visit
                    ON per.id = visit.visitor_id
                    WHERE per.company_id = '.$this->user->company_id.'
                    AND per.person_type = '.Person::TYPE_VISITOR.'
                    '.$searchQuery.'
                    group by per.id';
       
        $resultTwo = $this->db->query($queryTwo);
        $rowTotal = $resultTwo->num_rows();
        
        $result = $this->db->query($queryOne);
        $visitors = $result->result();
        
        $this->load->helper('custom_helper');
        
        $output = array(
    		"sEcho" => intval($_GET['sEcho']),
    		"iTotalRecords" => $rowTotal,
    		"iTotalDisplayRecords" =>$rowTotal,
    		"aaData" => array()
    	);
            
        if(count($visitors) > 0) {
             foreach( $visitors as $person ){
                $row = array();
                $attr = '';
                
                $visitor = new Visitor ($person->id , $this->user->company_id);
                $visitor = $this->model->get_object( $visitor , true,array(),null,'Visits');
                
                $row['DT_RowClass'] = $attr;
                $row[] = form_checkbox( 'visitors[]', $visitor->id, isset($selected_user[$visitor->id]) ? true: false );
                $row[] = get_popover_data_loading($visitor, 'visit_date');
                $row[] = get_href($visitor->first_name, $visitor->id);
                $row[] = get_href($visitor->last_name, $visitor->id);
                $row[] = get_href($visitor->address, $visitor->id);
                $row[] = $visitor->city;
                $row[] = get_href($visitor->phone_1, $visitor->id);
                $row[] = get_href($visitor->email, $visitor->id);
                $row[] = get_href($visitor->__age, $visitor->id);
                $row[] = get_popover_data_loading($visitor,'notes');
                // $row[] = '<div id="div_visitor_'.$visitor->id.'" class="btn-toolbar" style="margin: 0;">
                //         <div class="btn-group">
                //         <a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" role="button">Action <span class="caret"></span></a>
                //             <ul class="dropdown-menu">
                //                 <li>
                //                     <a id="'.$visitor->id.'" onclick="visitor(1,'.$visitor->id.')" href="#">Email</a>
                //                 </li>
                //                 <li>
                //                     <a id="'.$visitor->id.'" onclick="visitor(2,'.$visitor->id.')" href="#">Postcard</a>
                //                 </li>                            
                //             </ul>
                //         </div>
                //     </div>';

                $output['aaData'][] = $row;
            }
        }       

        echo json_encode( $output );
    }

    private function sort_visitors($visitors, $sort_descending = true){
        $visitors = (array)$visitors;
        $dates    = array();
        foreach($visitors as $visitor){
            $visit_dates = array();
            foreach( $visitor->_visits as $visit){
                $visit_dates[] = $visit->datetime;
            }
            sort($visit_dates);
            $dates[ end($visit_dates)][] = $visitor;
        }
        ksort($dates);
        if( $sort_descending ){
            $dates = array_reverse($dates);    
        }
        $sorted_visitors = array();
        foreach( $dates as $date ){
            foreach( $date as $visitor){
                $sorted_visitors[] = $visitor;
            }
        }
        return $sorted_visitors;
    }

    public function index_sort(){
        $this->index(null,array(),'', ($this->uri->segment(3) == 'true' ? false : true) );
    }

    public function new_visitor(){
        $header = 'header';
        $visitor_menu = 'visitor_menu';
        if( $this->uri->segment(3) == 'no_header'){
            $header = 'dont_display_header';
            $visitor_menu = 'no_menu';
        } 
        
        $done_by_added = false;
        if( $this->uri->segment(4) == 'done_by_added' || $this->input->post('done_by_added')){
            $done_by_added = true;
        }
        
        $visitor = new Visitor();
        $status  = '' ;
        
        /*//popup code start
        $selected = 0;
        $saved = null; 
        $type = new Type();
        $type->set_type( 4 );
        $type->set_company_id( $this->user->company_id );
        $typesFilter = $type;
        $control_id = 'new_assignment_doneby_person_id';
        if( $this->input->post('save_p')){
            $type = $this->input_mapper->create_object_from_input( 'Type',Input_mapper::SOURCE_POST);
            if( in_array($type->type, array( Type::TYPE_ASSIGNMENT, Type::TYPE_LANGUAGE, Type::TYPE_REFERER, Type::TYPE_DONE_BY, Type::TYPE_MINISTRY, Type::TYPE_FUNDS)) && $type->validate()){
                $control_id = $this->input->post('control_id');
                $id = $type->id;
                $type->set_company_id( $this->user->company_id );                
                if($id == ($new_id = $this->orm->save_object($type)) ){
                    $saved = 'update';
                    $selected = $id;
                } else {
                    $saved = true;
                    $type = new Type();
                    $type->set_type( $type_id );
                    $type->set_company_id( $this->user->company_id );
                }                  
            } else {
                $saved = false;
            }
        }

        $types = $this->orm->get_object( $typesFilter );*/
        //popup code end

        $data['type'] = $type;
        $data['types'] = $types;
        $data['control_id'] = $control_id;
        $data['saved'] = $saved;
        $data['selected'] = $selected;


        if( $this->input->post('save')){
            $visitor = $this->marshall_visitor();
            
            $visitor->set_phone_1( $this->input->post('phone_1').$this->input->post('phone_2').$this->input->post('phone_3'));
            if( $this->input->post('month') != '-'){
                $visitor->set_birth( $this->input->post('month').'-'.$this->input->post('day').'-'.$this->input->post('year'));    
            } else {
                $visitor->set_birth('');
            }
                          
            if( $visitor->validate()){
                $old_id = $visitor->id;    
                if($old_id == '') {
                    $visitor->set_registered_date( date('Y-m-d H:i:s') );   
                    
                    $first_name = $this->input->post('first_name');
                    $last_name =  $this->input->post('last_name');
                    $email =  $this->input->post('email');
                    
                    if($this->user->vr_username !='' and $this->user->vr_password !='' and $this->user->default_list !='') {
                        if($email !='') {
                            $instance = $this->initialize_vr();
                            $response = $instance->getContactWithEmail($email);
                            
                            if(!$response) {
                                $addContactResponse = $instance->addContact($email, 'normal', null, $first_name, $last_name);
                                 $instance->subscribeContactToList($addContactResponse->contactId,$this->user->default_list);
                            } else {
                                 $contactId = $response->contactId;
                                 $instance->subscribeContactToList($contactId,$this->user->default_list);
                            }
                            
                        }
                    }
                    
                    if($email !='') {
                        $template = new Template(); 
                        $template->set_event('creation');
                        $template->set_company_id( $this->user->company_id );
                        $templates = $this->orm->get_object( $template);    

                        if($templates and count($templates) > 0) {
                            $this->load->library('email'); 
                            $this->load->library('mailer');        
                            $this->mailer->initialize($this->email, $this->get_mail_config());

                            foreach($templates as $temp) {
                                $sent = $this->mailer->send_template_individual($temp,$visitor); 
                                $this->orm->system_log(
                                        'Email status',
                                        $this->mailer->get_logs()
                                    );
                            }
                        }
                    }
                    
                }
                
                $id = $this->orm->save_object( $visitor );
                
                $filter = new Relations();
                $filter->set_person_id( $id );
                $this->orm->delete_object( $filter );
                foreach( $visitor->objects as $object ){
                    if( $object instanceof Relations ){
                        $object->set_person_id( $id );     
                    } else if ($object instanceof Visits ){
                        $object->set_visitor_id( $id );    
                    } else if($object instanceof Assignment_log ){
                        $object->set_visitor_id( $id);
                        $object->set_company_id($this->user->company_id);
                    } else if($object instanceof Note ){
                        $object->set_visitor_id( $id );
                    }
                    
                    if( $object instanceof Visits){
                        if( !$this->orm->get_object($object, true) ){
                            $this->orm->save_object( $object );    
                        }
                    } else if( $object instanceof Assignment_log ) {
                        $old = $this->orm->get_object($object,true);
                        if( !$old || !$old->equal($object)){
                            $this->orm->save_object( $object );    
                        }
                    } else if( $object instanceof Note ){
                        if( $object->note ){
                            $this->orm->save_object($object);
                        }else if( !$object->note && $object->id){
                            $this->orm->delete_object(new Note($object->id, $this->user->company_id));
                        }
                    } else {
                        $this->orm->save_object( $object );    
                    }
                }
                $visitor->set_id( $id );  
                $status = $old_id == $id? self::STATUS_UPDATE: self::STATUS_SAVED;
                
                $visitor = $this->orm->get_objects_with_related(
                    new Visitor($id,$this->user->company_id),
                    array( //Fetch following related objects
                        'Visits',
                        'Relations', 
                        'Assignment_log',
                        'Note'
                    )            
                );
                
            } else {
                $status = self::STATUS_ERROR;    
            }
            
            $this->load_display(
                $visitor,
                $status,
                $done_by_added
            );            
        } else if( $this->input->post('delete') ){
            $visitor = $this->marshall_visitor();
            if( $visitor->id ){
                $this->orm->delete_object(new Visitor($visitor->id, $this->user->company_id));
            }
            $this->index();
        } else if( $this->input->post('print')){
            $visitor = $this->marshall_visitor();
            $visitor = $this->orm->get_objects_with_related(
                new Visitor($visitor->id,$this->user->company_id),
                array( 
                    'Visits',
                    'Relations', 
                    'Assignment_log',
                    'Note'
                )            
            );
            $this->load_print_display( $visitor, null, null);
        } else {
            $this->load_display(
                $visitor,
                $status,
                $done_by_added,$data
            ); 
        }
    }

    private function initialize_vr(){
        $instance = $this->icontact_api->initialize(
            $this->config->item('iconact_app_id'),
            $this->user->vr_username,
            $this->user->vr_password
        );    
        try{
                $instance->getLists();
                return $instance;
            } catch (Exception $ex) {
               return false;
            }
    }

    public function show_visitor(){ 
        $id = $this->uri->segment(3);
        $filter = new Visitor();
        $filter->set_id($id);
        
        $visitor = $this->orm->get_objects_with_related(
            $filter,
            array( //Fetch following related objects
                'Visits',
                'Relations', 
                'Assignment_log',
                'Note'
            )            
        );
        
                
        $this->load_display( $visitor, null );
    }

    public function add_new(){//echo "<pre>";print_r($this->input->post());die;
        if( ($type_id = $_REQUEST['type'])|| ($type = $_REQUEST['type'])){
            $selected = 0;
            $saved = null; 
            $type = new Type();
            $type->set_type( $type_id );
            $type->set_company_id( $this->user->company_id );
            $typesFilter = $type;
            $control_id = $_REQUEST['id'];//$this->uri->segment(4);
            if( $this->input->post('company_id')){
                $type = $this->input_mapper->create_object_from_input( 'Type',Input_mapper::SOURCE_POST);
                if( in_array($type->type, array( Type::TYPE_ASSIGNMENT, Type::TYPE_LANGUAGE, Type::TYPE_REFERER, Type::TYPE_DONE_BY, Type::TYPE_MINISTRY, Type::TYPE_FUNDS)) && $type->validate()){
                    $control_id = $this->input->post('control_id');
                    $id = $type->id;
                    $type->set_company_id( $this->user->company_id );                
                    if($id == ($new_id = $this->orm->save_object($type)) ){
                        $saved = 'update';
                        $selected = $id;
                    } else {
                        $saved = true;
                        $type = new Type();
                        $type->set_type( $type_id );
                        $type->set_company_id( $this->user->company_id );
                    }                  
                } else {
                    $saved = false;
                }

            }
            $types = $this->orm->get_object( $typesFilter );
            //echo "<pre>";print_r($types);die;
            //$this->load->view('common/header',  array('user'=>$this->user,'dont_display_header'=>true));
            $data = $this->load->view('common/add_new', array('action' => site_url('visitors/add_new/'.$type_id), 'objects'=>$types, 'object'=>$type, 'saved'=>$saved, 'control_id' => $control_id,'selected_item' => $selected ));
            echo $data;
            //$this->load->view('common/footer');            
        }         
    }

    public function add_new_s(){
        if( ($type_id = $_REQUEST['type'])|| ($type = $_REQUEST['type'])){

            $saved = null; 
            $type = new Type();
            $type->set_type( $type_id );
            $type->set_company_id( $this->user->company_id );
            $types = $this->orm->get_object( $type );
            $control_id = $_REQUEST['id'];
            if( $_REQUEST['save_p'] ){
                
                $type = $this->input_mapper->create_object_from_input( 'Type',Input_mapper::SOURCE_GET);
                
                if( in_array($type->type, array( Type::TYPE_ASSIGNMENT, Type::TYPE_LANGUAGE, Type::TYPE_REFERER, Type::TYPE_DONE_BY)) && $type->validate()){
                    $control_id = $_REQUEST['control_id'];
                    $id = $type->id;
                    $type->set_company_id( $this->user->company_id );                
                    if($id == ($new_id = $this->orm->save_object($type)) ){
                        $saved = 'update';
                    } else {
                        $saved = true;
                    }
                    $type->set_id($new_id);
                } else {
                    $saved = false;
                }
                
            
            }
            $msg  = '';                       
            if( isset( $saved ) && $saved !== null){
                if( $saved === true ){
                    $saved = 'alert alert-success';
                    $msg   = '<strong>Success: </strong> Added Successfully!';                    
                } else if ( $saved === false ) {
                    $saved = 'alert alert-error';
                    $msg   = '<strong>Error: </strong>Your query has not been saved (Please correct fields with red border)';            
                } else if ( $saved === 'update') {
                    $saved = 'alert alert-success';
                    $msg   = '<strong>Success: </strong>updated Successfully!';             
                }
            } 
            echo $msg;die;
        }
    }

    /**
     * For quick add function
     * @return json data
     */
    public function ajax_add_new(){
        $visitor = new Visitor();
        $status  = '' ;
        
        $visitor = $this->marshall_visitor();
        $visitor->set_phone_1( $this->input->post('phone_1').$this->input->post('phone_2').$this->input->post('phone_3'));
        if( $this->input->post('month') != '-'){
            $visitor->set_birth( $this->input->post('month').'-'.$this->input->post('day').'-'.$this->input->post('year'));    
        } else {
            $visitor->set_birth('');
        }
                      
        if( $visitor->validate()){
            $old_id = $visitor->id;    
            if($old_id == '') {
                $visitor->set_registered_date( date('Y-m-d H:i:s') );   
                $first_name = $this->input->post('first_name');
                $last_name =  $this->input->post('last_name');
                $email =  $this->input->post('email');

                $gender = $this->input->post('gender');
                $visitor->set_gender($gender);
                
                $head_household = $this->input->post('head_household');
                $visitor->set_head_household($head_household);
                
                // $active = $this->input->post('active');
                // $visitor->set_active($active);

                $zip = $this->input->post('zip');
                $visitor->set_zip($zip);

                $state = $this->input->post('state');
                $visitor->set_state($state);
                
                $city = $this->input->post('city');
                $visitor->set_city($city);

                $address = $this->input->post('address');
                $visitor->set_address($address);

                $datetime = $this->input->post('datetime');
                $datetime = strtotime($datetime);
                $datetime = date("Y-m-d H:i:s", $datetime);
                $visitor->set_datetime($datetime);
                // var_dump($visitor); exit;

                // if($this->user->vr_username !='' and $this->user->vr_password !='' and $this->user->default_list !='') {
                //     if($email !='') {
                //         $instance = $this->initialize_vr();
                //         $response = $instance->getContactWithEmail($email);
                        
                //         if(!$response) {
                //             $addContactResponse = $instance->addContact($email, 'normal', null, $first_name, $last_name);
                //              $instance->subscribeContactToList($addContactResponse->contactId,$this->user->default_list);
                //         } else {
                //              $contactId = $response->contactId;
                //              $instance->subscribeContactToList($contactId,$this->user->default_list);
                //         }
                //     }
                // }
                
                // if($email !='') {
                //     $template = new Template(); 
                //     $template->set_event('creation');
                //     $template->set_company_id( $this->user->company_id );
                //     $templates = $this->orm->get_object( $template);    

                //     if($templates and count($templates) > 0) {
                //         $this->load->library('email'); 
                //         $this->load->library('mailer');        
                //         $this->mailer->initialize($this->email, $this->get_mail_config());

                //         foreach($templates as $temp) {
                //             $sent = $this->mailer->send_template_individual($temp,$visitor); 
                //             $this->orm->system_log(
                //                     'Email status',
                //                     $this->mailer->get_logs()
                //                 );
                //         }
                //     }
                // }
            }

            $id = $this->orm->save_object( $visitor );
            
            $filter = new Relations();
            $filter->set_person_id( $id );
            $this->orm->delete_object( $filter );

           
            foreach( $visitor->objects as $object ){
                if(empty($object)) continue;
                if( $object instanceof Relations ){
                    $object->set_person_id( $id );
                    $object->set_relation_type( "referer" );     
                } else if ($object instanceof Visits ){
                    $object->set_visitor_id( $id );    
                } else if($object instanceof Assignment_log ){
                    $object->set_visitor_id( $id);
                    $object->set_company_id($this->user->company_id);
                } else if($object instanceof Ministry_log ){
                    $object->set_visitor_id( $id);
                    $object->set_company_id($this->user->company_id);
                } else if($object instanceof Note ){
                    $object->set_visitor_id( $id );
                }
                
                if( $object instanceof Visits){
                     //there are no column active in visit table, so we have to remove it before query
                     if(isset($object->active)){
                        unset($object->active);
                     }

                    if( !$this->orm->get_object($object, true) ){
                        $this->orm->save_object( $object ); 
                    }
                } else if( $object instanceof Assignment_log ) {
                    $old = $this->orm->get_object($object,true);
                    if( !$old || !$old->equal($object)){
                        $this->orm->save_object( $object );    
                    }
                } else if( $object instanceof Ministry_log) {
                    $old = $this->orm->get_object($object,true);
                    if( !$old || !$old->equal($object)){
                        $this->orm->save_object( $object );    
                    }
                } else if( $object instanceof Note ){
                    if( $object->note ){
                        $this->orm->save_object($object);
                    }else if( !$object->note && $object->id){
                        $this->orm->delete_object(new Note($object->id, $this->user->company_id));
                    }
                } else {
                    $this->orm->save_object( $object );  
                }
            }
            echo json_encode(array('err'=>0, 'status'=> $status)); exit;
        } else {
            $status = self::STATUS_ERROR;   
            echo json_encode(array('err'=>1, 'status'=> $status)); exit;
        }
    } 
    
    public function ajax_select_refresh(){
        switch ($this->input->post('id')) {
            case 'primary_language':
                $data = array(''=>'Select Item','add_new'=>'Add Item')+$this->orm->get_languages( $this->user->company_id );
                echo json_encode($data);
                break;
            case 'refered_by':
               $data =  $this->orm->get_referers();
                echo json_encode($data);
                break;
            
            case 'new_ministry_id':
               $data =  $this->orm->get_ministries();
                echo json_encode($data);
                break;
            case 'new_assignment_id':
               $data =  $this->orm->get_assignments();
                echo json_encode($data);
                break;
            case 'new_assignment_doneby_person_id':
               $data =  $this->orm->get_done_by_persons();
                echo json_encode($data);
                break;
        }                
    }

    public function ajax_control(){ 
        switch( $this->input->post('type')){
            case 'funds':
                echo $this->ajax_get_type();
            break;
        }
    }
    
    function ajax_get_type(){     
        $return = array();
        $filter = null;
        if( ($type = $this->input->post('item_type'))){
            $filter = new $type();
            $filter->set_id( $this->input->post('id'));
            $filter->set_company_id( $this->user->company_id );
        }
        
        if( $filter ){
            $filter->set_company_id( $this->user->company_id );
            $objects =  $this->orm->get_object( $filter, true );
            if( $objects ){
                       $return['name'] = $objects->name;
                       $return['description'] = $objects->description;
                       $return['active'] = $objects->active;
                       $return['id'] = $objects->id;
            }
        }        
        return json_encode( $return );
    }
       
    public function load_display( $visitor, $status = null, $done_by_added = false,$data = null ){  
        $states = $this->orm->get_us_states(); 
        $members = $this->orm->get_members();
        $languages = array(''=>'Select Item','add_new'=>'Add Item')+$this->orm->get_languages( $this->user->company_id ); 
        $assignments = $this->orm->get_assignments();
        $referers    = $this->orm->get_referers();
        $done_by_members = $this->orm->get_done_by_persons();
        
        $months = $this->get_months();
        $days   = $this->get_days();
        $years  = $this->get_years();
                
        $this->load->view('include/sidebar', array('user'=>$this->user, 'visitor_menu'=>true, 'no_new_user'=>true, 'jquery_ui'=>true ));
        $this->load->view(
            'visitors/new_visitor', 
             array(
                'visitor'=>$visitor, 
                'assignment_types'=>$assignments, 
                'members' => $members,
                'referrers' => $referers,
                'done_by_members' => $done_by_members,
                'status'   => $status, 
                'states'   => $states, 
                'languages'=> $languages, 
                'years'=>$years, 
                'months'=>$months, 
                'days'=>$days,
                'done_by_added' => $done_by_added,
                'data'=>$data
             )
        );
        $this->load->view('include/footer');        
    }
    
    public function load_print_display( $visitor, $status = null, $done_by_added = false ){  
        $states = $this->orm->get_us_states(); 
        $members = $this->orm->get_members();
        $languages = array(''=>'Select Item','add_new'=>'Add Item')+$this->orm->get_languages( $this->user->company_id ); 
        $assignments = $this->orm->get_assignments();
        $referers    = $this->orm->get_referers();
        $done_by_members = $this->orm->get_done_by_persons();
        
        $months = $this->get_months();
        $days   = $this->get_days();
        $years  = $this->get_years();
                
         
        $this->load->view(
            'visitors/print', 
             array(
                'visitor'=>$visitor, 
                'assignment_types'=>$assignments, 
                'members' => $members,
                'referrers' => $referers,
                'done_by_members' => $done_by_members,
                'status'   => $status, 
                'states'   => $states, 
                'languages'=> $languages, 
                'years'=>$years, 
                'months'=>$months, 
                'days'=>$days,
                'done_by_added' => $done_by_added
             )
        );
                
    }    
    /**
    * Performs Batch action on set of users
    * 
    */
    public function batch_send(){
         
        $status = '';
        $selected = array();
        
        foreach( $this->input->post() as $key=>$value){
            if( preg_match('/user_(?P<id>\d+)/', $key, $match)){
                if( $this->orm->get_object( new Visitor( $match['id'], $this->user->company_id ))){
                    $selected[$match['id']] = true;
                }
            } else if( preg_match('/action_(\w+)/', $key, $match)){
                $action = $match['0'];
            }
        }
        if( $action = $this->input->post('action')){
             switch( $action ){
                case self::BATCH_EMAIL;
                    $tos = base64_encode( serialize( $selected ));
                    header('Location: '.site_url('email/index/batch_email/'.urlencode($tos)));
                break;
                case self::BATCH_LETTER;
                case self::BATCH_POSTCARD:
                    $tos = base64_encode( serialize( $selected ));
                    header('Location: '.site_url('paper/new_order_step_1/batch_letter/'.urlencode($tos)));                
                break;
                case self::BATCH_TEXT:
                break;
            }           
        }
        $this->index( $this->input->post('first_letter'), $selected,  $status );  
    }
    
    
    public function batch_import() {
        
        $status = '';
        
        if( $this->input->post('save')){
            
            $status = self::STATUS_ERROR;
            
            if( isset( $_FILES['import_contacts']) and $_FILES['import_contacts']['name'] != ''){
                
                
                $handle = fopen($_FILES['import_contacts']['tmp_name'], "r");
               
                $all_data = array();
                while ( ($data = fgetcsv($handle) ) !== FALSE ) {
                        $all_data[] = $data;
                         
                }
                 if(count($all_data) > 1) {
                     unset($all_data[0]);
                     foreach($all_data as $newVisitor) {
                         $newVisitor[3] = strtolower($newVisitor[3]);
                         $visitor = new Visitor();
                         $visitor->set_company_id( $this->user->company_id );
                         $visitor->set_first_name($newVisitor[1]);    
                         $visitor->set_last_name($newVisitor[0]);    
                         $visitor->set_email($newVisitor[3]); 
                         $visitor->set_phone_1($newVisitor[2]);
                         $visitor->set_address($newVisitor[4]);    
                         $visitor->set_city($newVisitor[5]);    
                         $visitor->set_state($newVisitor[6]);    
                         $visitor->set_zip($newVisitor[7]);    
                         $visitor->set_registered_date( date('Y-m-d H:i:s') );   
                         $id = $this->orm->save_object($visitor);
                         $visit = new Visits(null, $this->user->company_id);
                         $visit->set_datetime( date('m-d-Y') );
                         $visit->set_visitor_id( $id );
                         if( $visit->validate()){
                                $this->orm->save_object($visit);
                                $status = 'success';                     
                         }
                         
                        $status = self::STATUS_SAVED;
                        $first_name = $newVisitor[1];
                        $last_name =  $newVisitor[0];
                        $email =  $newVisitor[3];

                        if($this->user->vr_username !='' and $this->user->vr_password !='' and $this->user->default_list !='') {


                            if($email !='') {
                                $instance = $this->initialize_vr();
                                $response = $instance->getContactWithEmail($email);

                                if(!$response) {
                                    $addContactResponse = $instance->addContact($email, 'normal', null, $first_name, $last_name);
                                     $instance->subscribeContactToList($addContactResponse->contactId,$this->user->default_list);
                                } else {
                                     $contactId = $response->contactId;
                                     $instance->subscribeContactToList($contactId,$this->user->default_list);
                                }

                            }
                        }
                     }
                 }
                
               
            }
             
        }
        
        $this->load->view('common/header', array('user'=>$this->user, 'visitor_menu'=>true, 'no_new_user'=>true));
        $this->load->view('visitors/batch_import',array('status' => $status));
        $this->load->view('common/footer');        
        
    }
    
    public function process(){ 
         
        if(($selected = $this->uri->segment(3)) && ($type = $this->uri->segment(4))){
            
            switch( $type ){
                case self::MAIL:                    
                     header('Location: '.site_url('email/index/batch_email/'.$selected));                        
                break;
                case self::POSTCARD:
                     header('Location: '.site_url('paper/new_order_step_1/batch_paper/'.$selected));
                break;
                case self::FORWARD:
                     header('Location: '.site_url('email/index/batch_forward/'.$selected));
                break;                                
            }            
        }
    }
    
    public function postcard(){
        $this->index();
    }
    
    public function letter(){
        $this->index();
    }
    
    public function visitor_print(){
        $this->index();
    }
    
    public function text(){
        $this->index();
    }
    
    public function sticky(){
        $this->index();
    }
    
    public function ajax_get_members(){  
        $filter = new Visitor();
        $filter->set_company_id( $this->user->company_id );
        
        $persons = $this->orm->get_object($filter);
        
        $array = array();
        foreach( $persons as $person){
            $array[$person->id] = $person->first_name.' '.$person->last_name;
        }
        echo json_encode($array);
    }
    
    public function ajax_save_notes(){
        $status = "";
        if( ($notes = $this->input->post('notes')) && ($id = $this->input->post('id'))){
            $visitor = $this->orm->get_object( new Visitor( $id, $this->user->company_id ), true );
            if( $visitor ){
                $visitor->set_notes( $notes );
                $this->orm->save_object($visitor);
                $status = 'success';
            } else {
                $status = 'fail';
            }
        }
        echo json_encode( $status );
    }
    
    public function ajax_add_visit(){
         
        $status = 'fail';
        if( ($date = $this->input->post('visit_date')) && ($id = $this->input->post('id'))){
            if($this->orm->get_object( new Visitor( $id, $this->user->company_id ), true )){
                $visit = new Visits(null, $this->user->company_id);
                $visit->set_datetime( $date );
                $visit->set_visitor_id( $id );
                if( $visit->validate()){
                    $this->orm->save_object($visit);
                    $status = 'success';                     
                }
                
            }
        }
        echo json_encode( $status );
    }  
      
    public function marshall_visitor(){
       
        $spouse_relation = $this->input_mapper->create_object_from_input( 'Relations',Input_mapper::SOURCE_POST,'spouse_');
        $refered_by_relation = $this->input_mapper->create_object_from_input( 'Relations',Input_mapper::SOURCE_POST,'refered_by_');        
        $child_relations = $this->input_mapper->create_object_array_from_input('Relations',Input_mapper::SOURCE_POST, 'child_<index>_<name>',1,10);
        $assignments  = $this->input_mapper->create_object_array_from_input('Assignment_log',Input_mapper::SOURCE_POST, 'assignment_<index>_<name>',0,10);
        $visit = $this->input_mapper->create_object_from_input( 'Visits',Input_mapper::SOURCE_POST);
        $visit->set_company_id( $this->user->company_id);
        
        if( ($post_notes = $this->input->post('new_note')) && is_array($post_notes)){
            foreach( $post_notes as $post_id=>$post_note ){
                list($id,$person_id) = split('_',$post_id);
                $note = new Note( $id ? $id : null, $this->user->company_id);
                $note->set_person_id($person_id ? $person_id : $this->user->id);
                $note->set_note($post_note);
                $note->_person = array(
                    $this->user
                );
                $notes[] = $note;                
            }

        }
        
        $visitor = $this->input_mapper->create_object_from_input( 'Visitor',Input_mapper::SOURCE_POST);
        $visitor->set_person_type( Person::TYPE_VISITOR )
        ->set_company_id( $this->user->company_id )
        ->add_object( $spouse_relation )
        ->add_object( $refered_by_relation )
        ->add_object( $child_relations )
        ->add_object( $visit )
        ->add_object( $assignments )
        ->add_object( $notes );
                                                       
          
        return $visitor;
    }
    
    public function print_map(){
        $this->load->view('visitors/print');
    }
    
    private function get_years(){
        $base_year = 1920;
        $year  = gmdate('Y');
        $years = array('-' => 'N/A');
        for( $i=1;($i+$base_year)<$year;$i++){
            $years[$base_year + $i] = $base_year + $i;
        }
        return $years;            
    }
    
    private function get_months(){
        return array(
            '-' => 'N/A',
            '01'=>'January',
            '02'=>'February',
            '03'=>'March',
            '04'=>'April',
            '05'=>'May',
            '06'=>'June',
            '07'=>'July',
            '08'=>'August',
            '09'=>'September',
            '10'=>'October',
            '11'=>'November',
            '12'=>'December'
        );
    }
    
    private function get_days(){
        $days = array('-' => 'N/A');
        for( $i=1;$i<32;$i++){
            if( $i<10){
                $key = '0'.$i;
            } else {
                $key = $i;
            }
            $days[$key]=$i;
        }
        return $days;
    }
    
    public function ajax_get_new_assignment($id = '', $date = '', $done = '', $index = ''){
        $type_id   = $id ? $id : $this->input->post('id');
        $date = $date ? $date : $this->input->post('date');
        $done = $done ? $done : $this->input->post('done');
        $index = $index ? $index : $this->input->post('index');
        
        $assignments     = $this->orm->get_assignments();
        $done_by_members = $this->orm->get_done_by_persons();
        
        $assignment = new Assignment_log();
        $assignment
            ->set_datetime($date)
            ->set_doneby_person_id($done)            
            ->set_type_id($type_id);
        
        $this->load->view(
            'visitors/component_assignment',
            array(
                'assignment_types' => $assignments,
                'done_by_members'  => $done_by_members,
                'assignment'       => $assignment,
                'index'            => $index
            )
        );      
    }
    
    public function ajax_remove_assignment(){ 
        if( $id = $this->input->post('id')){
            $assignment = new Assignment_log(
                $id,
                $this->user->company_id
            );
            $this->orm->delete_object($assignment);            
        }
    }
    
    public function batch_delete(){ 
        if( $visitors = $this->input->post('visitors')){
            foreach( $visitors as $visitor_id){
                if( $visitor = $this->orm->get_object( new Visitor( $visitor_id, $this->user->company_id ), true )){
                    $this->orm->delete_object(new Visitor( $visitor_id, $this->user->company_id ));
                    $visits = new Visits();
                    $visits->set_company_id($this->user->company_id);
                    $visits->set_visitor_id($visitor_id);
                    $this->orm->delete_object($visits);
                }
            }
        }
        $this->index();
    }
                     
    public function get_visitor_notes_popup(){
        if( $visitor_id = $this->input->post('id')){
            if( $visitor = $this->orm->get_object( new Visitor( $visitor_id, $this->user->company_id ), true )){
                $data = '<div id="div_status_'.$visitor->id.'" class="hidden">
                            <a class="close" data-dismiss="alert">�</a>
                         </div>
                         <textarea id="text_notes_'.$visitor->id.'">'.$visitor->notes.'</textarea>
                         <a class="btn" href="javascript:void(0)" onclick="save_notes('.$visitor->id.')">Save</a>
                         <a id="btn_cancel_'.$visitor->id.'"class="btn" href="javascript:void(0)" onclick="hide_notes('.$visitor->id.', this)">Cancel</a>';             
                $title = 'Notes';
                $field_data = $visitor->notes ? 'Edit Notes' : 'Add Notes';
                $trigger = 'data-trigger=\'click\'';
                $tag = 'a';
                $id = 'notes_';
                $class = 'btn span2'; 
                echo '<'.$tag.' '.$trigger.' class="'.$class.'"  data-html="" data-placement="left" id="'.$id.$visitor->id.'" data-content=\''.$data.'\'   data-original-title="'.$title.'">'.$field_data.'</'.$tag.'>';  
            }    
        }
    }
    
    public function get_visitor_dates_popup(){
        if( $visitor_id = $this->input->post('id')){
            if( $visitor = $this->orm->get_object( new Visitor( $visitor_id, $this->user->company_id ), true )){ 
                $visits  = new Visits();
                $visits->set_visitor_id($visitor_id);
                $visits->set_company_id($this->user->company_id);
                $visits  = $this->orm->get_object( $visits );
                 
                $id  = 'dates_'; 
                $data = '<div id="div_status_visits_'.$visitor->id.'" class="hidden">
                            <a class="close" data-dismiss="alert">�</a>
                         </div>';
                $data.= '<div class="form-inline"><input type="text" class="input-small" data-datepicker="datepicker" id="new_visit_'.$visitor->id.'" value="'.gmdate('m-d-Y').'" onload="$(this).datepicker()"/><a class="btn btn-warning" href="#" onclick="add_visit('.$visitor->id.')">Add visit</a></div>';
                $data.= '<table id="'.$id.$visitor->id.'_table"class="table  first center" onload="create_table(this)">';
                $data.= '<thead><tr><th>Visit dates</th></tr></thead>';
                foreach($visits as $visit ){
                    $data.= '<tr><td>'.$this->get_us_date_format($visit->datetime).'</td></tr>';    
                }
                $data.= '</table>';
                $data.= '<a id="btn_cancel_visits_'.$visitor->id.'"class="btn" href="#"  onclick="hide_visit_log('.$visitor->id.')">Close</a>';
                $title = 'Visit log';
                
                $field_data = 'Not available';
                if( is_array( $visits )){
                    $field_data = $visits[count($visits)-1]->datetime;
                }
                $tag = 'a';
                $trigger = 'data-trigger=\'click\'';
                $class = 'btn span1' ; 
                echo '<'.$tag.' '.$trigger.' class="'.$class.'"  data-html=""   id="'.$id.$visitor->id.'" data-content=\''.$data.'\'   data-original-title="'.$title.'">'.$field_data.'</'.$tag.'>';  
            }    
        }        
       
    }
    public function export() {
        $this->load->view('include/sidebar', array('user'=>$this->user, 'visitor_menu'=>true, 'no_new_user'=>true ));
        $this->load->view('visitors/export', array());
        $this->load->view('include/footer');
    }
    public function export_file() {
        
        $start_date = $this->input->get('start_date');
        $end_date   = $this->input->get('end_date');
        if($start_date == '' or $end_date == '')
            die('');
        $start_date = explode('-', $start_date);
        $end_date = explode('-', $end_date);
       
        $start_date = date('Y-m-d',strtotime($start_date[0].'-'.$start_date[1].'-'.$start_date[2]));
        $end_date = date('Y-m-d',strtotime($end_date[0].'-'.$end_date[1].'-'.$end_date[2]));
        
        $getRangeClients = $this->orm->getRangeVisitors($start_date,$end_date,$this->user->company_id);
        if(!$getRangeClients)
            die('no');
        $nl = "\n";
        $csv= 'First Name;Last Name;Phone;Email;Address;City;State;Zip'.$nl;
        foreach($getRangeClients as $client) {            
            $csv .= "$client->first_name;$client->last_name;$client->phone_1;$client->email;$client->address;$client->city;$client->state;$client->zip$nl";            
        }
       
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public", false); 
        header("Content-Description: File Transfer");
        header("Content-Type: application/force-download");
        header("Accept-Ranges: bytes");
        header("Content-Disposition: attachment; filename=export.csv;");
        header("Content-Transfer-Encoding: binary");
        
        echo $csv;       
    }



    protected function get_last_note($str_notes, $timestamp){
        $last_note = '';
        if(preg_match_all('!\[(?P<timestamp>\d+-\d+-\d+ \d+:\d+:\d+)[^\]\[]+?\]\s+(?P<notes>[^\]\[]+)!s',$str_notes, $matches)){
            foreach( $matches['notes'] as $index=>$note ){
                $note_timestamp = $matches['timestamp'][$index];
                if( strtotime($note_timestamp) == strtotime($timestamp)){
                    $last_note = $matches['notes'][$index];
                }
            }
        }      
        return $last_note;
    }
    
    public function convert_member() {
        $visitor_id = $this->uri->segment(3,0);
        
        if($visitor_id > 0) {
            $filter = new Visitor();
            $filter->set_id($visitor_id);
            //$filter->set_person_type(Person::TYPE_MEMBER);

            $visitor = $this->orm->get_objects_with_related($filter );
            if($visitor instanceof Visitor) {
                $this->db->where('id',$visitor_id);
                $this->db->update('dr_persons',array('person_type' => Person::TYPE_MEMBER));
                redirect('members/show_member/'.$visitor_id);
            }
        }
    }
         
}
