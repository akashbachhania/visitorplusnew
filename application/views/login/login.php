<!DOCTYPE html>
<html>
    <head>
        <title>Visitor Plus</title>
        <?php $this->load->view('login/login_header');?>
    </head>
    <body>        
        <div class="container">
          <section id="gridSystem">
              <div>
                  <div class="span6 offset2 well">
                    <form class="form-horizontal" action="<?php echo site_url('login')?>" method="post">
                        <fieldset>
                        <legend><img src="http://visitorplus.net/live/application/views/assets/img/visitorplus.logo.png" width="157" height="50" alt="Visitor Plus logo"></legend>
<div class="control-group">
              <label class="control-label" for="input01">Username</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" name="username"id="input01">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input02">Password</label>
                                <div class="controls">
                                    <input type="password" class="input-xlarge" name="password" id="input02">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input02"></label>
                                <div class="controls">
                                    <input type="submit" class="btn span2" name="submit" id="submit" value="Ok"><br/><br/>
                                    <a href="<?= base_url() ?>login/forget_pwd" style="color:#000;text-decoration:none;">I forgot my password</a>
                                </div>
                            </div>                                                           
                        </fieldset>
                    </form>          
                  </div>          
              </div>
          </section>
        </div>
    </body>
</html>            