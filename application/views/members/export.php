<link rel="stylesheet" href="<?php echo base_url()?>application/views/assets/style/export.css">
<div class="row top_row">
 <div class="col-md-3"></div>
    <div class="span4">
        <div class="well">
            <div class="div_legend">Export</div>
            <div name="controls">      
                <div>

                    <input type="text" id="start_date_export" class="input-small date" data-datepicker="datepicker">
                    <input type="text" id="end_date_export"  class="input-small date" data-datepicker="datepicker">
                   <br>
                    <a id="a_excel" data-placement="top" rel="tooltip"  data-original-title="Export Agents to Excel" onclick="exportMembers();" href="javascript:void(0)"></a>
                </div>  
            </div>
        </div>

    </div>   
</div>

<script>
    function exportMembers() {
        var start_date = $('#start_date_export').val();
        var end_date = $('#end_date_export').val();
        if (start_date == '') {
            $('#start_date_export').css('border-color', 'red');
            return false;
        } else {
            $('#start_date_export').css('border-color', '#ccc');
        }
        if (end_date == '') {
            $('#end_date_export').css('border-color', 'red');
            return false;
        } else {
            $('#end_date_export').css('border-color', '#ccc');
        }

        window.open('<?php echo site_url('members/export_file') ?>?start_date=' + start_date + '&end_date=' + end_date);
    }
</script>