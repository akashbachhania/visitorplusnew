<?php 
$error = 'error';

switch( $status ){
    case Members::STATUS_SAVED:
        $msg = "New members has been created!";
        $class = 'alert alert-success';
    break;
    case Members::STATUS_UPDATE:
        $msg = "Member has been updated";
        $class = 'alert alert-info'; 
    break;
    case Members::STATUS_ERROR:
        if( $visitor->errors ){
            $msg = "Plese fill in mandatory fields";
        } else {
            $msg = "There was an error while trying to save visitor";
        }
        
        $class = 'alert alert-error'; 
    break;
}

$referrers;
$member_children = array();
$children = array();
$member_referer = '';
$member_spouse  = '';
$spouse  = '';
$referer = '';
$hidden_referer = 'hidden';
$hidden_spouse  = 'hidden';
 
if( isset( $visitor->_relations )){
    foreach( $visitor->_relations as $relation ){
         if( $relation->relation_type == Relations::RELATION_CHILD ){
             $member_children[] = $relation->related_person_id;
             $children[] = $relation->related_person_name;
         } else if( $relation->relation_type == Relations::RELATION_SPOUSE ){
             $member_spouse = $relation->related_person_id;
             $spouse = $relation->related_person_name;
             $hidden_spouse = '';
         } else if( $relation->relation_type == Relations::RELATION_REFERER ){
             $member_referer = $relation->related_person_id;
             $referer = $relation->related_person_name;
             $hidden_referer = '';
         }
    }
}

$visitor_year = $visitor_day = $visitor_month = '-' ;
if(preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)!', $visitor->birth, $match)){
    $visitor_month = $match['month'];
    $visitor_year  = $match['year'];
    $visitor_day   = $match['day'];
}
?>
    <form class="form-horizontal" action="<?php echo site_url('members/new_member')?>" id="form_new_visitor" method="post"  enctype="multipart/form-data">
    <div id="div_new_visitor" class="span8">
        <?php if( $status ){ ?>
            <div id="div_visitor_added" class="<?php echo $class ?>">
                    <a class="close" data-dismiss="alert">x</a>
                    <?php echo $msg ?>
            </div>
        <?php } ?>    
            <fieldset>
            <div class="widgets_area">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="well red">
                            <div class="well-header">
                                <h5><?php echo $visitor->id ? $visitor->first_name .' '. $visitor->last_name: 'New Member'?></h5>
                            </div>
                            <div class="well-content no-search">
                                <div class="form_row">
                                    <label class="field_name align_right">Attend date:</label>
                                    <div class="field">
                                     <input type="text" name="datetime" class="span12" id="datetime" value="<?php echo  !empty($visitor->datetime) ?  substr($visitor->datetime, 0, 10)  : ''?>">
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#datetime').datepicker({
                                                    // dateFormat: 'mm-dd-yy',
                                                    dateFormat: 'yy-mm-dd',
                                                    onSelect: function(dateText) {
                                                        $('#datetime').val(this.value);
                                                        // console.log("Selected date: " + dateText + "; input's current value: " + this.value);
                                                    }
                                                });
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="form_row">
                                    <label class="field_name align_right">First</label>
                                    <div class="field">
                                        <input type="text" class="span5 <?php echo isset($visitor->errors['first_name'])? $error:''?>" value="<?php echo $visitor->first_name?>"   name="first_name" id="first_name">
                                        Last:
                                        <input type="text" class="span5 <?php echo isset($visitor->errors['last_name'])? $error:''?>" value="<?php echo $visitor->last_name?>"   name="last_name" id="last_name">
                                    </div>
                                </div>
                                <div class="form_row">
                                    <label class="field_name align_right">Address:</label>
                                    <div class="field">
                                        <input type="text" class="span12 <?php echo isset($visitor->errors['address'])? $error:''?>" value="<?php echo $visitor->address?>"   name="address" id="address">
                                    </div>
                                </div>

                                <div class="form_row">
                                    <label class="field_name align_right">City:</label>
                                    <div class="field">
                                        <input type="text" class="span3 <?php echo isset($visitor->errors['city'])? $error:''?>" value="<?php echo $visitor->city?>" name="city"   id="city">
                                        State:
                                        <?php echo form_dropdown('state', $states, $visitor->state, 'id="state" class="span4"') ?>
                                        Zip:
                                        <input type="text" class="span3 <?php echo isset($visitor->errors['zip'])? $error:''?>" value="<?php echo $visitor->zip?>"   name="zip"    id="zip">
                                    </div>
                                </div>

                
                                <div class="form_row">
                                    <label class="field_name align_right">Phone:</label>
                                    <div class="field">
                                        <input type="text" class="span2  <?php echo isset($visitor->errors['phone_1'])? $error:''?>" value="<?php echo substr($visitor->phone_1,0,3)?>" name="phone_1"   id="phone_1">
                                        -
                                        <input type="text" class="span2  <?php echo isset($visitor->errors['phone_1'])? $error:''?>" value="<?php echo substr($visitor->phone_1,3,3)?>" name="phone_2"   id="phone_2">
                                        -
                                        <input type="text" class="span2  <?php echo isset($visitor->errors['phone_1'])? $error:''?>" value="<?php echo substr($visitor->phone_1,6,4)?>" name="phone_3"   id="phone_3">
                                        Gender:
                                        <?php echo form_dropdown('gender', array('' => '-Select gender-', Visitor::GENDER_FEMALE => 'Female',Visitor::GENDER_MALE => 'Male'), $visitor->gender , 'id="gender" class="span4 '.(isset($visitor->errors['gender'])? $error:'').' "') ?>                                        
                                    </div>
                                </div>

                                <div class="form_row">
                                    <label class="field_name align_right">Email:</label>
                                    <div class="field">
                                        <input type="text" class="span12 <?php echo isset($visitor->errors['email'])? $error:''?>" value="<?php echo $visitor->email?>" name="email"   id="email">
                                    </div>
                                </div>
                                <div class="form_row">
                                    <label class="field_name align_right">Head of Household:</label>
                                    <div class="field">
                                        <?php echo form_dropdown('head_household', array(Visitor::MARRIED_SINGLE => 'No',Visitor::MARRIED_YES => 'Yes'), $visitor->head_household , 'id="head_household" class="span4"') ?>                
                                    </div>
                                </div>
                                <div class="form_row">
                                    <label class="field_name align_right">Member Status:</label>
                                    <div class="field">
                                        <?php echo form_dropdown('active', array(1 => 'Active',0 => 'Inactive'), $visitor->active , 'id="active" class="span4"') ?>                
                                    </div>
                                </div>
                                <div class="form_row">
                                    <label class="field_name align_right">Month:</label>
                                    <div class="field">
                                        <?php echo form_dropdown('month', $months, $visitor_month, 'class="birth span3" id="select_month" '); ?>
                                        <label class="inline_label birth">Day:</label>
                                        <?php echo form_dropdown('day', $days, $visitor_day, 'class="birth span4" id="select_day" '); ?>
                                        <label class="inline_label birth">Year:</label>
                                        <?php echo form_dropdown('year', $years, $visitor_year, 'class="birth span4" id="select_year" '); ?>
                                    </div>
                                </div>

                                <div class="form_row">
                                    <label class="field_name align_right">Married:</label>
                                    <div class="field">
                                        <?php echo form_dropdown('married', array('' => '-', Visitor::MARRIED_SINGLE => 'No',Visitor::MARRIED_YES => 'Yes'), $visitor->married , 'id="select_married" class="span4"') ?>                
                                        <label class="inline_label spouse <?php echo $hidden_spouse ?>">Spouse:</label>
                                        <input type="text" name="spouse_related_person_name" class="spouse <?php echo $hidden_spouse ?>" id="spouse" value="<?php echo $spouse?>">
                                        
                                        <input type="hidden" name="spouse_person_id" id="spouse_person_id"/>        
                                        <input type="hidden" name="spouse_relation_type" id="spouse_person_id" value="<?php echo Relations::RELATION_SPOUSE?>"/>        
                                        <input type="hidden" name="spouse_related_person_id" id="spouse_related_person_id" value="<?php echo $member_spouse;?>" />        
                                        <input type="hidden" name="name_spouse_old" id="name_spouse_old"  value="<?php echo $spouse?>" />        
                                    </div>
                                </div>
                                <div class="form_row" id="div_control_children">
                                    <label class="field_name align_right">Children:</label>
                                    <div class="field">
                                        <?php echo form_dropdown('children', array('0'=>'No','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'), count($member_children), 'id="select_children" class="span4"')?>            
                                    </div>
                                </div>
                                <?php
                                if( $children ){
                                    foreach( $children as $index=>$child ){
                                     echo   '<div class="children form_row">
                                            <label class="field_name align_right">Child#'.($index+1).'</label>
                                            <div class="field">
                                                <input type="text" class="input-normal" name="child_'.$index.'_related_person_name" id="child_name_'.$index.'" value="'.$child.'">                                                      
                                                <input type="hidden" class="span4" name="child_'.$index.'_related_person_id" id="child_'.$index.'related_person_id">
                                                <input type="hidden" class="span4" name="child_'.$index.'_person_id" id="child_'.$index.'person_id" value="'.$visitor->id.'">              
                                                <input type="hidden" class="span4" name="child_'.$index.'_relation_type" id="child_'.$index.'relation_type" value="child">     
                                                <input type="hidden" class="old_child span4" value="'.$child.'">     
                                            </div>
                                        </div>';                
                                    }
                                }
                                ?>
                                <div class="form_row">
                                    <label class="field_name align_right">Bible study:</label>
                                    <div class="field">
                                        <?php echo form_dropdown('bible_study', array('' => '-', Visitor::STUDY_YES => 'Yes',Visitor::STUDY_NO => 'No'), $visitor->bible_study , 'id="bible_study" class="span4"') ?>
                                        <label id="label_primary_lang">Primary language:</label>
                                        <?php echo form_dropdown('primary_language', $languages, $visitor->primary_language, ' id="primary_language" class="span4 add_new_type"  data-type="'.Type::TYPE_LANGUAGE.'"  onclick="add_new_type(this)"') ?>                                 
                                    </div>
                                </div>
                                <div class="form_row">
                                    <label class="field_name align_right">Reffered by:</label>
                                    <div class="field">
                                        <?php echo form_dropdown('refered_by', $referrers, $visitor->refered_by, ' class="span4 add_new_type" id="refered_by" data-type="'.Type::TYPE_REFERER.'" onclick="add_new_type(this)"')?>
                                        <label class="inline_label label_referer <?php echo $hidden_referer ?>">Name:</label>
                                        <input type="text" name="refered_by_related_person_name" class="input-name <?php echo $hidden_referer ?>" id="referer" value="<?php echo $referer ?>">               
                                        <input type="hidden" name="refered_by_person_id" id="refered_by_person_id"/>        
                                        <input type="hidden" name="refered_by_relation_type" id="refered_by_relation_type" value="<?php echo Relations::RELATION_REFERER?>"/>        
                                        <input type="hidden" name="refered_by_related_person_id" id="refered_by_related_person_id"/>                                  
                                    </div>
                                </div>
                             <?php 
                                if( isset($visitor->_ministry_log)){
                                    foreach($visitor->_ministry_log as $index=>$assignment ){
                                        $this->load->view(
                                            'members/component_ministr',
                                            array(
                                                'ministries' => $ministries,                                    
                                                'assignment' => $assignment,
                                                'index' => $index
                                            )
                                        );
                                    }    
                                }                                                                                 
                            ?>                               
                            <div class="form_row new_ministry">
                                <label class="field_name align_right">Ministry:</label>
                                <div class="field">
                                <?php echo form_dropdown('new_ministry_id', $ministries, '', ' id="new_ministry_id" class="span4 add_new_type" data-type="'.Type::TYPE_MINISTRY.'"  onclick="add_new_type(this)" ')?>
                                    <a class="btn" id="add_new_ministr">Add</a>
                                </div>
                            </div>
                            <?php 
                                if( isset($visitor->_assignment_log)){
                                    foreach($visitor->_assignment_log as $index=>$assignment ){
                                        $this->load->view(
                                            'members/component_assignment',
                                            array(
                                                'assignment_types' => $assignment_types,
                                                'members' => $members,
                                                'assignment' => $assignment,
                                                'index' => $index
                                            )
                                        );
                                    }    
                                }                                                                                 
                            ?>

                            <div class="new_assignment form_row">
                                <label class="field_name align_right">New assignment :</label>
                                <div class="field">
                                    <?php echo form_dropdown('new_assignment_id', $assignment_types, '', ' id="new_assignment_id" class="span3 add_new_type" data-type="'.Type::TYPE_ASSIGNMENT.'"  onclick="add_new_type(this)" ')?>
                                    <label  class="inline_label label_assign">Date:</label>                                                                      
                                    <input type="text" data-datepicker="datepicker" name="new_assignment_datetime"  id="new_assignment_date" class="input-small assignment_date" id="new_assignment_datetime" value="<?php echo gmdate('Y-m-d')?>" data-date-format="yyyy/mm/dd">
                                    <label class="inline_label label_referer">Done by:</label>                                                      
                                    <?php echo form_dropdown('new_assignment_doneby_person_id',$done_by_members, '', ' id="new_assignment_doneby_person_id"  class="span3  add_new_type" data-type="'.Type::TYPE_DONE_BY.'"  onclick="add_new_type(this)" ')?>
                                    <a class="btn" id="add_new_assignment">Add</a>
                                </div>
                            </div>
                        <!-- notes -->
                        <?php ?>
                        <div id="div_fourth">
                             <div class="">
                                <div id="visitor_notes">
                                    <table id="vifeed_table">
                                        <?php if(isset($visitor->_note) and $visitor->_note && is_array($visitor->_note)) { ?>
                                        <thead>
                                            <tr>
                                                <th>Time</th>
                                                <th>Modified By</th>
                                                <th>Note</th>
                                            </tr>
                                        </thead>
                                        <?php } ?>
                                        <tbody id="">
                                        <?php 
                                        if( isset($visitor->_note) and $visitor->_note && is_array($visitor->_note)) {
                                            foreach( $visitor->_note as $note ){
                                                if( $note->note ){
                                                    echo '<tr>'.
                                                            '<td>'.$note->change_time.'</td>'.
                                                            '<td>'.$note->_person[0]->name.'</td>'.
                                                            '<td><textarea name="new_note['.$note->id.'_'.$note->person_id.']" class="note_textarea" onchange="change_note(this)" data-clean="true" onclick="slide_down(this)">'.$note->note.'</textarea></td>'.                                        
                                                         '</tr>';                                                 
                                                }
                                            }
                                            
                                        }
                                        echo '<tr>'.
                                                '<td>New Note:</td>'.
                                                '<td colspan="2"><textarea name="new_note[0_0]" class="note_textarea"></textarea></td>'.                                        
                                             '</tr>';                                      
                                        ?>                 
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>

                <!-- end notes -->                
                
                <input type="hidden" name="id" id="id" value="<?php  echo $visitor->id?>"/>                                                                                                         
                <input id="button_control" type="hidden" name="save" value="save"/>
                <?php if(isset($done_by_added) && $done_by_added == true){?>                                                                                                         
                    <input type="hidden" name="done_by_added" id="done_by_added" value="done_by_added"/>                                                                                                         
                <?php } ?>
            <div id="button_control" class="form_row">
                <a name="save"     class="btn blue btn-info" onclick="$('#form_new_visitor').submit()">Save</a>                          
                <a name="email"    class="btn btn blue btn-info" <?php echo $visitor->id ? 'onclick="member(1,'.$visitor->id.')"' : ''?> >Email</a> 
                <a name="postcard" class="btn btn blue btn-info" <?php echo $visitor->id ? 'onclick="member(2,'.$visitor->id.')"' : ''?> >Postcard</a>
                <a name="forward"  class="btn blue btn-info" <?php echo $visitor->id ? 'onclick="member(4,'.$visitor->id.')"' : ''?> >Fwd contact</a>
                <a name="delete"   class="btn blue btn-info" <?php echo $visitor->id ? 'onclick="delete_contact()"' : ''?> >Delete</a>
                <a name="cancel"   class="btn blue btn-info" onclick="cancel_new_contact()" >Cancel</a>            
                <a name="print"    class="btn blue btn-info" onclick="$('#button_control').attr('name','print');$('#form_new_visitor').submit()">Print</a>            
            </div>
            </div> 
            </fieldset>
        </div>
        <div class="span3">
            <div class="span3 well-content" style="margin-top:45px">
                <div class="profile_pic">
                    <?php 
                    if($visitor->profile_image !='') {
                        $path = parse_url( site_url());
                        $path =  str_replace('index.php','',$path['path']) ; 
                        $path = $path  . 'application/views/assets/img/member/'.$visitor->profile_image;
                        ?><img src="<?php echo $path;?>" /><br><br><?php
                    }
                    ?>

                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                        <input type="file" name="profile_image" id="profile_image">
                      </span>
                      <span class="fileupload-name fileupload-preview"></span>
                      <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"><i class="icon-remove"></i></a>
                    </div>
                <iframe id="frame1" width="250" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Downey%2BCA%2B90241&ie=UTF8&z=12&t=m&iwloc=near&output=embed"></iframe>
                <a class="btn blue" id="map_print" target="_blank" class="btn">Print map</a>
            </div>
        </div> 
        </div>   
        </form>

<?php
function parse( $date ){
return $date;    
}
?>
 <script>
$(function() {
 $( "#spouse" ).autocomplete({
    source: "<?php echo site_url('members/auto_members/')?>",
    minLength: 3,
    select: function( event, ui ) {
        $('#spouse_related_person_id').val(ui.item.id);
        $('#name_spouse_old').val(ui.item.label);
    }
  });
  $( "#spouse" ).keyup(function() {
      var spouse = $('#spouse').val();
      var spouse_old = $('#name_spouse_old').val();
      if(spouse != spouse_old) {
          $('#spouse_related_person_id').val(0);
      }
  });
  $('#select_children').change(
        function(){
            childerAuto();
        }
        );
  childerAuto();
});
function childerAuto() {
    $( ".children .controls .input-normal" ).autocomplete({
    source: "<?php echo site_url('members/auto_members/')?>",
    minLength: 3,
    select: function( event, ui ) {
        var child_div = $(this).attr('id');
        child_div = child_div.replace('_name', '');
        
        $('#'+child_div+'related_person_id').val(ui.item.id);
        //$('#name_spouse_old').val(ui.item.label);
    }
  });
  $( ".children .controls .input-normal" ).keyup(function() {
      var child = $(this).val();
      var child_div = $(this).attr('id');
        child_div = child_div.replace('_name', '');
      var child_old = $(this).parent().find('.old_child').val();
      
      if(child != child_old) {
          $('#'+child_div+'related_person_id').val(0);
      }
  });
}
</script>

   <div id="myModal" class="modal hide fade" style="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
            <h3>&nbsp;</h3>
        </div>
        <div class="modal-body" id="load_view">
        
        </div>
    </div>

   
<style type="text/css">
    .modal.fade.in {
    top: 70%!important;
}
</style>

