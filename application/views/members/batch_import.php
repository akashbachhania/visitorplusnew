<?php 
$error = 'error';

switch( $status ){
    case Visitors::STATUS_SAVED:
        $msg = "New visitors has been created!";
        $class = 'alert alert-success';
    break;
    case Visitors::STATUS_ERROR:
        if( $visitor->errors ){
            $msg = "Plese fill in mandatory fields";
        } else {
            $msg = "There was an error while trying to save visitor";
        }
        
        $class = 'alert alert-error'; 
    break;
}
?>

<div class="row">
    <div id="div_new_visitor" class="span8">
        <?php if( $status ){ ?>
            <div id="div_visitor_added" class="<?php echo $class ?>">
                    <a class="close" data-dismiss="alert">x</a>
                    <?php echo $msg ?>
            </div>
        <?php } ?>    
        <form class="form-horizontal" id="form_new_visitor" action="<?php echo site_url('visitors/batch_import')?>" method="post" enctype="multipart/form-data">
            <fieldset>
                <legend>Import Visitors</legend>
                <div class="control-group">
                    <label class="control-label" for="input01">CSV file:</label>
                    <div class="controls">
                        <input type="file" name="import_contacts" class="input-xlarge" >
                    </div>
                </div>
                
            </fieldset>
            <div id="button_control">
                <input name="save" type="hidden" value="save" />
                <a name="save" style="margin-left: 178px;" class="btn btn-medium btn-info" onclick="$('#form_new_visitor').submit();">Save</a>                           
            </div>                 
        </form>
    </div>    
</div> 