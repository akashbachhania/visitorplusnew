<?php
if ($saved !== null) {
    if ($saved === true) {
        $saved = 'alert alert-success';
        $msg = '<strong>Success: </strong>Your details have been saved';
    } else if ($saved === false) {
        $saved = 'alert alert-error';
        if ($error_msg) {
            $msg = $error_msg;
        } else {
            $msg = '<strong>Error: </strong>Your settings have not been(Please correct fields with red border)';
        }
    }
} else {
    $saved = '';
    $msg = '';
}

$person_fields = array(
    'name', 'address', 'city', 'state', 'zip', 'active'
);
$giving_fields = array(
    'name', 'active'
);
?>
<div class="span8 offset2">
    <?php if ($saved) { ?>
        <div id="div_order_added" class="<?php echo $saved ?>">
            <a class="close" data-dismiss="alert">x</a>
            <?php echo $msg ?>
        </div>
    <?php } ?>
    <form action="<?php echo site_url('dashboard/settings') ?>" enctype="multipart/form-data" method="post">   

        <ul class="nav nav-tabs" style="margin-top: 20px;">
            <li class="active"><a data-toggle="tab" href="#general_settings" id="general_settings_link">General Settings</a></li>
            <?php if ($userLive->id == $primary_contact) { ?>
                <li><a data-toggle="tab" href="#users">Users</a></li>                
            <?php } ?>
            <li><a data-toggle="tab" href="#newsletter_integration">Newsletter Integration</a></li>
            <?php if ($userLive->id == $primary_contact || in_array($userLive->id, $funds)) { ?>
            <li><a data-toggle="tab" href="#finance">Finance Settings</a></li>
            <?php } ?>
        </ul>
        <div class="tab-content">
            <div id="general_settings" class="tab-pane fade in active">

                <div class="form-horizontal well">
                    <fieldset>
                        <legend>Settings</legend>
                        <div class="control-group">
                            <label class="control-label" for="input01">Username</label>
                            <div class="controls">
                                <input type="text" class="input-large" name="username"id="input01" value="<?php echo $userLive->email ?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="input02">Password</label>
                            <div class="controls">
                                <input type="password" class="input-large" name="password" id="password"  value="<?php echo $userLive->password ?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="input02">Repeat password</label>
                            <div class="controls">
                                <input type="password" class="input-large" placeholder="Repeat your password" name="password_2" id="password_repeat"  value="<?php echo $userLive->password ?>">
                            </div>
                        </div>
                        <?php if (isset($file) && $file) { ?>
                            <div class="control-group">
                                <label class="control-label" for="input04">Company logo</label>
                                <div class="controls">
                                    <img src="<?php echo $file; ?>">
                                </div>
                            </div>                                
                        <?php } ?>
                        <?php if ($userLive->id == $primary_contact) { ?>
                            <div class="control-group">
                                <label class="control-label" for="input02">Text new contacts</label>
                                <div class="controls">                                        
                                    <textarea style="height:100px;" class="input-large" placeholder="Text new contacts" name="text_contacts" id="text_contacts"><?php echo $userLive->text_contacts ?></textarea>
                                    <p>Numbers separated by comma</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input02">Automatically Send New Visitors</label>
                                <div class="controls">
                                    <textarea style="height:100px;" class="input-large" placeholder="Automatically Send New Visitors" name="send_emails" id="send_emails"><?php echo $userLive->send_emails ?></textarea>
                                    <p>Email Addresses separated by comma</p>
                                </div>
                            </div>
                        
                        
                            <div class="control-group">
                                <label class="control-label" for="input02">Grant Access of Financial Area</label>
                                <div class="controls">
                                    <?php
                                    $arrayLists = array();
                                    if ($users) {
                                        $listUsers = array();
                                        foreach ($users as $userM) {
                                            if ($userM->person_type == Person::TYPE_USER) {                                                
                                                $listUsers[$userM->id] = $userM->name;
                                            }
                                        }
                                        
                                        if ($vertical_list and count($vertical_list) > 0) {
                                            foreach ($vertical_list as $object) {
                                                $arrayLists[$object->listId] = $object->name;
                                            }
                                        }

                                        echo form_dropdown('grant_access[]', $listUsers, $funds, ' id="grant_access" multiple="multiple" ');

                                    }
                                    ?>
                                </div>
                            </div>
                        <?php } ?>


                        <div class="control-group">
                            <label class="control-label" for="input02"></label>
                            <div class="controls">
                                <input type="submit" class="btn span2" name="save" id="submit" value="Ok">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="input02"></label>
                            <div class="controls">
                                <input type="submit" class="btn span2" name="save" id="btn_window_close" value="Cancel">
                            </div>
                        </div>                                                                                                                           
                    </fieldset>
                </div>



            </div>

            <div id="newsletter_integration" class="tab-pane fade">
                <div class="form-horizontal well">
                    <div class="control-group">
                        <div>
                            Hi <?php echo $userLive->name ?> we'll show you how to setup your newsletter, it's really easy.<br />
                            You will only have to do this once to send out newsletters.<br /><br />
                            <div style="font-family:Arial, Helvetica, sans-serif; color:#090;">
                                <span style="color:#000;">1.</span> Let's setup email provider by <a href="http://go.icontact.com/AFF/?afid=228024&amp;ic_ecmpid=IC_US_AF?utm_source=cj&amp;utm_medium=affiliate&amp;utm_campaign=textlink-better_together_30dayfreetrial2" target="_blank">CLICKING HERE</a>.<br />
                                <span style="color:#000;">2.</span> Login to the account by <a href="https://app.icontact.com/icp/core/externallogin" target="_blank">CLICKING HERE</a><br />
                                <span style="color:#000;">3.</span> Under Application ID paste the Prospecto App ID: <span style="background-color:#FFFBAE">j5cWHiIKVhIhzBZp9MnMCtupC3W9VB3Y</span><br />
                                <span style="color:#000;">4.</span> Create a new password (make it different than your iContact password)<br />
                                <span style="color:#000;">5.</span> User the username and the “application password” you created below<br /><br />
                            </div>
                        </div>
                        <div class="controls">
                            <label class="checkbox">
                                <input id="chk_settings" type="checkbox" <?php echo $userLive->vr_username ? ' checked="checked" ' : '' ?> >iContact?
                            </label>
                        </div>
                    </div>                                
                    <div class="control-group vr_credentials"  <?php echo $userLive->vr_username ? '' : ' style="display:none;" ' ?> >
                        <label class="control-label" for="input01">iC  Username</label>
                        <div class="controls">
                            <input type="text" class="input-large" placeholder="iContact username" name="vr_username"id="input01" value="<?php echo $userLive->vr_username ?>">
                        </div>
                    </div>
                    <div class="control-group vr_credentials" <?php echo $userLive->vr_username ? '' : ' style="display:none;" ' ?>  >
                        <label class="control-label" for="input02">iC  Password</label>
                        <div class="controls">
                            <input type="text" class="input-large" placeholder="iContact password" name="vr_password" id="password"  value="<?php echo $userLive->vr_password ?>">
                        </div>
                    </div>   
                    <?php if ($userLive->vr_username != '' and $userLive->vr_password != '') { ?>
                        <div class="control-group">
                            <label for="inputEmail" class="control-label">Default New Vistor Added</label>
                            <div class="controls">
                                <?php
                                $arrayLists = array();
                                if ($vertical_list and count($vertical_list) > 0) {
                                    foreach ($vertical_list as $object) {
                                        $arrayLists[$object->listId] = $object->name;
                                    }
                                }

                                echo form_dropdown('default_list', $arrayLists, $user->default_list, ' id="default_list" ');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="control-group">
                        <label class="control-label" for="input02"></label>
                        <div class="controls">
                            <input type="submit" class="btn span2" name="save" id="submit" value="Ok">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="input02"></label>
                        <div class="controls">
                            <input type="submit" class="btn span2" name="save" id="btn_window_close" value="Cancel">
                        </div>
                    </div>      
                </div>
            </div>

            <?php if ($userLive->id == $primary_contact) { ?>
                <div id="users" class="tab-pane fade">
                    <div class="well">
                        <fieldset>
                            <legend>Users</legend>
                            <?php
                            if ($users) {
                                $coordinators = array();
                                foreach ($users as $index => &$user) {
                                    if ($user->person_type == Person::TYPE_USER) {
                                        $coordinators[] = $user;
                                    }
                                }

                                echo $this->order_helper->generate_table($coordinators, $person_fields, $this->table, array(
                                    '__href' => array(
                                        'href' => site_url('dashboard/add_new/Person/<id>/show_user'),
                                        'id' => '<id>',
                                        'name' => 'person',
                                    )
                                        )
                                );
                            }
                            ?>
                        </fieldset>
                        <a class="btn" target="_blank" href="<?php echo site_url('dashboard/add_new/Person/' . $userLive->company_id) ?>">New user</a>
                    </div>   
                </div>
            
        <?php } ?>
            
            <?php if ($userLive->id == $primary_contact || in_array($userLive->id, $funds)) { ?>
                <div id="finance" class="tab-pane fade">
                    <div class="well">
                        <?php if ($userLive->id == $primary_contact) { ?>
                            <fieldset>
                                <legend>Users with access Giving area</legend>
                                <?php
                                if ($users) {
                                    $coordinators = array();
                                    foreach ($users as $index => &$user) {
                                        if ($user->person_type == Person::TYPE_USER and in_array($user->id, $funds)) {
                                            $coordinators[] = $user;
                                        }
                                    }

                                    echo $this->order_helper->generate_table($coordinators, $person_fields, $this->table, array(
                                        '__href' => array(
                                            'href' => site_url('dashboard/add_new/Person/<id>/show_user'),
                                            'id' => '<id>',
                                            'name' => 'person',
                                        )
                                            )
                                    );
                                }
                                ?>
                            </fieldset>
                        <?php } ?>
                        
                            <fieldset>
                                <legend>Giving funds</legend>
                                <?php
                                if ($giving) {
                                    

                                    echo $this->order_helper->generate_table($giving, $giving_fields, $this->table
                                    );
                                }
                                ?>
                            </fieldset>
                        <a onclick="add_new_type(this)" data-type="6" value="add_new" name="funds" class="btn" href="javascript:void(0);">New Fund</a>
                    </div>   
                </div>
            <?php } ?>
        </div>
    </form>




</div>


<script type="text/javascript">
    $(document).ready(function() {
        $("#general_settings_link").click();
    });
</script>

