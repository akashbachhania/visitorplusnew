<!-- <div id="div_reports" class="row col-md-12 form-inline  panel panel-default">
    <div id="data-table" class="panel-heading datatable-heading col-md-12">
        <h4 class="section-title">Filter</h4>
    </div>
    <div class="col-md-12">
    <br> 
        <div class="col-md-2"></div> 
        <div class="form-group">
            <input type="text" class="form-control" id="start_date" data-datepicker="datepicker" placeholder="Date start">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="end_date" data-datepicker="datepicker" placeholder="Date end">
        </div>
        <button type="submit" class="btn btn-info" onclick="draw_chart()">Set dates</button>
    </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="div_status" class="alert alert-error">
                        <a class="close" data-dismiss="alert">x</a>
                        <strong>Oops! </strong>There are no statistics matching your filter, please try with different time interval
                    </div>       
                </div>        
            </div>
            <div class="charts">
                <div id="visits_per_sex" class="span6"></div>
                <div id="visits_per_married" class="span6"></div>        
            </div>
            <div class="charts">
                <div id="visits_per_town" class="span6"></div>
                <div id="visitors_per_age" class="span6"></div>        
            </div>
            <div class="charts">
                <div id="visits_per_date" class="span6"></div>
                <div id="visits_per_dow" class="span6"></div>         
            </div>
            <div class="charts">
                <div id="visits_per_visitor" class="span6"></div>        
                <div id="attendance_per_sex" class="span6"></div>        
            </div>
            <div class="charts">
                <div id="attendance_per_married" class="span6"></div>        
                <div id="attendance_per_dow" class="span6"></div>        
            </div>
            <div class="harts">
                <div id="attendance_per_date" class="span6"></div>
                <div id="giving_amount_by_fund" class="span6"></div>
            </div>
                <div class="charts">    
                    <div id="giving_amount_by_member" class="span6"></div>     
                </div>

    </div>
    <div class="charts col-md-12">    
        <div id="giving_amount_by_member" class="span6"></div>     
        <div class="span6">Grand Total : <i id="grand_total_funding"></i></div>     
    </div>
 -->



            <div class="widgets_area">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="well red">
                            <div class="well-header">
                                <h5>Filter</h5>
                            </div>

                            <div class="well-content no-search">
                                <div class="form_row">
                                    <div class="field">
                                        <input type="text" class="span3" id="start_date" data-datepicker="datepicker" placeholder="Date start">
                                        <input type="text" class="span3" id="end_date" data-datepicker="datepicker" placeholder="Date end">
                                        <button type="submit" class="btn blue" onclick="draw_chart()">Set dates</button>

                                    </div>
                <div class="span12">
                    <div id="div_status" class="alert alert-error">
                        <a class="close" data-dismiss="alert">x</a>
                        <strong>Oops! </strong>There are no statistics matching your filter, please try with different time interval
                    </div>       
                </div>        
            <div class="charts">
                <div id="visits_per_sex" class="span6"></div>
                <div id="visits_per_married" class="span6"></div>        
            </div>
            <div class="charts">
                <div id="visits_per_town" class="span6"></div>
                <div id="visitors_per_age" class="span6"></div>        
            </div>
            <div class="charts">
                <div id="visits_per_date" class="span6"></div>
                <div id="visits_per_dow" class="span6"></div>         
            </div>
            <div class="charts">
                <div id="visits_per_visitor" class="span6"></div>        
                <div id="attendance_per_sex" class="span6"></div>        
            </div>
            <div class="charts">
                <div id="attendance_per_married" class="span6"></div>        
                <div id="attendance_per_dow" class="span6"></div>        
            </div>
            <div class="harts">
                <div id="attendance_per_date" class="span6"></div>
                <div id="giving_amount_by_fund" class="span6"></div>
            </div>
            <div class="charts">    
                <div id="giving_amount_by_member" class="span6"></div>     
            </div>
    <div class="charts span12">    
        <div id="giving_amount_by_member" class="span6"></div>     
        <div class="span6">Grand Total : <i id="grand_total_funding"></i></div>     
    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>            
</div>
<style>
    .charts {
        float: left;
    }
</style>
