<?php
   
?>
<div id="div_view_transactions" class="row div_header">
    <div id="div_transactions_table" class="span12 well">
        <legend>Orders</legend>
        <?php echo $this->order_helper->get_order_table( $transactions, array(
                    'id',
                    'amount',
                    'status',
                    //'order_date',
                    'datetime',
              ), 
              $this->table);
        ?>
    </div>                
</div>
