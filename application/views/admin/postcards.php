<?php    
foreach ($postcards as $postcard) {
    /**
    * @var Letter
    */
    $postcard;
    $this->html_table->add_row( array(
        '<img  id="'.$postcard->id.'" src="'.str_replace('index.php','',site_url()).'application/views/assets/img/add-icon.png'.'"  border="1"/>',
        '<img  class="img-polaroid" src="'.$path.'/'.$postcard->path_thumb.'" height="50" width="100" border="1"/>',
        array( 
            'data'=>$postcard->description, 
            'attributes'=>array('id'=>'td_description_'.$postcard->id)),         
        array( 
            'data'=>$postcard->price ? $postcard->price :'$0.00', 
            'attributes'=>array('id'=>'td_price_'.$postcard->id)),        
        array( 
            'data'=>
                (($postcard->type_front_side_letter) ? 'Envelope<br>': ''). 
                (($postcard->type_back_side_letter) ? 'Letter inside<br>': ''). 
                (($postcard->type_front_side_postcard) ? 'Front side<br>': ''). 
                (($postcard->type_back_side_postcard) ? 'Front side<br>': ''), 
            'attributes'=>array(
                'id'=>'td_type_'.$postcard->id,
                'data-front-postcard'=> $postcard->type_front_side_postcard ? 'true' : 'false',
                'data-back-postcard' => $postcard->type_back_side_postcard  ? 'true' : 'false',
                'data-front-letter'  => $postcard->type_front_side_letter   ? 'true' : 'false',
                'data-back-letter'   => $postcard->type_back_side_letter    ? 'true' : 'false',
                )
            ),                                                
        isset($postcard->_order) ? count($postcard->_order) : 0,
        array( 
            'data'=>($postcard->active) ? 'Active': 'Disabled', 
            'attributes'=>array('id'=>'td_active_'.$postcard->id)),
        ),
        array(
            'id' => 'row_'.$postcard->id,
            'class' =>  $postcard->active ? 'info': 'warning' 
        )        
    );
}
$this->html_table->set_heading(array('','Picture','Description','Price','Type','Sold','Delete'));
$table = $this->html_table->generate();

function get_popover_data( $field, $title ){     
    return '<span data-content="'.$field.'" rel="popover" data-original-title="'.$title.'">'.substr($field,0,100).(strlen($field)>100?'...':'').'</span>';
}  
?>
<div id="div_postcards" class="row">

    <div id="div_postcards_inner" class="span12">
    <?php if( isset($status) && $status  == 'success' ){ ?>
        <div id="div_order_added" class="alert alert-success">
                <a class="close" data-dismiss="alert">�</a>
                <strong>Postcard has been uploaded</strong>
        </div>
    <?php } ?>    
            <?php echo $table ?>
    <a id="btn_add_new"  role="button" class="btn btn-info" data-toggle="modal">Add Postcard</a>  
    </div>
    
</div>

<div class="modal <?php  echo ($status == 'fail' ?'': 'fade');?>" id="div_add_new" tabindex="-1" role="dialog" aria-labelledby="Add new Postcard" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add new postcard</h3>
    </div>
    <div class="modal-body">
        <form id="form_add_new" action="<?php echo site_url('admin/add_new_postcard')?>" method="post" class="form-horizontal" enctype="multipart/form-data">             
            <div id="div_order_added_id" class="<?php echo $status == null ? 'hidden': ($status == 'success' ? 'alert alert-success' : 'alert alert-error') ?>">
                    <a class="close" data-dismiss="alert">�</a>
                    <span>Please correct red fields</span>
            </div>
            <div class="control-group  <?php echo isset($new_letter->errors['_file'])?'error':'' ?> ">
                <label for="inputEmail" class="control-label">File</label>
                <div class="controls">
                    <input type="file" placeholder="Please select file" value="" name="file"/>
                </div>
           </div>                
            <div class="control-group <?php echo isset($new_letter->errors['description'])?'error':'' ?> ">
                <label for="inputEmail" class="control-label">Description</label>
                <div class="controls">
                    <input type="text" placeholder="Postcard description" value="<?php echo $new_letter->description ?>" name="description"/>
                </div>
           </div>
           <div class="control-group <?php echo isset($new_letter->errors['price'])?'error':'' ?>"> 
                <label for="inputEmail" class="control-label">Price</label>
                <div class="controls">
                    <input type="text" placeholder="Price" value="<?php echo $new_letter->price ?>" name="price"/>
                </div>
           </div>
           <div class="control-group <?php echo isset($new_letter->errors['type'])?'error':'' ?>"> 
                <label for="inputEmail" class="control-label">Type</label>
                <div class="controls">
                <?php echo form_dropdown('type', array(1=>'Front side',2=>'Back side'), $new_letter->type)?>
                </div>
           </div>
           <div class="control-group <?php isset($new_letter->errors['active'])?'error':'' ?>"> 
                <label for="inputEmail" class="control-label">Status</label>
                <div class="controls">
                <?php echo form_dropdown('active', array(1=>'Active',2=>'Disabled'), $new_letter->active)?>                
                </div>
           </div>                                                
        </form>
    </div>
    <div class="modal-footer">
         <a class="btn btn-info" onclick="$('#form_add_new').submit()">Save</a>
         <a class="btn btn-info" id="btn_close_id" onclick="">Cancel</a>
    </div>
</div>