(function($) {

	"use strict";
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	
	if(dd<10) {
		dd='0'+dd
	} 
	
	if(mm<10) {
		mm='0'+mm
	} 
	
	today = yyyy+'-'+mm+'-'+dd;

	var options = {
		view: 'month',
                events_source: 'schedule/events_show/',
		tmpl_path: String(window.location).match(/(.*)index.php/).pop()+"application/views/assets/callenders/",
		tmpl_cache: false,
		day: today,
                modal: "",
                modal_type : "",
                views:{ 
                        day:   {
				enable: 0
			}
		},
		onAfterEventsLoad: function(events) {
			if(!events) {
				return;
			}
			var list = $('#eventlist');
			list.html('');

			$.each(events, function(key, val) {
				$(document.createElement('li'))
					.html('<a href="' + val.url + '">' + val.title + '</a>')
					.appendTo(list);
			});
		},
		
		onAfterViewLoad: function(view) {
			$('.page-header h3').text(this.getTitle());
			$('.btn-group button').removeClass('active');
			$('button[data-calendar-view="' + view + '"]').addClass('active');

			$(".event").unbind('click').click(function(e) {
				showEditElement($(this));
				$('#add-events-modal').modal('show');
			});

			//handle for edit event function.
			$(".event").hover(function() {
				/* Stuff to do when the mouse enters the element */
				$(this).removeClass('event-info').addClass('icon-edit');
				$(this).removeClass('event');
				$(this).css({
					'margin-right': '2px',
					'margin-bottom': '2px'
				});

			}, function() {
				/* Stuff to do when the mouse leaves the element */
				$(this).removeClass('icon-edit').addClass('event-info');
				$(this).addClass('event');
			});
		},
		classes: {
			months: {
				general: 'label'
			}
		}
		
	};

	var calendar = $('#calendar').calendar(options);

	$('.btn-group button[data-calendar-nav]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.navigate($this.data('calendar-nav'));
		});
	});

	$('.btn-group button[data-calendar-view]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.view($this.data('calendar-view'));
		});
	});

	$('#first_day').change(function(){
		var value = $(this).val();
		value = value.length ? parseInt(value) : null;
		calendar.setOptions({first_day: value});
		calendar.view();
	});

	$('#language').change(function(){
		calendar.setLanguage($(this).val());
		calendar.view();
	});

	$('#events-in-modal').change(function(){
		var val = $(this).is(':checked') ? $(this).val() : null;
		calendar.setOptions({modal: val});
	});

	$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
		//e.preventDefault();
		//e.stopPropagation();
	});

	//edit schedule
	function showEditElement(El){
		var date_schedule = El.data('date');
		$("#add-events-modal h3").text('Edit Schedule For '+ date_schedule);
		$("#add-events-modal .datepickergroup").hide();
		$("#add-events-modal button.btn").first().html('Update Schedule').attr('data-isedit', 'true');
		var time = El.parent().data('cal-start');
		$("#edit_from_time").val(time);
		var event_id = El.data('event-id');
		var ministry_id = El.data('event-ministryid');
		var title = El.data('original-title');
		var date_schedule = El.data('date');

		var i = title.indexOf(":");
		var ministryName = title.substr(0, i).trim();
		// var users = title.substr(i+1);
		
		var userList = El.find(".member-id-edit");
	
		$('#add_relations_div').html("<ul></ul>");
		userList.each(function(index, el) {
			var name = $(el).text();
			var personid = $(el).attr('id');
			var sid = $(el).data('sid');
			var remove = '<label style="width:20px;" class="inline_label label_referer"><i onclick="deleteEditRelation(\''+index+'\')" style="cursor:pointer; color:red;">Remove</i></label>';
	        $('#add_relations_div').css('display','block');
	        $('#add_relations_div ul').append('<li id="edit_' + index + 'div">' + ministryName + ': ' + name + remove + '</li>');
	        $('#add_relations_div ul').append('<input type="hidden" name="member_id[' + ministry_id + ']['+sid+'][]" value="'+personid+'">');
	        // $('#add_relations_div ul').append('<input type="hidden" name="sid" value="'+sid+'">');
		});

		// $("#add_eventFrm").append('<input type="hidden" name="event_id" id="idofevent" value="'+event_id+'">');

        $("#date_schedule").val(date_schedule);
	}
        
}(jQuery));