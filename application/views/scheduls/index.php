<link rel="stylesheet" href="<?php echo base_url() . 'application/views/assets/style/calendar.min.css' ?>">
<div class="page-header">
    <div class="pull-right form-inline">
        <div class="btn-group">
            <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
            <button class="btn" data-calendar-nav="today">Today</button>
            <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
        </div>
        <div class="btn-group">
            <button class="btn btn-warning" data-calendar-view="year">Year</button>
            <button class="btn btn-warning active" data-calendar-view="month">Month</button>
            <button class="btn btn-warning" data-calendar-view="week">Week</button>
        </div>
    </div>
    <h3></h3>
    <a href="javascript:void(0);" onclick="addEvent();" class="btn btn-primary btn-primary">Add Event</a>
</div>
<div id="calendar"></div>

<div class="horizontal_center" style="width: 37%; margin-top: 30px;">
  <input type="button" id="btn_email" name="action" value="Email" onclick="scheduleActions('email')" class="btn span2 btn-info" disabled="">
  <input type="button" id="btn_print" name="action" value="Print"  onclick="scheduleActions('print')" class="btn span2 btn-info">
</div>


<div class="modal hide fade" id="events-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Event</h3>
    </div>
    <div class="modal-body" style="height: 400px">
    </div>
    <div class="modal-footer">
        <a href="#" data-dismiss="modal" class="btn">Close</a>
    </div>
</div>
<style>
    .datepickerdiv .datepicker {
        left:1px!important;
        top:66px!important;
    }
</style>
<a href="#." id="print_action_url" target="_blank" style="opacity: 0;">Print Schedule</a>
<script>
    jQuery(document).ready(function($) {
        $('#add-events-modal').on('hidden.bs.modal', function (event) {
            $('#add-events-modal h3').text('Add Schedule');
            $("#add_relations_div").html("<ul></ul>").hide();
            $("#date_schedule").val('');
            $("#idofevent").remove();
            $("#add-events-modal button.btn").first().html('Add Schedule').attr('data-isedit', 'false');
            $("#add-events-modal .datepickergroup").show();
        })

        $('#add-events-modal .add_schedule').click(function(event) {
        if($(this).attr('data-isedit') =="true"){
                editSchedual();
            }else{
                addSchedual();
            }
        });
    });

    function addRelation() {
        var member_id = $('#member_id').val();
        var ministry_id = $('#ministry_id').val();
        // if (ministry_id == null || role_id == '') {
        //     $('#ministry_id').css('border-color', 'red');
        //     return false;
        // }else 
        if (member_id == null || member_id == '') {
            $('#member_id').css('border-color', 'red');
            return false;
        }
        $('#member_id').css('border-color', '#cccccc');
        
        var ministry_value = $('#ministry_id [value='+ministry_id+']').text();
        member_id = member_id  + '';
        var members = member_id.split(",");
        var member_value = ''; 
        var member_ids = '';
        var inputValues = '';
        $.each(members, function(index, value) {
            member_value += $('#member_id [value='+value+']').text()+ ', ';
            member_ids += value+ '_';
            inputValues += '<input type="hidden" name="member_id['+ministry_id+'][]" value="'+value+'">';
        });
        var lenght = member_value.length - 2;
        member_value = member_value.substring(0,lenght);
        var div_ids = ministry_id+'_'+member_ids;
        var remove = '<label style="width:20px;" class="inline_label label_referer"><i onclick="deleteRelation(\''+div_ids+'\')" style="cursor:pointer; color:red;">Remove</i></label>';
        $('#add_relations_div').css('display','block');
        $('#add_relations_div ul').append('<li id="relation_'+div_ids+'div">'+ministry_value+': '+member_value+remove+inputValues+'</li>');
    }

    function deleteRelation(div_ids) {
        $('#relation_'+div_ids+'div').remove();
    } 

    function deleteEditRelation(div_ids) {
        $('#edit_'+div_ids+'div').hide();
        var value = $('#edit_'+div_ids+'div').next().attr('name');
        var value = 'delete_' + value;
        $('#edit_'+div_ids+'div').next().attr('name', value);
    }

    function addEvent() {
        $('#add-events-modal').modal('show');
    }
    function addSchedual() {
        var ministry_id = $('#ministry_id').val();
        var date_schedule = $('#date_schedule').val();
        var member_id = $('#member_id').val();
        var role_id = $('#role_id').val();

        if (date_schedule == '') {
            $('#date_schedule').css('border-color', 'red');
            return false;
        }

        else if (ministry_id == '') {
            $('#ministry_id').css('border-color', 'red');
            return false;
        } 

        else if (member_id == '') {
            $('#member_id').css('border-color', 'red');
            return false;
        } 

        // else if (role_id == '') {
        //     $('#role_id').css('border-color', 'red');
        //     return false;
        // }
        $('#ministry_id, #date_schedule,#member_id,#role_id').css('border-color', '#cccccc');

        $.ajax({
            url: "<?php echo base_url() ?>index.php/schedule/ajax_add_schedule",
            type: 'POST',
            data: $('#add_eventFrm').serialize(),
            success: function(data) {
                var obj = $.parseJSON(data);
                if (obj.success) {
                    //alert(obj.success);
                    $('#add-events-modal').modal('hide');
                    document.location.href = document.location.href;
                }
            }
        });
    }

    function editSchedual() {
        var ministry_id = $('#ministry_id').val();
        var date_schedule = $('#date_schedule').val();
      
        if (date_schedule == '') {
            $('#date_schedule').css('border-color', 'red');
            return false;
        } 


        $('#ministry_id, #date_schedule').css('border-color', '#cccccc');

        $.ajax({
            url: "<?php echo base_url() ?>index.php/schedule/ajax_edit_schedule",
            type: 'POST',
            data: $('#add_eventFrm').serialize(),
            success: function(data) {
                var obj = $.parseJSON(data);
                if (obj.success) {
                    $('#add-events-modal').modal('hide');
                    document.location.reload();
                }
            }
        });
    }


    function refreshItems() {
        var ministry_id = $('#ministry_id').val();
        if (ministry_id != '') {
            $.ajax({
                url: "<?php echo base_url() ?>index.php/schedule/get_members",
                type: 'POST',
                data: 'ministry_id=' + ministry_id,
                success: function(data) {
                    var obj = $.parseJSON(data);
                    if (obj.success) {
                        $('#role_id').html('');
                        $.each(obj.roles, function(index, value) {
                            $('#role_id').append($('<option>').text(value).attr('value', index));
                        });
                    } else {
                    }
                }
            });
        }
    }

    function listingMembers(value) {
        if (value != '') {
            $.ajax({
                url: "<?php echo base_url() ?>index.php/schedule/get_members",
                type: 'POST',
                data: 'ministry_id=' + value,
                success: function(data) {
                    var obj = $.parseJSON(data);
                    if (obj.success) {
                        $('#member_id').html($('<option>').text('Select Item').attr('value', ''));
                        $.each(obj.members, function(index, value) {
                            $('#member_id').append($('<option>').text(value.name).attr('value', value.id));
                        });

                        // $('#role_id').html('');
                        // $.each(obj.roles, function(index, value) {
                        //     $('#role_id').append($('<option>').text(value).attr('value', index));
                        // });
                    } else {
                    }
                }
            });
        }
    }
    
    function scheduleActions(action) {
        var get_date = $('.page-header h3').html();
                    $.ajax({
                        url: "<?php echo base_url() ?>index.php/schedule/schedule_email",
                        type: 'POST',
                        data: 'action=' + action+'&month='+get_date,
                        success: function(data) {
                            if(action == 'email') {
                                alert('Done');
                            } else if('print') {
                               
                                window.open(data);
                            }
                        }
                    });        
    }

    function c_lick(action){
        $('.events-list a[data-event-id='+action+']').trigger('click');
    }
</script>
<style>
    .tooltip {
        background-color: #C4E5EE;
        padding: 3px;
        opacity: 1!important;
    }
    .tooltip > div {
        background-color: #FDF9D1;
        opacity: 1!important;
        color: black;
        border-radius: 0px;
        padding: 10px;
    }
    .tooltip-arrow {
        display: none;
    }
    .btn-warning {
    margin-right: 3px!important;
}
</style>

<div class="modal fade" id="add-events-modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Add Schedule</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" id="add_eventFrm">
            <button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
            <div class="control-group datepickergroup" style="position: relative;">
                <label class="control-label">Date</label>
                <div class="controls datepickerdiv">
                    <input type="text" name="date_schedule" id="date_schedule" class="form-control" data-datepicker="datepicker" />
                </div>
            </div>
            <div class="control-group" style="position: relative;">
                <label class="control-label"></label>
                    <select class="form-control" name="tym_type" style="width: 100px;">
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                    </select>
            </div>

            <div class="control-group">
                <label class="control-label">Ministry</label>
                <div class="controls">
                    <?php echo form_dropdown('ministry_id', $ministries, '', ' id="ministry_id" class="form-control" data-type="' . Type::TYPE_MINISTRY . '"  onchange="listingMembers(this.value)" ') ?>
                </div>
            </div>
            <div class="control-group">  
                <hr>
                <div id="add_relations_div" style="border: 2px solid red; margin-bottom: 5px; display: none;">
                    <ul></ul>
                </div>
               
                <label class="control-label">Members</label>
                <div class="controls">
                    <select class="form-control" id="member_id" multiple="">
                        <option value="">Select Item</option>
                    </select>
                    <a href="javascript:void(0)" onclick="addRelation();" class="btn btn-primary">Add</a>
                </div>
                <input type="hidden" name="edit_from_time" value="" id="edit_from_time"></input>
                <hr>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-primary add_schedule" type="button" data-isedit="false">Add Schedule</button>
                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>

    </div>
    <div class="modal-footer">

    </div></div></div>
</div>
