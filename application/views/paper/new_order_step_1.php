<?php
  
if( $step == 1){
    switch( $order_type ){
        case Order::ORDER_LETTER:
            $label = 'Choose the envelope of your letter';
            $legend = 'Select letter to mail';
        break;
        case Order::ORDER_POSTCARD:
            $label = 'Choose the front of your postcard';
            $legend = 'Select postcard to mail';
        break;
    }
    $var   = 'front_side';
    $action = site_url('paper/new_order_step_2'. ($postcard->id ? "/$postcard->id" :''));
    $field = 'front_letter_id';
} else {
    switch( $order_type ){
        case Order::ORDER_LETTER:
            $label = 'Choose the inside of your';
            $legend = 'Select letter to mail';
        break;
        case Order::ORDER_POSTCARD:
            $label = 'Choose the back side of your postcard';
            $legend = 'Select postcard to mail';
        break;
    }
    $var    = 'back_side';
    $action = site_url('paper/final_step' . ($postcard->id ? "/$postcard->id" :''));
    $field = 'back_letter_id'; 
}

 
$images = '';   
foreach( $letters as $letter ){        
   $images .= '<a  href="'.$path.'/'.$letter->path_full.'"><img id="'.$letter->id.'" src="'.$path.'/'.$letter->path_thumb.'" data-title="My title" data-description="'.$letter->description.'"></a>';
}
   
 
?>
<form action="<?php echo $action ?>" method="post">
    <div id="div_print" class="row-fluid">
          <div class="span2"></div>  
          <div id="div_print_inner" class="span8">
            <div class="well red">
                <div class="well-header">
                    <?php if( isset($status) && $status ){ ?>
                        <div id="div_order_added" class="alert alert-error">
                                <a class="close" data-dismiss="alert"></a>
                                <strong>Oops! </strong><?php echo $status ?>
                        </div>
                    <?php } ?>
                    <h5><?php echo $legend?></h5>
                </div>

                <div class="well-content no-search">
                    <div id="div_recipients" class="form_row">
                      <label class="field_name align_right"></label>
                      <div class="field">
                        <select id="select_recipients" name="recipients[]" multiple="multiple" size="5">
                        <?php foreach ( $visitors as $visitor ) {?>
                            <option value="<?php echo $visitor->id; ?>" <?php echo isset($selected_visitors[$visitor->id]) ? 'selected="selected"' : '' ?>> <?php echo $visitor->first_name .' '. $visitor->last_name ?> </option>
                        <?php } ?>       
                        </select>
                        <a class="btn" id="select_all" data-state="select">Select all</a>              
                      </div>
                    </div>
                </div>

            </div>
          </div>
    </div>
    <div class="row-fluid">
        <div class="span2"></div>
        <div class="span8">
            <div class="well red">
                <div class="well-header">
                    <h5><?php echo $label ?></h5>
                </div>
                <div class="well-content no-search">
                    <div id="div_covers">
                        <div id="div_gallery_frame">
                            <div id="div_galleria">
                                <?php echo $images; ?>
                            </div>
                        </div>

                        <?php if( $step == 2 ){ ?>
                            <div class="hero-unit form_row">
                                <legend class="center">Enter your message here</legend>
                                <textarea name="text" class="text_centered"><?php echo ($postcard->text ? $postcard->text : '') ?></textarea>
                                <input type="hidden"  id="back_side" name="back_side" value=""/>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2"></div>
        <div class="span8">
            <div class="well">
                <div class="well-content no-search" style="background: none;border: none;">
                    
                    <input class="btn blue btn-info" type="submit" id="btn_next"   name="next"   value="Next"/>
                    <input class="btn light btn-info" type="submit" id="btn_cancel" name="cancel" value="Cancel"/>
                    <input type="hidden" id="front_side" name="front_side" value=""/>

                </div>
            </div>
        </div>
    </div>
</form>