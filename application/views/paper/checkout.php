<?php
    
if( $step == 1){
    $label = 'Choose the front of your postcard';
    $var   = 'front_side';
    $action = site_url('paper/new_order_step_2');
} else {
    $label  = 'Choose the back side of your postcard';
    $var    = 'back_side';
    $action = site_url('paper/final_step');
}

$this->table->set_heading(array('Select','Postcard'));
$row = array();    
foreach( $letters as $letter )  {
    /**
    * @var Letter
    */
    $letter;
    
    if(count($row)<2){
        $row[] = form_radio($var, $letter->id, false) . '<img class="img_cover" src="'.$path.'/'.$letter->path_thumb.'" height="450" width="300" border="1"/>';
    } else {
        $this->table->add_row($row);
        $row = array();
    }
}
$table = $this->table->generate();


?>
<div id="div_print" class="row">
    <div id="div_print_inner" class="span12">
        <div id="div_title">Select postcard to mail</div>             
        <form action="<?php echo $action ?>" method="post">          
            <?php
            echo '<div id="div_recipients">';
                echo '<select id="select_recipients" name="recipients[]" multiple="multiple" size="5">';
                foreach ( $visitors as $visitor ) {
                    echo '<option value="'.$visitor->id.'" ' .(isset($selected_visitors[$visitor->id]) ? ' selected="selected"' : '') . ' >' . $visitor->first_name .''.$visitor->last_name. '</option>';
                }       
                echo '</select>';
            echo '</div>';
            echo '<div id="div_covers">';
            echo '<div class="div_label">'.$label.'</div>';
            echo $table;
            if( $step == 2 ){
                echo '<div class="div_label">Enter your message here</div>';
                echo '<textarea name="text" class="text_centered"></textarea>';
            }
            echo '</div>';
            ?> 
        </form>        
    </div>
</div>
