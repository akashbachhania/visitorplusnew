<?php
if(!isset( $object )){
    die('Access denied');
}
       
$msg  = '';                       
if( isset( $saved ) && $saved !== null){
    if( $saved === true ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong> Added Successfully!';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your query has not been saved (Please correct fields with red border)';            
    } else if ( $saved === 'update') {
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>updated Successfully!';             
    }
} 

$name = '';
$fields = array();
switch( get_class( $object ) ){
    case 'Company':
        $name = 'New Company';
        $fields = array('name','address','city','state','zip','phone_1','phone_2','email','active','id');
    break;
    case 'Person':
        if( $object->person_type == Person::TYPE_USER ){
            $name = 'User';
            $fields = array(
               'name','address','city','state','zip','phone_1','phone_2','email','password', 'active','id','company_id','person_type',
            );                    
        } 
        
    break;
    case 'Ministry':
        $name = 'Ministry Role';
        $fields = array('name','active','id','company_id','ministry_id');   
        
    break;
    case 'Type':
        switch( $object->type ){
            case Type::TYPE_ASSIGNMENT:
                $name = 'Assignment';
                $fields = array(
                               'name','description','active','id','company_id','type'
                            );            
            break;
            case Type::TYPE_MINISTRY:
                $name = 'Ministry';
                $fields = array(
                               'name','description','active','id','company_id','type'
                            );            
            break;
            case Type::TYPE_FUNDS:
                $name = 'Funds';
                $fields = array(
                               'name','active','id','company_id','type'
                            );            
            break;
            case Type::TYPE_LANGUAGE:
                $name = 'Language';
                $fields = array(
                               'name','description','active','id','company_id','type'
                            );            
            break;
            case Type::TYPE_REFERER:
                $name = 'Referered By';
                $fields = array(
                               'name','description','active','id','company_id','type'
                            );            
            break;
            case Type::TYPE_DONE_BY:
                $name = 'Done By';
                $fields = array(
                               'name','description','active','id','company_id','type'
                            );            
            break;                         
        }
         
    break;
}
$options = array(
    1 => 'Active',
    0 => 'Disabled'
)
?>
                       
    <div id="div_row_add_new">
        <div id="div_add_new" >
            
            <?php if( $saved ){ ?>
                <?php if( $object->type == Type::TYPE_DONE_BY || $object->type == Type::TYPE_ASSIGNMENT || $object->type == Type::TYPE_MINISTRY || $object->type == Type::TYPE_FUNDS){ ?>
                    <div id="div_assignment_item_added" data-id="<?php echo $control_id ?>" class="<?php echo $saved ?>">
                            <a class="close" data-dismiss="alert">x</a>
                            <?php echo $msg ?>
                    </div>                    
                <?php } else {?>
                    <div id="div_item_added" class="<?php echo $saved ?>">
                            <a class="close" data-dismiss="alert">x</a>
                            <?php echo $msg ?>
                    </div>
                <?php } ?>
            <?php } ?>             
         
                
                
                <?php if( isset( $objects )){ 
                        $array = array();
                        foreach( $objects as $temp_object ){
                            $array[ $temp_object->id ] = $temp_object->name;
                        }
                        $array['0'] = '-Select item-';                        
                        $dropdown = form_dropdown('select_edit',$array, isset($selected_item)? $selected_item :'0','onchange="change_val()"');
                ?>
                        <div id="div_add_new_dropdown" class="form-horizontal well">
                                <legend>Item to edit</legend>
                                <br>
                                <div name="controls">
                                    <div class="control-group">
                                        <label class="control-label" for="type">Item</label>
                                        <div class="controls">
                                            <?php echo $dropdown; ?>
                                         </div>
                                    </div>
                                </div>
                            
                        </div>
                         
                <?php } ?>

                <?php  
                    $hidden = array(
                            'object_type' => get_class( $object ),                            
                            'control_id'  => $control_id,                            
                    );
                    $specials = array(
                                  'active' => array( 
                                        'element'  => Output_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $options,
                                        'selected' => $object->active ),                     
                                    '__legend' => ''. $name,
                                    '__form'   => array(
                                        'action' => $action, 
                                        'submit_value' => 'Save',
                                        'submit_name'  => 'save',
                                        'close_btn'   =>  'close',
                                        'class'        => 'btn blue',
                                        'hidden' => $hidden
                                        )
                    );
                                           
                    if( get_class( $object) == 'Person' ){
                        $hidden['person_type'] = $object->person_type;
                        $specials = $specials + array ('state'=>
                                                    array('element'=>  Output_helper::ELEMENT_DROPDOWN,
                                                          'options'=> $states,
                                                          'selected'=>$object->state ));

                         
                    } else if( get_class( $object) == 'Company'){
                        $specials = $specials + array ('state'=>
                                                    array('element'=> Output_helper::ELEMENT_DROPDOWN,
                                                          'options'=> $states,
                                                          'selected'=>$object->state ));
                    }
                    echo $this->output_helper->get_object_form( $object, $fields, $this->table, $specials );
                ?>
               
                <?php if( isset($select_name)){ ?>
                <input type="hidden" value="<?php echo $select_name ?>" id="select_refresh"/>
                <?php }?>
                
            </div>
     </div>
<script>
    $('input[name="btn_cancel"], #c_btn').click(function() {
       $('#myModal').modal('hide');
       console.log($('[name="control_id"]').val() );
       var i = $('[name="control_id"]').val();
       $('select#'+ i + ' option:nth-child(1)').attr('selected','selected');    
    });

</script>
                 

