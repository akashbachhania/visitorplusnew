
<?php
$from ='';
$to = '';
$subject = '';

switch( $status ){
    case Email::STATUS_SENT:
        $msg = "Emails have been sent!";
        $class = 'alert alert-success';
    break;    
    case Email::STATUS_SAVED:
        $msg = "New template has been created!";
        $class = 'alert alert-success';
    break;
    case Email::STATUS_UPDATE:
        $msg = "Template has been updated";
        $class = 'alert alert-info'; 
    break;
    case Email::STATUS_ERROR:
        $msg = "Error while saving template, please fill in all fields";
        $class = 'alert alert-error'; 
    break;
    case Email::STATUS_NOTSENT:
        $msg = "One or more mails couldnt be sent at this time, please contact server administrator";
        $class = 'alert alert-error'; 
    break;    
    default:
        $msg = '';
        $class = '';
}

$mail_body = '';
$mail_from = '';
$mail_to   = '';
$mail_from = '';
$mail_subject = '';
 
if(isset($errors) and $errors !='') {
    $msg = $errors;
}

if( !isset($show_event)){
    $show_event = false;
}

$makeReadOnly = '';
if($message)
    $makeReadOnly = 'readonly="readonly"';
?>
<div class="widgets_area">
  <div class="row-fluid">
      <div class="span12">
          <div class="well red">
              <div class="well-header">
                  <h5>Send email From iContact</h5>
              </div>

              <div class="well-content no-search">
                  <a id="a_vertical" data-placement="right" rel="tooltip"  data-original-title="Click to export contacts" href="" role="button" data-toggle="modal"><img src="<?php echo str_replace('index.php','',site_url()).'application/views/assets/img/'?>verticalresponse-logo_0.png"/></a>
                  <?php if( $status ){ ?>
                  <div id="div_order_added" class="<?php echo $class ?>">
                          <a class="close" data-dismiss="alert">x</a>
                          <?php echo $msg ?>
                  </div>
                  <?php } ?>
                  <form class="form-horizontal" id="form_visitors" action="<?php echo site_url('email/process_icontact')?>" method="post">
                    <div class="form_row">
                      <label class="field_name align_right">From:</label>
                      <div class="field">
                      <?php
                       $arrayCamp = array('' => '~*~ Select ~*~');
                       if($campigns and isset($campigns->campaigns) and is_array($campigns->campaigns) and count($campigns->campaigns) > 0){
                           foreach ($campigns->campaigns as $object) {
                               $arrayCamp[$object->campaignId] = $object->name .' -- '.$object->fromName.' -- '.$object->fromEmail;
                           }
                       }
                       
                       echo form_dropdown('camp_id', $arrayCamp, (isset($message) and $message->campaignId) ? $message->campaignId : '', 'id="camp_id" class="span5" onchange="getMessages(this.value);" '.$makeReadOnly) ?>
                      </div>
                    </div>

                    <div class="form_row">
                      <label class="field_name align_right">Messages:</label>
                      <div class="field">
                        <?php
                         $arrayMessages = array('' => '~*~ Select ~*~');
                         if(isset($message) and $message->campaignId and is_array($messages) and count($messages) > 0) {
                             foreach ($messages as $object) {
                                 if($object->campaignId == $message->campaignId)
                                      $arrayMessages[$object->messageId] = $object->subject;
                             }
                         }
                        echo form_dropdown('message_id', $arrayMessages, $message_id, ' id="message_id" class="span5" onchange="getMessage(this.value);" ') ?>
                      </div>
                    </div>

                    <div class="form_row">
                      <label class="field_name align_right">Lists:</label>
                      <div class="field">
                        
                          <?php
                             $arrayLists = array();
                             if(is_array($vertical_list) and count($vertical_list) > 0){
                                 foreach ($vertical_list as $object) {
                                     $arrayLists[$object->listId] = $object->name;
                                 }
                             }
                             
                             echo form_dropdown('list_name', $arrayLists, '', ' id="list_name" class="span5"') ?>
                      </div>
                    </div>

                    <div class="form_row">
                      <label class="field_name align_right">Subject:</label>
                      <div class="field">
                       <input id="subject" type="text" class="span5 <?php echo $mail_subject ?>" name="mail_subject" value="<?php echo (isset($message) and $message->subject !='') ? $message->subject : ''?>" <?php echo $makeReadOnly;?>/>
           
                      </div>
                    </div>
                    <div class="form_row">
                      <label class="field_name align_right">Body:</label>
                      <div class="field">
                      <!-- <textarea <?php /* if(!$makeReadOnly) {?>id="body"<?php }?> name="mail_body" style="width: 98%" class="<?php echo $mail_body ?>" cols="10" rows="20" <?php echo $makeReadOnly;?>><?php echo (isset($message) and $message->htmlBody !='') ? $message->htmlBody : ''/*/?></textarea> -->
                                     <textarea class="textarea" name="mail_body" style="width: 98%; height: 300px"></textarea>
               
                      <br><br>
                          <input class="btn blue" type="submit" value="Send" name="action_send"/>
                          <input class="btn blue" type="submit" value="Cancel" name="action_cancel"/>          
                      </div>
                    </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="div_export" tabindex="-1" role="dialog" aria-labelledby="memberquickadd" style="display:none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header well red">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Export contacts to iContact</h4>
      </div>
        <div class="modal-body">
        <div id="form_add_new" action="<?php echo site_url('admin/add_new_postcard')?>" method="post" class="form-horizontal">             
            <div id="div_order_added_id" class="hidden ">
                    <a class="close" data-dismiss="alert">x</a>
                    <span>Please correct red fields</span>
            </div>
            <div class="control-group">
                <label for="inputEmail" class="control-label">Existing list</label>
                <div class="controls">
                   <?php
                   $arrayLists = array();
                   if(is_array($vertical_list) and count($vertical_list) > 0){
                       foreach ($vertical_list as $object) {
                           $arrayLists[$object->listId] = $object->name;
                       }
                   }
                   
                   echo form_dropdown('list_name', $arrayLists, '', ' id="list_name" ') ?>
                </div>
           </div>                           
            <div class="control-group">
                <label for="inputEmail" class="control-label">New List name</label>
                <div class="controls">
                    <input type="text" placeholder="Name of the list" value="" id="new_list_name"/>
                    <a class="btn blue" id="add_new_list">Add</a>
                </div>
           </div>
           <div class="control-group"> 
                <label for="inputEmail" class="control-label">Contacts</label>
                <div class="controls">
                <?php echo form_dropdown('select_visitors', $select_visitors, null, ' id="select_visitors"')?>                
                </div>
           </div>                                                
        </div>
    </div>
    <div class="modal-footer">
         <a class="btn blue" id="btn_export_list">Export</a>
         <a class="btn btn-info" id="btn_close_id" onclick="closeFromCancel();">Cancel</a>
    </div>    
        </div>
    </div>
  </div>
</div>


<script>
function getMessages(camp_id) {
    
    if(camp_id !='') {
            $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/email/get_icontact_messages", 
                    { 
                      id: camp_id
                    },
                    function(data) {                            
                        $('#message_id').html(data);
                    }
                );    
    } else {
        $('#message_id').html('<option value="">~*~ Select ~*~</option>');
    }
}
function getMessage(message_id) {
    if(message_id != '')
        document.location.href = '<?php echo site_url('email/icontact')?>/'+message_id;
    
}
function closeFromCancel() {
   
    $('.modal-header .close').click();
}
</script>

 


                 
        

        
         
        