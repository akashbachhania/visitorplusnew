<?php
class Scheduler_model extends orm{    
    public function get_templates($event){
        $template = new Template();
        $template->set_event($event);
        $templates = $this->get_object(
            $template
        );
        return $templates;
    }
}
