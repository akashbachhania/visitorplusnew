<?php    
class Orm extends CI_Model{
    
    const BASE_CLASS = 'Record';
    
    private $domain_classes = array(
        'Letter',
        'Order',
        'Person',
        'Visitor',
        'Message',
        'Template',
        'Relations',
        'Company',
        'Visits',
        'Note',
    );
    
    const QUERY_UPDATE = '1';
    const QUERY_INSERT = '2';
    
    const REL_EQUAL = '=';
    const REL_LESS  = '<';
    const REL_GREAT = '>';
    const REL_LESS_E = '<=';
    const REL_GREAT_E = '>=';
    const REL_DIFF = '!=';
    
    const REL_BINDING_OR = ' OR ';
    const REL_BINDING_AND = ' AND ';
    
    public function __construct(){            
        parent::__construct();
        $this->cache = array();
        $this->cache_foreign = array();
    }

    public function get_person_names( $type = Person::TYPE_CLIENT, $company_id  ){        
        $filter = new Person();
        $filter->set_person_type( $type );
        $filter->set_company_id( $company_id );
        $filter->set_active(1);
        $filter->set_order_by( array('name'));
         
        $persons = $this->get_object( $filter );
        $return  = array();
        foreach( $persons as $person ){
            $return[ $person->id ] = $person->name;
        }
        return $return;
    }
    public function get_funds_member( $user_id  ){        
        $this->db->select('*');
        $query = $this->db->get_where('dr_funds_members',array('user_id'=>$user_id));
        return $query->result();
    }
 
    public function get_us_states(){
      $states = array(
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "District of Columbia",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin",
            "Wyoming"
          );
       $ret = array();
       foreach( $states as $state ){
           $ret[ $state ] = $state;
       }
       return $ret;          
    }

    public function get_languages($company_id){
        
        $lang = new Type(null, $company_id, 1);
        $langs = $this->get_object( $lang );
        
        $languages = array();
        foreach( $langs as $lang ){
            $languages[ $lang->id ] = $lang->description;
        }
        return $languages;
    }

    public function get_companies( Person $person ){
        $return = array();
        if( $person->person_type == Person::TYPE_ADMIN ){
            $filter = new Company();
            $companies = $this->get_object( $filter );
            foreach( $companies as $company ){
                $return[ $company->id ] = $company->name;
            }
        }
        return $return;
    }

    public function get_times(){
       return  array( 
          '7:00' => '7:00 AM',
          '7:30' => '7:30 AM',
          '8:00' => '8:00 AM',
          '8:30' => '8:30 AM',
          '9:00' => '9:00 AM',
          '9:30' => '9:30 AM',
          '10:00' => '10:00 AM',
          '10:30' => '10:30 AM',
          '11:00' => '11:00 AM',
          '11:30' => '11:30 AM',
          '12:00' => '12:00 PM',
          '12:30' => '12:30 PM',
          '13:00' => '1:00 PM',
          '13:30' => '1:30 PM',
          '14:00' => '2:00 PM',
          '14:30' => '2:30 PM',
          '15:00' => '3:00 PM',
          '15:30' => '3:30 PM',
          '16:00' => '4:00 PM',
          '16:30' => '4:30 PM',
          '17:00' => '5:00 PM',
          '17:30' => '5:30 PM',
          '18:00' => '6:00 PM',
          '18:30' => '6:30 PM',
          '19:00' => '7:00 PM',
          '19:30' => '7:30 PM',
          '20:00' => '8:00 PM'
      );        
    }

    public function get_duration_times(){
        for( $i=0.5;$i<24;$i+=0.5 ){
            $return["$i"] = "$i hours";
        }
        return $return;
    }
    public function get_estimated_ages(){
        $base_year = 1920;
        $year  = gmdate('Y');
        $years = array();
        for( $i=1;($i+$base_year)<$year;$i++){
            $years[] = $base_year + $i;
        }
        return $years;
    }   
    
    public function save_object( Record $record, Record $old_record = null ){
        if( $old_record ){
            if( !$record->equal( $old_record )){
                $type = self::QUERY_UPDATE;
            }
        } else {
            if( isset($record->id) && intval($record->id) && $old_record == null ){
                //throw new Order_model_exception('New person with id, withoud old person, sometnihg is  fishy!'. $record->__toString() );
                $type = self::QUERY_UPDATE; 
            } else {
                $type = self::QUERY_INSERT;
            }
        }
        // ini_set('display_errors', 1);
        // error_reporting(E_ALL ^ E_NOTICE);

        switch( $type ){
            case self::QUERY_INSERT:
            case self::QUERY_UPDATE:
                $query = $this->create_query( $record, $type);
                if(strpos($query, "0000-00-00 00:00:00")){
                    $query = str_replace('0000-00-00 00:00:00', date('Y-m-d H:i:s'), $query);
                }

                if($this->db->query( $query )){
                    $id    = isset( $record->id) && $record->id ? $record->id : $this->db->insert_id();
                } else {
                    throw new Order_model_exception('Database transaction failed');
                }
            break;
            default:
                $id = $record->id;
            break;
        }
        return $id;
    }

    public function delete_object( Record $object ){
        $filters = array();
        foreach( $object as $field=>$value ){
            if( $value !== null && !is_array($value) && !is_object($value) && $field != 'table'){
                $filters[] = "$field = '$value'";
            }
        }
        $filter = $filters ? 'WHERE ' . implode(' AND ', $filters) : '';
        if( $filter ){
            $query = "DELETE FROM $object->table $filter";
            $this->db->query( $query );
            return $this->db->affected_rows();                    
        }
    }

    private function create_query( Record $object ){
        $type = self::QUERY_INSERT;
        if( isset($object->id) && $object->id > 0 ){
            $type = self::QUERY_UPDATE;
        }
        
        if( in_array( get_class($object), array('Visitor','Assignment_log','Visits', 'Note'))){
            $object->set_change_time( date('Y-m-d H:i:s'));
        }
        
        switch( $type ){
            case self::QUERY_INSERT:
                foreach( $object as $column=>$value ){
                    
                    if( $value !== null && 
                        !is_object( $value ) && 
                        !is_array( $value ) && 
                        $column != 'id' && 
                        $column != 'table' &&
                        !preg_match('/^_/', $column )){
                             
                        $value = $this->db->escape($value);
                        $query[ $column ] ="$value";
                    }

                    //set default value to handle database error when inserting.
                    if($column == 'birth' &&  $value==="''"){
                        $tmpTime = date('Y-m-d');
                        $query[ $column ] = "'$tmpTime 00:00:00'";
                    }

                    if($column == 'married' &&  $value==="''"){
                        $query[ $column ] = 0;
                    }

                    if($column == 'bible_study' &&  $value==="''"){
                        $query[ $column ] = 0;
                    }

                    // if($column == 'datetime'){
                    //     $tmpTime =  date('Y-m-d H:i:s',strtotime($value));
                    //     $query[ $column ] ="'$tmpTime'";
                    // }
                    
                    if($column == 'text_contacts' && $value==="''"){
                        $query[ $column ] = '';
                    }

                    if($column == 'related_person_id' && $value==="''"){
                        $query[ $column ] = 0;
                    }
                }            
            break;
            case self::QUERY_UPDATE:
                foreach( $object as $column=>$value ){
                    
                    if( $value !== null &&
                        !is_object( $value ) &&
                        !is_array( $value ) &&
                        $column != 'table' && 
                        $column != 'id' &&
                        !preg_match('/^_/', $column )){
                            
                        $value = $this->db->escape($value); 
                        $query[] ="$column = $value";
                    }
                }            
            break;
        }
         
        switch( $type ){
            case self::QUERY_INSERT:
            
                $columns = implode( ',' , array_keys( $query ));
                $values  = implode( ',' , $query );
                $table   = $object->table;
                
                $query = "INSERT INTO `$object->table` ($columns) VALUES ( $values )";
            break;
            case self::QUERY_UPDATE:
                
                $columns_values  = implode(' , ', $query );
                $id      = $object->id;                    
                $query   = "UPDATE `$object->table` SET $columns_values WHERE id=$id";
                                
            break;
        }
        return $query;
    }

    /**
    * put your comment there...
    * 
    * @param Record $filter_object Object that is fetched
    * @param mixed $return_one  Return just one object or array
    * @param mixed $relations  field relations of this object
    * @param mixed $relation_binding  OR | AND on relations
    * @param mixed $related_objects Objects related to one being fetched
    */
    public function get_object( $filter_objects, $return_one = false, $relations = array(),$relation_binding = self::REL_BINDING_OR, $related_objects = null){
        /**
        * Collect query parameters from passed object(s)
        */
        if( is_object( $filter_objects )){
            $order_by = $filter_objects->order_by;
            $table   = $filter_objects->table;
            $class   = get_class( $filter_objects );
            $filter_objects = array($filter_objects);            
        } else if( is_array( $filter_objects )){
            $class   = get_class( $filter_objects['0'] );
            $table   = $filter_objects['0']->table;
            $order_by = $filter_objects['0']->order_by;
        }
        
        if( !$class ){
            throw new Exception('Expected <filter_objects> to be object or array of object');    
        }
            
        $binding_filters = array();
        foreach( $filter_objects as $index=>$object ){
            $filters = array();
            foreach( $object as $field=>$value ){
                if( $value !== null && !is_array($value) && !is_object($value) && $field != 'table' && !preg_match('/^__/', $field )){
                    
                    $relation  = '=';
                    if( isset($relations[$index][$field])){
                        $relation = $relations[$index][$field]; 
                    } else if (isset($relations[$field])){
                        $relation = $relations[$field];
                    }
                    /**
                    * Regex relation has different syntax 
                    */
                    if( preg_match('/REGEXP/',$relation)){
                        $relation = str_replace("<$field>", $value, $relation );
                        $filters[] = "$field $relation";    
                    } else {
                        $filters[] = "$field $relation '$value'";    
                    }
                    
                }
            }
            $binding_filters[] = $filters;            
        }

        /**
        * Construct query
        */
        $filter_array = array();
        foreach( $binding_filters as $filters ){
            if( $filters ){
                $filter_array[] = '('. implode(' AND ', $filters). ')';     
            }
        }

        $filter_string = implode( $relation_binding, $filter_array );
        
        $filter = $filter_string ? 'WHERE ' . $filter_string : '';
        
        $order_by_string = '';
        if( $order_by ){
            $order_by_string = 'ORDER BY '.implode(',' , $order_by);
        }

        $query  = "SELECT * FROM `$table` $filter $order_by_string";
        
        $results = $this->db->query( $query );

        /**
        * Process db results
        */
        $objects = array();
        $methods = get_class_methods( $class );
        foreach( $results->result() as $row_object ){
            $object = new $class;
            foreach( $row_object as $field=>$value ){
                /**
                * Set field
                */                
                $call = 'set_' . $field;               
                if( in_array( $call, $methods )){
                    $object->$call( $value );
                }
  
                if( $field && preg_match('/(?P<class>\w+)_id/',$field, $match)){
                    $foreign_class = null; 

                    if( class_exists( ucfirst($match['class']) ) ){
                        $foreign_class = ucfirst($match['class']);
                    } else {
                        foreach( $this->domain_classes as $domain_class ){
                            if( preg_match('/_'.$this->lcfirst($domain_class).'_/', $field )){
                                $foreign_class = $domain_class;
                            }
                        }
                    }
                       
                    if( $foreign_object = $this->get_foreign_object( $foreign_class, $value )){
                        
                        $foreign_object = is_array($foreign_object) ? $foreign_object: array($foreign_object);
                        $related_field = '_'.$this->lcfirst($foreign_class);

                        if( isset($object->$related_field)){  
                            $object->$related_field = array_merge($object->$related_field, $foreign_object);                          
                        } else {
                           $object->$related_field = $foreign_object;    
                        }
                                                                              
                    } 
                }                
            }
            /**
            * Load all objects that have foreign key that points to this object
            */
            if( $related_objects ){  
                if( is_scalar( $related_objects )){
                    $related_objects = array($related_objects );
                }
                foreach( $related_objects as $index=>$related_object ){
                    
                    $related_to_related = array();
                    if(is_array($related_object)){ //If we want to fetch object that are related to related object then index is the class or related, related object is array of relatives
                       $related_to_related = $related_object;
                       $related_object = $index; 
                    }  
                    
                    if( class_exists( $related_object )){
                        $related_field = '_'.strtolower($related_object);
                        $object->$related_field = $this->get_related_object( $related_object, $class, $object->id, $related_to_related );
                    } else {
                        throw new Exception('Related class doesnt exist:'. $related_object);
                    }
                }
            }
            /**
            * Return one/many
            */
            if( $return_one ){
                $objects = $object;
                break;
            } else {
                $objects[] = $object;    
            }
        }
        return $objects;
    }
    public function update_person_pwd($data,$id){
        
        $this->db->set('password',$data);
        $this->db->where('id',$id);
        $result = $this->db->update('dr_persons');
        if($result){
            return true;
        }
    }    
    public function get_us_date_format( $date ){
        if( preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $date, $matches ) ){
            $date = $matches['month'].'-'.$matches['day'].'-'.$matches['year'].' '.( isset($matches['rest']) ?' '.$matches['rest']: '' );
        }
        return $date;
    }
    /**
    * Get object that has id that points to $class with $foreign_id
    * 
    * @param mixed $related_class
    * @param mixed $class
    * @param mixed $foreign_id
    */
    public function get_related_object( $related_class, $class, $foreign_id, $related_to_related = array() ){
        $objects = null;   if($related_class == 'Type'){}
        $call    = null;
        /**
        * Check the cache
        */
        if( !isset( $this->cache_foreign[$related_class][$foreign_id])){
            
            $call    = $method  = 'set_' . strtolower( $class).'_id';
            $methods = get_class_methods($related_class);
            /**
            * Try to find simple sette i.e set_$class_id,
            * if it doesnt exist try to find method set_<something>_$class_id
            * If not found, repeat above for parent classes of $related_class (Maybe the setter was defined there)
            * If not found throw exception.
            */
            if( !in_array( $method, $methods )){
                $call = null;
                $compatible_calls = array();
                foreach( $methods as $method){
                    if( preg_match('/_'.$this->lcfirst($class).'_/', $method )){
                        $compatible_calls[] = $method;    
                    }
                }
                $call = $compatible_calls;
                
                if( !$call ){
                    $ancestors = $this->get_parent_classes( $class );
                    foreach( $ancestors as $ancestor ){
                         $method = 'set_'.strtolower( $ancestor ).'_id';
                         if( in_array( $method, $methods )){
                             $call = $method;
                             break;
                         }
                         if( !$call ){
                            foreach( $methods as $method){
                                if( preg_match('/_'.$this->lcfirst($class).'_/', $method )){
                                    $compatible_calls[] = $method;    
                                }
                            }
                            $call = $compatible_calls;                             
                         }
                    }                    
                }

                if( !$call ){
                    throw new Exception("Invalid call: $class-$call");     
                }
            }
            
            $call = is_array( $call ) ? $call : array($call);
            
            $objects = array();
            foreach( $call as $setter ){
                $filter  = new $related_class;
                $filter->$setter($foreign_id);
                $objects = $objects + $this->get_object( $filter, false, false, false, $related_to_related );
            }
             
            $this->cache[$related_class][$foreign_id] = $objects;           
        } else {
            $objects = $this->cache[$related_class][$foreign_id];
        }
        return $objects; 
    }

    public function get_parent_classes( $class ){
        $desc_object = new $class(); 
        $classes = array(); 
        while( ($parent = get_parent_class( $class)) != self::BASE_CLASS ){
            $parent_object = new $parent();
            if( $desc_object->table == $parent_object->table ){
                $classes[] = $parent;     
            }
            $class = $parent; 
        }
        return $classes;
    }
    /**
    * Get object of $class with id $id. If $id is array then it will try to get all objects with ids.
    * 
    * @param mixed $class
    * @param mixed $id  array or scalar
    */
    public function get_foreign_object( $class, $id ){
        $objects = array();
        if( $id ){
            if( !is_numeric( $id )){
                $ids = unserialize( $id );
            } else {
                $ids = (array) $id;    
            }
            foreach( $ids as $id ){
                if( !isset( $this->cache[$class][$id])){
                    $filter = new $class;
                    $filter->set_id($id);
                    $this->cache[$class][$id] = $this->get_object( $filter, true );
                }  
                $objects[] = $this->cache[$class][$id];
                            
            }            
        }
        if( count($objects)>1){
            return $objects;
        } else {
            return reset($objects);
        } 
    }

    private function lcfirst($str){ 
      $str[0] = strtolower($str[0]);
      return $str;         
    }
    /**
    * Shorthand function to get related object without additioal options
    * 
    * @param Record $object
    * @param mixed $related_objects
    */
    public function get_objects_with_related( Record $object, $related_objects ){
        return $this->get_object(
            $object, 
            true, 
            null,
            null,
            $related_objects
        );        
    }
    
    public function set_user( $user ){
        $this->user = $user;
    }
    
    public function system_log( $message,  $data = '', $status = System_log::TYPE_SUCCESS ){
        $log = new System_log();
        $log->set_data($data)
            ->set_message($message)
            ->set_status($status);
        $this->save_object($log);
    }
}


abstract class Record{
    public $id;
    public $company_id;
    /**
    * Used for validation.
    * Contains map fields and corresponding error messages
    * 
    * @var array
    */
    public $errors = array();
    /**
    * DB table this record refers to
    * 
    * @var string
    */
    public $table;
    /**
    * Used for validation. This record field need to be non null
    * 
    * @var array
    */
    protected $required = array();
    /**
    * Record validation function
    * 
    */
    public $active;
    
    public $order_by = array();
    
    public function __construct( $id = null, $company_id = null ){
        if( $id !== null ){
            $this->id = $id;
        }
        if( $company_id !== null ){
            $this->company_id = $company_id;
        }        
    }
    public function validate(){
        foreach( $this->required as $field ){
            if( $this->$field == null ){
                $this->errors[ $field ] = 'Must not be null';
            }
        }
        $valid = true;
        if( $this->errors ){
            $valid = false;
        }
        return $valid;
    }
    public function set_active( $active ){
        $this->active = $active;
        return $this;
    }    
    public function set_order_by( array $columns ){  
        $this->order_by = (array)$columns;
    }
    /**
    * Record comparison function
    * 
    * @param Record $record
    */
    public abstract function equal( Record $record );
    public function get_us_date( $date ){
        if( preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $date, $matches ) ){
            $date = $matches['month'].'-'.$matches['day'].'-'.$matches['year'].' '.( isset($matches['rest']) ?' '.$matches['rest']: '' );
        }
        return $date;        
    }
    public function get_eu_date( $date ){
        if(preg_match('/(\d\d)-(\d\d)-(\d\d\d\d)/', $date, $matches )){
            $date = $matches['3'].'-'.$matches['1'].'-'.$matches['2'];
        }
        return $date;  
    }
    public function __set( $var, $value ){
        $this->$var = $value;
    }
    public function set_company_id( $company_id ){
        $this->company_id = $company_id;
    }
}
 
class Person extends Record{
    
    public $table = 'dr_persons';
    
    const TYPE_USER      = 3;
    const TYPE_ADMIN     = 2;
    const TYPE_VISITOR   = 1;
    const TYPE_MEMBER   = 4;
 
    public $name;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $phone_1;
    public $phone_2;
    public $email;
    public $fax;
    public $datetime;
    public $password;
    public $id;
    public $company_id;
    public $person_type;
    public $text_contacts;
    
    public $vr_username;
    public $vr_password;
    public $send_emails;
    public $default_list;
       
    public $active;
    public $company;
    

                
    public $required = array(
        'name',
        'person_type',
        'company_id'
    );
    
    public function __construct( $id = null, $company_id = null, $type = null ){
        if( $type ){
            $this->person_type = $type;
        }
        parent::__construct( $id, $company_id );
    }
      
    public function set_id( $id ){
        $this->id = $id;
        return $this;       
    }
    public function set_name( $name ){
        $this->name = $name;
        return $this;
    }
    public function set_city( $city ){
        $this->city = $city;
        return $this;
    }
    public function set_birth( $birth ){
        $this->birth = $this->get_eu_date($birth);
        return $this;
    }    

    public function set_text_contacts( $text_contacts ){
        $this->text_contacts = $text_contacts;
        return $this;
    }    
    public function set_address( $address ){
        $this->address = $address;
        return $this;
    }
    public function set_vr_username( $zip ){
        $this->vr_username = $zip;
        return $this;
    }
    public function set_vr_password( $zip ){
        $this->vr_password = $zip;
        return $this;
    }         
    public function set_zip( $zip ){
        $this->zip = $zip;
        return $this;
    }
    public function set_state( $state ){
        $this->state = $state;
        return $this;
    }        
    public function set_phone_1( $phone1 ){
        if( $phone1 ){
            if( $this->check_phone( $phone1 )){
                $this->phone_1 = $phone1;    
            } else {
                $this->errors['phone_1'] = 'Invalid phone format';
            }            
        }
        return $this;
    }
    public function set_phone_2(  $phone2 ){
        if( $phone2 ){
            if( $this->check_phone( $phone2 )){
                $this->phone_2 = $phone2;    
            } else {
                $this->errors['phone_2'] = 'Invalid phone format';
            }            
        }
        return $this;
    }    
    public function set_email( $email ){
        if( $email ){
            if( preg_match("/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email ) ){
                $this->email = $email;    
            } else {
                $this->errors[ 'email'] = 'Invalid email format';
            }            
        }
        return $this;
    }
    public function set_fax( $fax ){
        if( $fax ){
            if( $this->check_phone( $fax )){
                $this->fax = $fax;    
            } else {
                $this->errors['fax'] = 'Invalid phone format';
            }        
            $this->fax   = $fax;            
        }
        return $this;
    }
    public function set_person_type( $type ){
        $this->person_type = $type;
        return $this;
    }
    public function set_datetime( $datetime ){
        $this->datetime = $datetime;
        return $this;
    }
    public function set_company_id( $company_id ){
        $this->company_id = $company_id;
        return $this;
    }
    public function set_password( $password ){
        $this->password = $password;
        return $this;
    }
    public function equal( Record $person ){
        return false;
    }
    public function set_active( $active ){
        $this->active = $active;
        return $this;
    }
    public function set_company( $active ){
        $this->company = $active;
        return $this;
    }
    public function set_timeoff_time_start($date){
        $this->timeoff_time_start = $date;    
    }
    public function set_timeoff_time_end($date){
        $this->timeoff_time_end = $date;
    }
   
    public function set_gender( $gender ){
        $this->gender = $gender;
        return $this;
    }
    public function set_send_emails( $send_emails ){
        $this->send_emails = $send_emails;
        return $this;
    }
    public function set_default_list( $default_list ){
        $this->default_list = $default_list;
        return $this;
    }
   
    public function __toString(){
        return '';
    }

    protected function check_phone( $phone ){
        $valid = false;
        if( preg_match('/\d+(-|\s+\d+)*/', $phone )){
            $valid = true;
        }
        return $valid;
    }

    public function validate(){
        foreach( $this->required as $field ){
            if( $this->$field == null ){
                $this->errors[ $field ] = 'Must not be null';
            }
        }
        $valid = true;
        if( $this->errors ){
            $valid = false;
        }
        return $valid;
    }
    
    public function set_change_time($time){
        $this->change_time = $time;
    }
}
class Visitor extends Person{
    const NULL_AGE = '0000-00-00 00:00:00';
    const GENDER_MALE   = 'male';
    const GENDER_FEMALE = 'female';
    
    const MARRIED_YES = '1';
    const MARRIED_SINGLE = '0';

    const STUDY_YES = '1';
    const STUDY_NO  = '0';
    
    const LANG_ENGLISH  = 'english';
    const LANG_SPANISH  = 'spanish';
                      
    const REFERRED_BY_NEWSPAPER = 'news';
    const REFERRED_BY_WALKIN = 'walkin';
    const REFERRED_BY_FRIEND = 'friend';
    const REFERRED_BY_OTHER  = 'other';
    const REFERRED_BY_MEMBER = 'member';
    
    
    public $objects = array();
    
    public $gender;
    public $birth;
    public $spouse;
    public $first_name;
    public $last_name;
    public $referer;
  
    
    public $married;
    public $refered_by;
    public $notes;
    public $bible_study;
    public $primary_language;
    public $registered_date;
    public $head_household;
    public $profile_image;


    public $required = array(
        'first_name',
        'last_name',
    );
    
    public function __construct($id=null, $company_id = null){
        $this->person_type = Person::TYPE_VISITOR;
        $this->cache = array();
        parent::__construct( $id, $company_id );
    }
    public function add_object( $relation ){
        if( !is_array( $relation )){
            $relations = array($relation);
        } else {
            $relations = $relation;
        } 
        foreach( $relations as $relation ){
            $this->objects[] = $relation;
            $this->cache[ get_class( $relation )][] = $relation;     
        }
        
        foreach( $this->cache as $class=>$objects ){
            $field = '_' . strtolower($class);
            if( !isset($this->$field) ){
                $this->$field = $objects;
            } else {
                $this->$field = array_merge($this->$field, $objects);
            }
        }
        $this->cache = array();
        
        return $this;
    }
    public function set_gender( $value ){
        $this->gender = $value;
        return $this;
    }
    public function set_referer( $value ){
        $this->referer = $value;
        return $this;
    }    
    public function set_first_name( $first_name ){
        $this->first_name = $first_name;
        return $this;
    }
    public function set_last_name( $last_name ){
        $this->last_name = $last_name;
        return $this;
    }               
    public function set_birth( $value ){
        $this->birth = $this->get_eu_date($value);
        $this->__age = $this->get_age();
        return $this;
    }
    public function set_spouse( $value ){
        $this->spouse = $value;
        return $this;
    }
    public function set_refered_by( $value ){
        $this->refered_by = $value;
        return $this;
    }    
    public function set_married( $value ){
        $this->married = $value;
        return $this;
    }
    public function set_notes( $value ){
        $this->notes = $value;
        return $this;
    }
    public function set_primary_language( $language ){
        $this->primary_language = $language;
        return $this;
    }
    public function set_bible_study( $var ){
        $this->bible_study = $var;
        return $this;
    }
    public function is_bday_today(){
        if( substr($this->birth,5,5) == gmdate('m-d')){
            return true;
        }
        return false;
    }
    private function get_age(){
        $age = '';
        if( $this->birth != self::NULL_AGE && preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $this->birth, $matches ) ){
            $age = (int)gmdate('Y') - (int)$matches['year'];
        }        
        return $age;
    }
    public function set_change_time($time){
        $this->change_time = $time;
    } 
    public function set_registered_date($registered_date){
        $this->registered_date = $registered_date;
    } 
    public function set_head_household( $head_household ){
        $this->head_household = $head_household;
        return $this;
    }
    public function set_profile_image( $profile_image ){
        $this->profile_image = $profile_image;
        return $this;
    }
}


class Member extends Person{
    const NULL_AGE = '0000-00-00 00:00:00';
    const GENDER_MALE   = 'male';
    const GENDER_FEMALE = 'female';
    
    const MARRIED_YES = '1';
    const MARRIED_SINGLE = '0';

    const STUDY_YES = '1';
    const STUDY_NO  = '0';
    
    const LANG_ENGLISH  = 'english';
    const LANG_SPANISH  = 'spanish';
                      
    const REFERRED_BY_NEWSPAPER = 'news';
    const REFERRED_BY_WALKIN = 'walkin';
    const REFERRED_BY_FRIEND = 'friend';
    const REFERRED_BY_OTHER  = 'other';
    const REFERRED_BY_MEMBER = 'member';
    
    
    public $objects = array();
    
    public $gender;
    public $birth;
    public $spouse;
    public $first_name;
    public $last_name;
    public $referer;
  
    
    public $married;
    public $refered_by;
    public $notes;
    public $bible_study;
    public $primary_language;
    public $registered_date;
    public $head_household;
    public $profile_image;


    public $required = array(
        'first_name',
        'last_name',
    );
    
    public function __construct($id=null, $company_id = null){
        $this->person_type = Person::TYPE_MEMBER;
        $this->cache = array();
        parent::__construct( $id, $company_id );
    }
    public function add_object( $relation ){
        if( !is_array( $relation )){
            $relations = array($relation);
        } else {
            $relations = $relation;
        } 
        foreach( $relations as $relation ){
            $this->objects[] = $relation;
            $this->cache[ get_class( $relation )][] = $relation;     
        }
        
        foreach( $this->cache as $class=>$objects ){
            $field = '_' . strtolower($class);
            if( !isset($this->$field) ){
                $this->$field = $objects;
            } else {
                $this->$field = array_merge($this->$field, $objects);
            }
        }
        $this->cache = array();
        
        return $this;
    }
    public function set_gender( $value ){
        $this->gender = $value;
        return $this;
    }
    public function set_referer( $value ){
        $this->referer = $value;
        return $this;
    }    
    public function set_first_name( $first_name ){
        $this->first_name = $first_name;
        return $this;
    }
    public function set_last_name( $last_name ){
        $this->last_name = $last_name;
        return $this;
    }               
    public function set_birth( $value ){
        $this->birth = $this->get_eu_date($value);
        $this->__age = $this->get_age();
        return $this;
    }
    public function set_spouse( $value ){
        $this->spouse = $value;
        return $this;
    }
    public function set_refered_by( $value ){
        $this->refered_by = $value;
        return $this;
    }    
    public function set_married( $value ){
        $this->married = $value;
        return $this;
    }
    public function set_notes( $value ){
        $this->notes = $value;
        return $this;
    }
    public function set_primary_language( $language ){
        $this->primary_language = $language;
        return $this;
    }
    public function set_bible_study( $var ){
        $this->bible_study = $var;
        return $this;
    }
    public function is_bday_today(){
        if( substr($this->birth,5,5) == gmdate('m-d')){
            return true;
        }
        return false;
    }
    private function get_age(){
        $age = '';
        if( $this->birth != self::NULL_AGE && preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $this->birth, $matches ) ){
            $age = (int)gmdate('Y') - (int)$matches['year'];
        }        
        return $age;
    }
    public function set_change_time($time){
        $this->change_time = $time;
    } 
    public function set_registered_date($registered_date){
        $this->registered_date = $registered_date;
    } 
    public function set_head_household( $head_household ){
        $this->head_household = $head_household;
        return $this;
    }
    public function set_profile_image( $profile_image ){
        $this->profile_image = $profile_image;
        return $this;
    }
}
     

class Company extends Record{
    public $table = 'dr_companies';
    
    public $name;

    public $address;
    public $city;
    public $state;
    public $zip;
    public $phone_1;
    public $phone_2;
    public $email;
    public $fax;
    public $active;   
    public $datetime;
    public $id;
    public $primary_contact;
    public $stripe_id;
    public $expire_company;
    public $subscription_id;



    public $required = array(
        'name',
        'city',
        'address',
        'state',
        'phone_1',
        'email',
    );
    
    public function __construct( ){
  
    }
    public function __set( $var, $value ){
        throw new Order_model_exception('Use setter!');
    }
    public function set_id( $id ){
        $this->id = $id;
        return $this;       
    }
    public function set_name( $name ){
        $this->name = $name;
        return $this;
    }        
    public function set_city( $city ){
        $this->city = $city;
        return $this;
    }
    public function set_address( $address ){
        $this->address = $address;
        return $this;
    }
    public function set_zip( $zip ){
        $this->zip = $zip;
        return $this;
    }
    public function set_state( $state ){
        $this->state = $state;
        return $this;
    }        
    public function set_phone_1( $phone1 ){
        $this->phone_1 = $phone1;
        return $this;
    }
    public function set_phone_2(  $phone2 ){
        $this->phone_2 = $phone2;
        return $this;
    }    
    public function set_email( $email ){
        $this->email = $email;
        return $this;
    }
    public function set_fax( $fax ){
        $this->fax   = $fax;
        return $this;
    }
    public function set_datetime( $datetime ){
        $this->datetime = $this->get_eu_date($datetime);
        return $this;
    }
    public function set_active( $active ){
        $this->active = $active;
        return $this;
    }
    public function set_primary_contact( $primary_contact ){
        $this->primary_contact = $primary_contact;
        return $this;
    }
    public function set_stripe_id( $stripe_id ){
        $this->stripe_id = $stripe_id;
        return $this;
    }
    public function set_expire_company( $expire_company ){
        $this->expire_company = $expire_company;
        return $this;
    }
    public function set_subscription_id( $subscription_id ){
        $this->subscription_id = $subscription_id;
        return $this;
    }
   
    public function equal( Record $person ){
        return false;
    }
    public function __toString(){
        return '';
    }    
}
class Visits extends Record{
    public $table = 'dr_visits';
    public $visitor_id;
    public $company_id;
    public $datetime;
    public $change_time;
    public $is_member;

    public function __construct($id = null, $company_id = null,$is_member = false){
        if( $id !== null ){
            $this->id = $id;
        }
        if( $company_id !== null ){
            $this->company_id = $company_id;
        }  
       // $this->is_member = $is_member;
    }
    public function set_visitor_id( $person_id ){
        $this->visitor_id = $person_id;
        return $this;
    }
    public function set_company_id( $company_id ){
        $this->company_id = $company_id;
        return $this;
    }    
    public function set_datetime( $datetime ){
        $this->datetime = $this->get_eu_date( str_replace('/','-',$datetime));
        if( preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $this->datetime, $matches ) ){
            $this->__visit_year = $matches['year'];
            $this->__visit_month = $matches['month'];
            $this->__visit_day = $matches['day'];
        } else {
            $this->errors['datetime'] = 'Invalid date format';
        }        
        return $this;
    }
    public function equal(Record $record){
        return true;
    }
    public function set_change_time($time){
        $this->change_time = $time;
    }
    public function set_is_member($is_member){
        $this->is_member = $is_member;
    }
    
}
class Relations extends Record{
    
    const RELATION_CHILD   = 'child';
    const RELATION_SPOUSE  = 'spouse';
    const RELATION_REFERER = 'referer';
    
    public $table = 'dr_relations';
    public $person_id;
    public $related_person_id;
    public $relation_type;
    public $related_person_name;
    
    public function set_person_id( $person_id ){
        $this->person_id = $person_id;
        return $this;
    }
    public function set_related_person_id( $related_person_id ){
        $this->related_person_id = $related_person_id;
        return $this;
    }    
    public function set_relation_type( $relation_type ){
        $this->relation_type = $relation_type;
        return $this;
    }
    public function set_related_person_name( $person_name ){
        $this->related_person_name = $person_name;
        return $this;
    }
    public function equal(Record $record){
        return true;
    }        
}
class Template extends Record{
    
    const EVENT_BIRTHDAY = 'bday';
    const EVENT_CREATION = 'creation';
    
    
    public $table = 'dr_templates';
    public $mail_from;
    public $mail_to;
    public $mail_subject;
    public $mail_body;
    public $company_id;
    public $event;
    public $_recipients_full = array();
    public $required = array(
        'mail_from',
        'mail_to',
        'mail_subject',
        'mail_body'
    );
    
    
    public function set_id( $id ){
        $this->id = $id;
    }
    public function set_company_id( $id ){
        $this->company_id = $id;
    }
    public function set_mail_from( $var ){
        if(!$array = @unserialize($var)){
            $this->mail_from = serialize( $this->get_addresses($var));    
        } else {
            $this->mail_from = $var;     
        }
        
    }
    public function set_mail_to( $var ){
        if(!$array = @unserialize($var)){
            if( is_array($var)){
                $elements = array();
                foreach( $var as $element){
                    if($element){
                        $elements[] = $element;
                    }
                }            
                $var = $elements;
            }
            $this->mail_to = serialize( $var );    
        } else {
            $this->mail_to = $var;     
        }
    }
    public function set_mail_body( $var ){
        $this->mail_body = $var;
    }
    public function set_mail_subject( $var ){
        $this->mail_subject = $var;
    }
    public function get_mail_from(){  
        return implode(', ', ($array = unserialize( $this->mail_from )) ? $array : array());
    }
    public function get_mail_to(){
        return implode(', ', ($array = unserialize( $this->mail_to )) ? $array : array());
    }
    public function is_recipient( $id ) {
        if( !$this->_recipients){
            $this->get_mail_to_recipients();
        }
        return in_array($id,$this->_recipients['visitors']) ? true : false;
    }
    public function get_anonymous_recipients() {
        if( !isset($this->_recipients)){
            $this->get_mail_to_recipients();
        }
        $recipients = array();
        foreach( $this->_recipients['anonymous'] as $mail ){
            $recipient = new Visitor();
            $recipient->set_email($mail);
            $recipients[] = $recipient;
        }
        return $recipients;
    }
    public function get_anonymous_recipients_string() {
        if( !isset($this->_recipients)){
            $this->get_mail_to_recipients();
        }
        return implode(',', $this->_recipients['anonymous']);
    }    
    public function get_visitor_recipients() {
        if( !isset($this->_recipients)){
            $this->get_mail_to_recipients();
        }
        return $this->_recipients['visitors'];
    }        
    private function get_mail_to_recipients(){
        $array = ($array = unserialize( $this->mail_to )) ? $array : array();
        $this->_recipients = array(
            'visitors' => array(),
            'anonymous' => array()    
        );
        $temp = array();
        foreach( $array as $value ){
            if( is_numeric($value)){
                $this->_recipients['visitors'][]=$value;
            } else {
                $temp[]=$value;
            }
        }
        foreach( $temp as $temp_address){
            $mails = preg_split('!,?\s+,?!',$temp_address);
            foreach( $mails as $mail){
                if( strlen($mail) >= 3){
                    $this->_recipients['anonymous'][]=$mail;    
                }
            }            
        }     
    }
    public function add_recipients( $recipients ){
        $this->_recipients_full = array_merge($recipients, $this->_recipients_full);
    }
    public function get_recipients( $recipients ){
        return isset($this->_recipients_full) ? $this->_recipients_full: array();
    }    
    private function get_addresses( $string ){
        $emails = array();
        if( preg_match_all('!<?(?P<email>.+@.+)>?!', $string, $matches )){
            $emails = $matches['email'];
        }
        return $emails;
    }
    public function set_event($event){
        $this->event = $event;
    }
    public function equal( Record $item ){
        return $true;
    }
    public function validate(){  
        foreach( $this->required as $field ){
            if( $this->$field == null ){
                $this->errors[ $field ] = 'Must not be null';
            } else {
                switch($field){
                    case 'mail_from':
                    case 'mail_to':
                        if(!unserialize($this->$field)){
                            $this->errors[$field] = 'Addresses cant be null';
                        }
                    break;
                    case 'mail_body':
                        if( $this->$field == '<br>'){
                            $this->errors[$field] = 'Body cant be null';    
                        }
                    break;
                }
            }
        }
        $valid = true;
        if( $this->errors ){
            $valid = false;
        }
        return $valid;
    }        
}

class TemplateLog extends Record{
    public $table = 'dr_template_logs';
    public $id;
    public $template_id;
    public $datetime;
    
    public function __construct( $id = null, $template_id = null){
        $this->id = $id;
        $this->template_id = $template_id;
    }
    
    public function set_id( $id ){
        $this->id = $id;
    }
    public function set_template_id( $id ){
        $this->template_id = $id;
    }
    public function equal( Record $item ){
        return $true;
    }
    public function validate(){  
        return true;
    }        
}

class Message extends Record{
    
    const TYPE_MSG = 1;
    const TYPE_TODO = 2;
    
    public $table    = 'dr_messages';
    public $required = array(
    
    );
    
    public $id;
    public $type;
    public $from_user_id;
    public $to_user_id;
    public $subject;
    public $message;
    public $datetime;
    
    public function set_id( $id ){
        $this->id = $id;
    }
    public function set_from_user_id( $id ){
        $this->from_user_id = $id;
    }
    public function set_to_user_id( $id ){
        $this->to_user_id = $id;
    }
    public function set_subject( $subject ){
        
    }
    public function set_type( $type ){  
        $this->type = $type;
    }       
    public function set_message( $message ){
        $this->message = $message;
        $this->subject = substr( $message, 0 ,20 ); 
    }
    public function set_datetime( $datetime ){
        $this->datetime = $datetime;
    }
    public function equal( Record $item ){
        return $true;
    }
}
class Letter extends Record{
    public $table    = 'dr_letters';
    
    const TYPE_FRONT = 1;
    const TYPE_BACK  = 2;
    
    public $id;
    public $path_full;
    public $path_thumb;
    public $company_id;
    public $description;
    public $price;
    public $active;
    public $__order_type;
    
    public $type_back_side_postcard;
    public $type_front_side_postcard;
    public $type_back_side_letter;
    public $type_front_side_letter;
    
    public function set_id( $var ){
        $this->id = $var;
    }
    public function set_active( $var ){
        $this->active = (bool)$var;
    }
    public function set_path_full( $var ){
        $this->path_full = $var;
    }
    public function set_path_thumb( $var ){
        $this->path_thumb = $var;
    }
    public function set_type( $var, $order_type = null){
        $order_type = $order_type ? $order_type : $this->__order_type;
        switch( $var ){
            case self::TYPE_BACK:
                switch( $order_type ){
                    case Order::ORDER_LETTER:
                        $this->set_type_back_side_letter(true);
                    break;
                    case Order::ORDER_POSTCARD:
                        $this->set_type_back_side_postcard(true);
                    break;
                }
            break;
            case self::TYPE_FRONT:
                switch( $order_type ){
                    case Order::ORDER_LETTER:
                        $this->set_type_front_side_letter(true);
                    break;
                    case Order::ORDER_POSTCARD:
                        $this->set_type_front_side_postcard(true);
                    break;
                }
            break;
        }
    } 
    public function set_type_back_side_postcard( $var ){
        $this->type_back_side_postcard = $var ? 1 : 0;
    }
    public function set_type_front_side_postcard( $var ){
        $this->type_front_side_postcard = $var ? 1 : 0;
    }
    public function set_type_back_side_letter( $var ){
        $this->type_back_side_letter = $var ? 1 : 0;
    }
    public function set_type_front_side_letter( $var ){
        $this->type_front_side_letter = $var ? 1 : 0;
    }                    
    public function set_order_type( $var ){
        if( !in_array( $var, array(Order::ORDER_LETTER,Order::ORDER_POSTCARD)) ){
            $this->errors['type'] = 'Invalid type value';
        } else {
            $this->__order_type = $var;    
        }
    }    
    public function set_price( $price ){
        if( !is_numeric( $price )){
            $this->errors['price'] = 'Price must be numeric value';
        } else {
            $this->price = $price;
            if( isset($this->errors['price'])){
                unset($this->errors['price']);
            }
        }
        
    }
    public function set_description( $var ){
        if( !$var ){
            $this->errors['description'] = 'Invalid description';
        } else {
            $this->description = $var;    
        }
    }
    public function equal( Record $item ){
        return $true;
    }    
}
class Order extends Record{
    
    const STATUS_PENDING = '1';
    const STATUS_SENT = '2';
    
    const ORDER_LETTER = 'letter';
    const ORDER_POSTCARD = 'postcard';
    
    public $table    = 'dr_order'; 
    public $company_id;
    public $front_letter_id;
    public $back_letter_id;
    public $visitor_ids;
    public $text;
    public $total;
    public $timestamp;
    public $timestamp_sent;
    public $id;
    public $type;
    //public $transction_id;
    //public $auth_code;
    public $status;
    public $_letter = array();
    public $_visitor = array();
    
    public function __construct( $id=null, $company_id=null, $type=null){
        $this->id = $id;
        $this->company_id = $company_id;
        $this->type = $type;
    }
    public function set_id( $id ){
        $this->id = $id;
    }
    public function set_transaction_id( $id ){
        //$this->transaction_id = $id;
    }
    public function set_auth_code( $id ){
        //$this->auth_code = $id;
    }
    public function set_front_letter_id( $id ){
        $this->remove_letter($this->front_letter_id);
        $this->front_letter_id = $id;
    }
    public function set_back_letter_id( $id ){
        $this->remove_letter($this->back_letter_id);
        $this->back_letter_id = $id;
    }    
    public function set_visitor_ids( $var ){
        $this->visitor_ids = $var;
    }
    public function set_text( $text ){
        $this->text = $text;
    }
    public function set_timestamp( $timestamp ){
        $this->timestamp = $timestamp;
    }
    public function set_timestamp_sent( $timestamp ){
        $this->timestamp_sent = $timestamp;
    }    
    public function set_total( $total ){
        $this->total = $total;
    }
    public function set_status( $status ){
        $this->status = $status;
    }
    public function set_type($type ){
        $this->type = $type;
    }
    public function add_letter( Letter $letter ){
        $this->_letter[] = $letter;
    }
    public function add_visitor( Visitor $visitor ){
        $this->_visitor[] = $visitor;
    }    
    public function equal( Record $item ){
        return $true;
    }
    private function remove_letter( $id ){
        if( $id ){
            foreach( $this->_letter as $index=>$letter){
                if($letter->id == $id){
                    unset( $this->_letter[$index]);
                }
            }        
        }
    }
    public function get_total(){

        $count = count(unserialize($this->visitor_ids));
        $total = 0;
        foreach( $this->_letter as $letter){
            $total += $count*$letter->price;
        }
        return $total;  
    }
}
class Transaction extends Record{
    
    public $table = 'dr_transactions';
    public $order_id;
    public $amount;
    public $request;
    public $datetime;
    public $response;
    public $status;
    public $message;
    public $id;
    
    public function set_id( $id ){
        $this->id = $id;
    }    
    public function set_order_id($order_id){
        $this->order_id = $order_id;
        return $this;
    }
    public function set_amount($amount ){
        $this->amount = $amount;
        return $this;
    }
    public function set_request($request){
        $this->request = $request;
        return $this;
    }
    public function set_response($response){
        $this->response = $response;
        return $this;
    }
    public function set_datetime( $datetime ){
        $this->datetime = $datetime;
        return $this;
    }
    public function set_status($status){
        $this->status = $status;
        return $this;
    }
    public function set_message($status){
        $this->message = $status;
        return $this;
    }    
    public function equal( Record $record ){
    
    } 
}
class Type extends Record{
    
    const TYPE_LANGUAGE           = '1';
    const TYPE_ASSIGNMENT         = '2';
    const TYPE_REFERER            = '3';
    const TYPE_DONE_BY            = '4';
    const TYPE_MINISTRY           = '5';
    const TYPE_FUNDS              = '6';
    
    public $table = 'dr_types';
    public $type;
    public $name;
    public $description;
    public $company_id;
    public $id;
    public $active;
    public $required = array(
        'name',
        'company_id',
        'type'
    );


    public function __construct( $id = null, $company_id = null, $type = null){
        $this->id = $id;
        $this->company_id = $company_id;
        $this->type = $type;
    }
    public function set_type( $type ){
        $this->type = $type;    
    }
    public function set_name( $name ){
        $this->name = $name; 
    }
    public function set_company_id( $id ){
        $this->company_id = $id; 
    }
    public function set_id( $id ){
        $this->id = $id; 
    }         
    public function set_description( $description ){
        $this->description = $description; 
    }
    public function set_active( $active ){
        $this->active = $active;
        return $this;
    }    
    public function validate(){
        foreach( $this->required as $field ){
            if( $this->$field == null ){
                $this->errors[ $field ] = 'Must not be null';
            }
        }
        $valid = true;
        if( $this->errors ){
            $valid = false;
        }
        return $valid;
    }
    public function equal( Record $record ){
    
    }    
}

class Ministry extends Record {
    
    public $table = 'dr_ministry_roles';
    public $ministry_id;
    public $name;
    public $company_id;
    public $id;
    public $active;
    
    public function __construct( $id = null, $company_id = null,$ministry_id = null){
        $this->id = $id;
        $this->company_id = $company_id;
        $this->ministry_id = $ministry_id;
    }
    public function set_ministry_id( $ministry_id ){
        $this->ministry_id = $ministry_id;   
        return $this;
    }
    public function set_name( $name ){
        $this->name = $name; 
        return $this;
    }
    public function set_company_id( $id ){
        $this->company_id = $id; 
        return $this;
    }
    public function set_id( $id ){
        $this->id = $id; 
        return $this;
    }  
    public function set_active( $active ){
        $this->active = $active;
        return $this;
    }    
    public function validate(){
        return true;
    }
    public function equal( Record $record ){
    
    }    
}

class Note extends Record{
    public $table = 'dr_notes';
    public $visitor_id;
    public $note;
    public $change_time;
    public $person_id;
    
    public function set_id( $id ){
        $this->id = $id;
        return $this; 
    }
        
    public function set_visitor_id( $name ){
        $this->visitor_id = $name;
        return $this; 
    }                            
    public function set_note( $name ){
        $this->note = $name;
        return $this; 
    }
    public function set_change_time($time){
        $this->change_time = $time;
        return $this;         
    }
    public function set_person_id($id){
        $this->person_id = $id;
        return $this;         
    }
    public function validate(){
        return true;
    }
    public function equal( Record $record ){
    
    }                   
    
}
class Assignment_log extends Record{
    const TYPE_LANGUAGE           = '1';
    const TYPE_ASSIGNMENT         = '2';
    
    public $table = 'dr_assignments_log';
    public $type_id;
    public $datetime;
    public $visitor_id;
    public $doneby_person_id;
    public $company_id;
    public $id;
    public $active;
    public $change_time;
    
    public function set_type_id( $type ){
        $this->type_id = $type;
        return $this;    
    }
    public function set_visitor_id( $name ){
        $this->visitor_id = $name;
        return $this; 
    }
    public function set_company_id( $id ){
        $this->company_id = $id;
        return $this; 
    }
    public function set_id( $id ){
        $this->id = $id;
        return $this; 
    }
    public function set_datetime( $id ){
        $this->datetime = $id;
        return $this; 
    }               
    public function set_doneby_person_id( $id ){
        $this->doneby_person_id = $id;
        return $this; 
    }
    public function set_active( $active ){
        $this->active = $active;
        return $this;
    }    
    public function validate(){
        return true;
    }
    public function equal( Record $record ){
        $equal = false;
        if( $record->visitor_id == $this->visitor_id &&
            $record->company_id == $this->company_id &&
            $record->type_id    == $this->type_id        
        ) {
            $equal = true;
        }
        return $equal;        
    }    
    public function set_change_time($time){
        $this->change_time = $time;
    }    
}


class Ministry_log extends Record{
    const TYPE_LANGUAGE           = '1';
    const TYPE_MINISTRY           = '5';
    
    public $table = 'dr_ministries_log';
    public $type_id;
    public $visitor_id;   
    public $company_id;
    public $id;
    public $change_time;
    
    public function set_type_id( $type ){
        $this->type_id = $type;
        return $this;    
    }
    public function set_visitor_id( $name ){
        $this->visitor_id = $name;
        return $this; 
    }
    public function set_company_id( $id ){
        $this->company_id = $id;
        return $this; 
    }
    public function set_id( $id ){
        $this->id = $id;
        return $this; 
    }  
    public function validate(){
        return true;
    }
    public function equal( Record $record ){
        $equal = false;
        if( $record->visitor_id == $this->visitor_id &&
            $record->company_id == $this->company_id &&
            $record->type_id    == $this->type_id        
        ) {
            $equal = true;
        }
        return $equal;        
    }    
    public function set_change_time($time){
        $this->change_time = $time;
    }    
}

class System_log extends Record{
    const TYPE_SUCCESS      = 'SUCCESS';
    const TYPE_FAIL         = 'FAIL';
    
    public $table = 'dr_logs';
    public $id;
    public $message;
    public $data;
    public $status;

    
    public function set_message( $type ){
        $this->message = $type;
        return $this;    
    }
    public function set_data( $name ){
        $this->data = $name;
        return $this; 
    }
    public function set_status( $name ){
        $this->status = $name;
        return $this; 
    }        
    public function validate(){
        return true;
    }
    public function equal( Record $record ){
    
    }    
}

class Scheduleing extends Record{
    
    public $table = 'dr_schedules';
    public $id;
    public $company_id;
    public $date_schedule;
    public $relation;
    public $ministry_id;
    public $added_date;

    public function __construct( $id = null, $company_id = null,$ministry_id = null){
        $this->id = $id;
        $this->company_id = $company_id;
        $this->ministry_id = $ministry_id;
    }
    public function set_id( $id ){
        $this->id = $id;
        return $this; 
    }
    public function set_company_id( $company_id ){
        $this->company_id = $company_id;
        return $this; 
    }
    public function set_date_schedule( $date_schedule ){
        $this->date_schedule = $date_schedule;
        return $this; 
    }
    public function set_ministry_id( $ministry_id ){
        $this->ministry_id = $ministry_id;
        return $this; 
    }
    public function set_relation( Scheduler_relation $relation  ){
        $this->relation[] = $relation;
        return $this; 
    } 
    public function set_added_date( $added_date ){
        $this->added_date = $added_date;
        return $this; 
    } 
    public function set_time_type( $time_type ){
        $this->time_type = $time_type;
        return $this; 
    } 
    public function validate(){
        return true;
    }
    public function equal( Record $record ){
    
    }    
}

class Scheduler_relation extends Record{
    
    public $table = 'dr_schedules_relation';
    
    public $schedule_id;
    public $member_id;
    public $role_id;
    
    public $required = array(
        'schedule_id',
        'member_id',
        'role_id'
    );
    public function set_id( $id ){
        $this->id = $id;
        return $this; 
    }
    public function set_schedule_id( $schedule_id ){
        $this->schedule_id = (int)$schedule_id;
        return $this;        
    }     
    public function set_member_id( $member_id ){
        $this->member_id = (int)$member_id;
        return $this;        
    } 
    public function set_role_id( $role_id ){
        $this->role_id = (int)$role_id;
        return $this;        
    }  
    public function equal( Record $item ){
        return false;
    }   
}
 
###############################################
###############################################


class Order_model_exception extends Exception{}
//End of model
