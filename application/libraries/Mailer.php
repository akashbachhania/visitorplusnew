<?php
class Mailer{
    
    private $logs = array();
    protected function compose( Person $recipient, Template $template ){
        $replacements = array(
            '/#first/' => $recipient->first_name,
            '/#last/'  => $recipient->last_name,
        );
        $message = preg_replace( array_keys($replacements), $replacements, $template->mail_body );
        return $message;    
    }
    
    public function initialize( $mail, $config){
        $this->email = $mail;
        $this->email->initialize( $config);
    }
    
    public function send_template( Template $template ){
        $this->email->from($template->get_mail_from()); 
        foreach( $template->get_recipients() as $recipient){                
            $message = $this->compose($recipient, $template);
            $this->email->to(  $recipient->email ); 
            $this->email->subject( $template->mail_subject );
            $this->email->message( $message );
            $sent = $this->email->send(); 
            
            if( !$sent ){
                $this->logs[] = $this->email->print_debugger();
            }
        }
        return $sent;        
    }
    public function send_template_individual( Template $template,$recipient ){
        $this->email->from($template->get_mail_from());
        $message = $this->compose($recipient, $template);
        $this->email->to(  $recipient->email ); 
        $this->email->subject( $template->mail_subject );
        $this->email->message( $message );
        $sent = $this->email->send(); 

        if( !$sent ){
            $this->logs[] = $this->email->print_debugger();
        }
        
        return $sent;        
    }
    
    public function get_logs(){
        return implode(',', $this->logs);
    }
}