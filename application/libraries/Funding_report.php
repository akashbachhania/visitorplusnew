<?php
class Funding_report{
    
   public function __construct(){ 
       require_once('application/libraries/MPDF54/mpdf.php');
       
   }
   public function create_pdf( Company $company, $members = array(),$start_date, $end_date,  $file = false ){
       
        $mpdf=new mPDF('win-1252','A4','','arial',20,15,25,25,10,10);
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle( $company->name . '-Funding:' . $company->id );
        $mpdf->SetAuthor($company->name);
        $mpdf->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = false;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        
        $html = '<html>
    <head>
        <style>
            body {font-family: sans-serif;
                 font-size: 10pt;
            }
            p { margin: 0pt;
            }
            td { vertical-align: top; }  
            </style>
    </head>
    <body>
        

        <!--mpdf
        <htmlpageheader name="myheader">
        <table width="100%"><tr>
        <td width="100%" style="color:#000000;"><span style="font-weight: bold; font-size: 14pt;">'.$company->name.'</span> '.$company->address.' '.$company->city.','.$company->state.','. $company->zip. ' '.$company->phone_1.' '. $company->email .'</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="font-weight: bold; font-size: 14pt;">&nbsp;</td>
        </tr></table>

        </htmlpageheader>

        <htmlpagefooter name="myfooter">
        <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
        Page {PAGENO} of {nb}
        </div>
        </htmlpagefooter>

        <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
        <sethtmlpagefooter name="myfooter" value="on" />
        mpdf-->';
        
        $html .= '<table width="100%" style="margin-bottom:20px;"><tr>
                <td style="color:#000000;">Thank you for your contributions! Below you will find your giving report for the date range of:<br>
                From : '.$start_date.'<br>
                To : '.$end_date.'<br>
                </td>
                
                </tr></table>';
        
        if(is_array($members) and count($members) > 0) {
            $grandTotal = 0;
            $tithesTotal = $offeringTotal = $buildingFundTotal = $youthTotal = $sundaySchoolTotal = 0;
            $html .= '<table width="100%">';

            $i = 0;
            foreach($members as $member) {
                if($i % 3 == 0){
                    $html .= '<tr>';
                }

                $html .= '<td>';

                $person = $member['person'];
                $html .= '<table width="100%" style="margin-bottom:20px;"><tr>
                <td width="50%" style="color:#000000;"><span style="font-weight: bold; font-size: 14pt;">'.$person->first_name.' '.$person->last_name.'</span><br />'.$person->address.'<br />'.$person->city.' '.$person->state.','. $person->zip. '<br />'.$person->phone_1.'<br /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="font-weight: bold; font-size: 14pt;">&nbsp;</td>
                </tr></table>';
                
                $html .='<table width="40%" style="font-family: sans; border-collapse: collapse;" cellpadding="0">';   
                $funding = $member['giving'];
                if(is_array($funding) and count($funding) > 0) {
                    $subTotal = 0;
                    $v_al = 1;
                    $tithes = $offering = $buildingFund = $youth = $sundaySchool = 0;
                    foreach($funding as $name=> $value) {
                        if( $v_al == 1 ){
                            $tithes = $value;
                        }
                        elseif( $v_al == 2 ){
                            $offering = $value;    
                        }
                        elseif( $v_al == 3 ){
                            $buildingFund = $value;    
                        }
                        elseif( $v_al == 4 ){
                            $youth = $value;    
                        }
                        elseif( $v_al == 5 ){
                            $sundaySchool = $value;    
                        }
                        
                        $subTotal += $value;
                        
                        $html .='<tr>
                            <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">"'.$name.'"</td>
                            <td align="center" width="15%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$value.'</td> 
                        </tr>';
                        $v_al++;
                    }
                    $tithesTotal += $tithes;
                    $offeringTotal += $offering;
                    $buildingFundTotal += $buildingFund;
                    $youthTotal += $youth;
                    $sundaySchoolTotal += $sundaySchool;
                    $grandTotal += $subTotal;
                    $html .='<tr>
                            <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">Sub Total</td>
                            <td align="center" width="15%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$subTotal.'</td> 
                        </tr><br/><br/><br/><br/>';
                }
                $html .='</table>';

                $html .= '</td>';

                $i++;
                if($i % 3 == 0){
                    $html .= '</tr>';
                }
            }

            $html .= '</table>';

            $html .='<table width="70%" style="font-family: sans; border-collapse: collapse;" cellpadding="10">'; 
            $html .='
                    <tr>
                        <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">Tithes Total</td>
                        <td align="center" width="25%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$tithesTotal.'</td> 
                     </tr>
                     <tr>
                        <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">Offering Total</td>
                        <td align="center" width="25%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$offeringTotal.'</td> 
                     </tr>
                     <tr>
                        <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">Building Fund Total</td>
                        <td align="center" width="25%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$buildingFundTotal.'</td> 
                     </tr>
                     <tr>
                        <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">Youth Total</td>
                        <td align="center" width="25%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$youthTotal.'</td> 
                     </tr>
                     <tr>
                        <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">Sunday School Total</td>
                        <td align="center" width="25%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$sundaySchoolTotal.'</td> 
                     </tr>
                    <tr>
                        <td align="left" width="50%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">Grand Total</td>
                        <td align="center" width="25%"><span style="font-weight: bold;font-size: 11pt; color: #555555; font-family: sans;">'.$grandTotal.'</td> 
                     </tr>';
            $html .='</table>';
        }
                

    $html .= '</body>
</html>';        
   
        $mpdf->WriteHTML( $html );
        if( $file ){
            $mpdf->Output( $file, 'F');    
        } else {
            $mpdf->Output( $this->get_company_name($company), 'I');    
        }
   }
   public function get_company_name(Company $company ){
        
        $array = array( 
            preg_replace('!\W!','_', $company->city), 
            preg_replace('!\W!','_', $company->zip), 
            preg_replace('!\W!','_', trim($company->address)),
            date('Y-m-d')
        );
        return implode('_',$array).'.pdf';        
    } 
}