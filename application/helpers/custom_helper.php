<?php

function get_href( $value, $id, $url = 'visitors/show_visitor'){
    return '<a href="'.site_url($url."/$id").'">'.$value.'</a>';
}

function get_href_members( $value, $id, $url = 'members/show_member'){
    return '<a href="'.site_url($url."/$id").'">'.$value.'</a>';
}

function get_popover_data_loading( Visitor $visitor, $type ){
    $trigger = '';
    switch($type){
        case 'visit_date':
            $id  = 'dates_'; 
            $data = '<div id="div_status_visits_'.$visitor->id.'" class="hidden">
                        <a class="close" data-dismiss="alert">x</a>
                     </div>';
            $data.= '<div class="form-inline datepickerDiv"><input type="text" class="input-small datepickerpop" data-datepicker="datepicker" id="new_visit_'.$visitor->id.'" value="'.gmdate('m-d-Y').'" onload="$(this).datepicker()"/><a class="btn btn-warning" href="javascript:void(0);" onclick="add_visit('.$visitor->id.')">Add visit</a></div>';
            $data.= '<table id="'.$id.$visitor->id.'_table"class="table  first center" onload="create_table(this)">';
            $data.= '<thead><tr><th>Visit dates</th></tr></thead>';
            foreach($visitor->_visits as $visit ){
                $data.= '<tr><td>'.get_us_date_format($visit->datetime).'</td></tr>';    
            }
            $data.= '</table>';
            $data.= '<a id="btn_cancel_visits_'.$visitor->id.'"class="btn" href="javascript:void(0);"  onclick="hide_visit_log('.$visitor->id.')">Close</a>';
            $title = 'Visit log';
            
            $field_data = 'Not available';
            if( is_array( $visitor->_visits )){
                $last_date  = $visitor->_visits[count($visitor->_visits)-1]->datetime;                
                $field_data = $visitor->get_us_date($last_date);
            }
            $tag = 'a';
            $trigger = 'data-trigger=\'click\'';
            $class = 'btn span1';
            $left_or_right= 'right';
        break;
        case 'notes':
            $data = '<div id="div_status_'.$visitor->id.'" class="hidden">
                        <a class="close" data-dismiss="alert">x</a>
                     </div>
                     <textarea id="text_notes_'.$visitor->id.'">'.preg_replace( "!'!", "", $visitor->notes ).'</textarea>
                     <a class="btn" href="javascript:void(0);" onclick="save_notes('.$visitor->id.')">Save</a>
                     <a id="btn_cancel_'.$visitor->id.'"class="btn" href="#" onclick="hide_notes('.$visitor->id.', this)">Cancel</a>';             
            $title = 'Notes';
            $field_data = $visitor->notes ? 'Edit Notes' : 'Add Notes';
            $trigger = 'data-trigger=\'click\'';
            $tag = 'a';
            $id = 'notes_';
            $class = 'btn span2';
            $left_or_right = 'left';
        break;
    }
    return '<'.$tag.' '.$trigger.' class="'.$class.'"  data-html=""   id="'.$id.$visitor->id.'" data-placement="'.$left_or_right.'" data-content=\''.$data.'\'   data-original-title="'.$title.'">'.$field_data.'</'.$tag.'>';
}

function get_popover_data_member( Visitor $visitor, $type ){
    $trigger = '';
    switch($type){
        case 'visit_date':
            $id  = 'dates_'; 
            $data = '<div id="div_status_visits_'.$visitor->id.'" class="hidden">
                        <a class="close" data-dismiss="alert">x</a>
                     </div>';
            $data.= '<div class="form-inline datepickerDiv"><input type="text" class="input-small datepickerpop" data-datepicker="datepicker" id="new_visit_'.$visitor->id.'" value="'.gmdate('m-d-Y').'"/><a class="btn btn-warning" href="javascript:void(0);" onclick="add_attende('.$visitor->id.')">Add Attended</a></div>';
            
            $data.= '<a id="btn_cancel_visits_'.$visitor->id.'"class="btn" href="javascript:void(0);"  onclick="hide_member_log('.$visitor->id.')">Close</a>';
            $title = 'Attende log';
            
            $field_data = 'Not available';
            
            $last_date  = $visitor->registered_date;
            $last_date  = explode(' ', $last_date);
            $field_data = $visitor->get_us_date($last_date[0]);
            
            $tag = 'a';
            $trigger = 'data-trigger=\'click\'';
            $class = 'btn span1';
        break;
        case 'notes':
            $data = '<div id="div_status_'.$visitor->id.'" class="hidden">
                        <a class="close" data-dismiss="alert">x</a>
                     </div>
                     <textarea id="text_notes_'.$visitor->id.'">'.preg_replace( "!'!", "", $visitor->notes ).'</textarea>
                     <a class="btn" href="javascript:void(0);" onclick="save_notes('.$visitor->id.')">Save</a>
                     <a id="btn_cancel_'.$visitor->id.'"class="btn" href="javascript:void(0);" onclick="hide_notes_members('.$visitor->id.', this)">Cancel</a>';             
            $title = 'Notes';
            $field_data = $visitor->notes ? 'Edit Notes' : 'Add Notes';
            $trigger = 'data-trigger=\'click\'';
            $tag = 'a';
            $id = 'notes_';
            $class = 'btn span2';
        break;
    }

    return '<'.$tag.' '.$trigger.' class="'.$class.'"  data-html=""   id="'.$id.$visitor->id.'" data-content=\''.$data.'\' data-placement="left" data-original-title="'.$title.'">'.$field_data.'</'.$tag.'>';
}

function get_us_date_format( $date ) {
    if( preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $date, $matches ) ){
        $date = $matches['month'].'-'.$matches['day'].'-'.$matches['year'].' '.( isset($matches['rest']) ?' '.$matches['rest']: '' );
    }
    return $date;    
}

function sort_visitors($visitors, $sort_descending = true){
    $visitors = (array)$visitors;
    $dates    = array();
    foreach($visitors as $visitor){
        $visit_dates = array();
        foreach( $visitor->_visits as $visit){
            $visit_dates[] = $visit->datetime;
        }
        sort($visit_dates);
        $dates[ end($visit_dates)][] = $visitor;
    }
    ksort($dates);
    if( $sort_descending ){
        $dates = array_reverse($dates);    
    }
    $sorted_visitors = array();
    foreach( $dates as $date ){
        foreach( $date as $visitor){
            $sorted_visitors[] = $visitor;
        }
    }
    return $sorted_visitors;
}
function getTotalCompanyMembers($company_id) {
    $ci =&get_instance();
   $query = 'SELECT COUNT(id) AS total 
            FROM dr_persons
            WHERE company_id = '.$company_id.' AND person_type = 4';    
    $result = $ci->db->query( $query );
    if($result->num_rows > 0) {        
        $row = $result->row();        
        return $row->total;
    }
    return 0;
}